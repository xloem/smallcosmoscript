set -e

CHAIN_JSON="$1"
if [ ! -r "$CHAIN_JSON" ]
then
    if [ ! -r "../chain-registry/$CHAIN_JSON/chain.json" ]
    then
        echo cannot read from "'$CHAIN_JSON'"
        false
    else
        CHAIN_JSON="../chain-registry/$CHAIN_JSON/chain.json"
    fi
fi

git -C akash-tools checkout -- .
for patch in akash-tools-*.patch
do
    patch -p1 --directory=akash-tools --reject-file=- < "$patch"
done


#export LOGLEVEL=1
. akash-tools/cli-booster/akash.source || true

akash_mkcert 2>/dev/null|| true

CHAIN_REGISTRY_NAME="$(jq -r .chain_name "$CHAIN_JSON")"
CHAIN_HOME="$(jq -r '.node_home | sub("\\$HOME";"/root")' "$CHAIN_JSON")"

CHAIN_ID="$(jq -r .chain_id < "$CHAIN_JSON")"
BECH32_PREFIX="$(jq -r .bech32_prefix < "$CHAIN_JSON")"
FEE_DENOM="$(jq -r .fees.fee_tokens[0].denom < "$CHAIN_JSON")"
GAS_PRICE_MIN="$(jq -r .fees.fee_tokens[0].fixed_min_gas_price < "$CHAIN_JSON")"
GAS_PRICE_LOW="$(jq -r .fees.fee_tokens[0].low_gas_price < "$CHAIN_JSON")"
STAKING_DENOM="$(jq -r .staking.staking_tokens[0].denom < "$CHAIN_JSON")"
P2P_SEEDS="$(jq -r '.peers.seeds | map(.id+"@"+.address) | join(",")' < "$CHAIN_JSON")"
P2P_PERSISTENT_PEERS="$(jq -r '.peers.persistent_peers | map(.id+"@"+.address) | join(",")' < "$CHAIN_JSON")"
GENESIS_URL="$(jq -r '.codebase.genesis.genesis_url? // .genesis.genesis_url? // .genesis?' < "$CHAIN_JSON")"
#BINARY_URL="$(jq -r '.codebase.binaries."linux/amd64"?' < "$CHAIN_JSON")"
PROJECT_BIN="$(jq -r '.codebase.daemon_name? // .daemon_name?' < "$CHAIN_JSON")"
REPOSITORY="$(jq -r '.codebase.git_repo' < "$CHAIN_JSON")"
FULL_DIR="$(jq -r '.codebase.node_home? // .node_home?' < "$CHAIN_JSON")"
PROJECT_DIR="${FULL_DIR#'$HOME/'}"
CHAIN_NODE="$(jq -r ".apis.rpc[$RANDOM%length].address" < "$CHAIN_JSON")"
[ "${CHAIN_NODE%://*:*}" = "$CHAIN_NODE" ] && CHAIN_NODE="${CHAIN_NODE}:443"
#for CHAIN_COSMPY in $(jq -r '(["rest+"+.apis.rest[].address]+["grpc+"+.apis.grpc[].address]) |.[]' < "$CHAIN_JSON" | sort -R)
#do
#  curl --head --silent "${CHAIN_COSMPY#*+}" >/dev/null && echo "$CHAIN_COSMPY" && break
#done
CHAIN_COSMPY_URLS="$(jq -r '(["rest+"+.apis.rest[].address]+["grpc+"+.apis.grpc[].address]) |.[]' < "$CHAIN_JSON" | sort -R)"

## if domain lookups are failing the main clone process will crash the whole system
## but this might be much simpler if the cloning happened in this script; it could just call run.sh
#REPOSITORY_HOST="${REPOSITORY#*//}"
#REPOSITORY_HOST="${REPOSITORY_HOST%%/*}"
#REPOSITORY_IP="$(host -t A "$REPOSITORY_HOST" | awk '{ print $4 }')"
#REPOSITORY_PROTO="${REPOSITORY%%$REPOSITORY_HOST*}"
#REPOSITORY_PATH="${REPOSITORY#*$REPOSITORY_HOST}"
#REPOSITORY="git://$REPOSITORY_IP$REPOSITORY_PATH"

DEPLOY_YAML="$CHAIN_REGISTRY_NAME".deploy.yaml
cat >"$DEPLOY_YAML" <<EOF
---
version: "2.0"

services:
  node:
    image: baffo32/cosmos-omnibus:v0.3.31-6-g345cac5b-build-20-2023-05-28
    env:
      - MONIKER=node_1
      #- CHAIN_JSON=file:///script/chain-registry/$CHAIN_REGISTRY_NAME/chain.json
      - CHAIN_ID=$CHAIN_ID
      - P2P_SEEDS=$P2P_SEEDS
      - P2P_PERSISTENT_PEERS=$P2P_PERSISTENT_PEERS
      - GENESIS_URL=$GENESIS_URL
      - PROJECT_BIN=$PROJECT_BIN
      - REPOSITORY=$REPOSITORY
      - PROJECT_DIR=$PROJECT_DIR
      - START_CMD=$PROJECT_BIN start --api.enable
      - ADDRBOOK_POLKACHU=1
      - SNAPSHOT_POLKACHU=1
      #- DEBUG=1
      #- DEBUG=2
      - MINIMUM_GAS_PRICES=0.000$FEE_DENOM
    expose:
      - port: 26657
        as: 80
        to:
          - global: true
      - port: 26656
        to:
          - global: true
    # params:
    #   storage:
    #     data:
    #       mount: $CHAIN_HOME

profiles:
  compute:
    node:
      resources:
        cpu:
          units: 4
        memory:
          size: 8Gi
        storage:
          size: 100Gi
          # - size: 100Mi
          # - name: data
          #   size: 400Gi
          #   attributes:
          #     persistent: true
  placement:
    dcloud:
      attributes:
        host: akash
      signedBy:
        anyOf:
          - akash1365yvmc4s7awdyj3n2sav7xfx76adc6dnmlx63
      pricing:
        node:
          denom: uakt
          amount: 1000

deployment:
  node:
    dcloud:
      profile: node
      count: 1
EOF

akash_deploy "$DEPLOY_YAML"

function atexit {
    export LOGLEVEL=1
    echo "Closing order."
    akash_close >/dev/null &
}
trap atexit EXIT

while ! akash_accept
do
    sleep 1
done

akash_send_manifest "$DEPLOY_YAML"

if [ -z "$SEED_FILE" ]
then
    akash_findprovider
    SEED_FILE="$AKASH_PROVIDER".wallet
    export -n AKASH_PROVIDER
fi

if [ ! -e "$SEED_FILE" ]
then
    $AKASH_BIN keys mnemonic > "$SEED_FILE" 2>&1
fi

#AKASH_KEYS="$AKASH_BIN keys --keyring-dir=${SEED_FILE}.keyring"
#ADDR="$($AKASH_KEYS add "$SEED_FILE" --recover --output json < "$SEED_FILE" 2>&1 | jq -r .address)"
#ADDR="${BECH32_PREFIX}${ADDR#akash}" #oops checksum
##ADDR="$($AKASH_KEYS show "$SEED_FILE" --output json 2>&1 | jq -r .address)"
#yes '' | $AKASH_KEYS delete "$SEED_FILE" > /dev/null 2>&1
#rm -rf "${SEED_FILE}.keyring"

export SEED_FILE BECH32_PREFIX CHAIN_ID FEE_DENOM STAKING_DENOM CHAIN_COSMPY_URLS GAS_PRICE_MIN GAS_PRICE_LOW
PYTHONPATH=.. python3 - <<EOF
from cosmpy.aerial.wallet import LocalWallet
from cosmpy.aerial.client import Account, Transaction, create_bank_send_msg, prepare_and_broadcast_basic_transaction
from cosmpy2 import LedgerClient, NetworkConfig
import json
import os
import logging
logging.basicConfig(level=logging.DEBUG)
with open(os.environ['SEED_FILE']) as fh:
    wallet = LocalWallet.from_mnemonic(fh.read().strip(), os.environ['BECH32_PREFIX'])
for url in os.environ['CHAIN_COSMPY_URLS'].split('\n'):
    #if not url.startswith('rest+'):
    #   continue
    cfg = NetworkConfig(os.environ['CHAIN_ID'], int(os.environ['GAS_PRICE_MIN']), os.environ['FEE_DENOM'], os.environ['STAKING_DENOM'], url=url)
    try: 
        client = LedgerClient(cfg, api_timeout_secs=10)
        balance = client.query_bank_balance(wallet.address())
        break
    except Exception as e:
        print(url, e)
print(cfg)
if not balance:
    with open('../../omniflix.wallet') as fh:
        fundsrc = LocalWallet.from_mnemonic(fh.read().strip(), os.environ['BECH32_PREFIX'])
    denom = client.network_config.staking_denomination
    avail = client.query_bank_balance(fundsrc.address(), denom=denom)
    if not avail:
        raise RuntimeError(f'fund {fundsrc.address()} for {client.network_config.chain_id} {os.environ["SEED_FILE"]}')
    print(fundsrc.address())
    acctjson = json.loads(client.auth._rest_api.get(f'{client.auth.API_URL}/accounts/{fundsrc.address()}'))['account']['base_account']
    acct = Account(fundsrc.address(), int(acctjson['account_number']), int(acctjson['sequence']))
    #acct = client.auth.Account(fundsrc.address())
    tx = Transaction()
    tx.add_message(
        create_bank_send_msg(fundsrc.address(), wallet.address(), avail // 100, denom=denom)
    )
    #gas_limit = client.estimate_gas_for_tx(tx)
    tx = prepare_and_broadcast_basic_transaction(client, tx, fundsrc, gas_limit=1000000, account=acct)
    #tx = client.send_tokens(wallet.address(), avail / 100, denom, fundsrc, account=acct)
    tx.wait_to_complete()

#print(wallet.address(),end='')
EOF
#AKASH_NODE="$CHAIN_NODE" $AKASH_BIN query account "$ADDR"

#akash_logs -f&
LOG_PID=$!
while akash_status | grep -q '"available": 0' && ps "$LOG_PID" >/dev/null
do
    echo "Waiting for service availability ..."
    sleep 1
done

set -vx
set +e
{
    git -C "$(git rev-parse --show-toplevel)" bundle create - --all
    sleep 10
    cat /dev/zero
} | akash_shell /usr/bin/env bash -vxc '
    set -e
    mkdir /script
    cd /script
    git init .
    git bundle unbundle /dev/stdin > .git/packed-refs
    git checkout
    git submodule update --init --checkout --recursive --progress &
    apt-get install -y python3-pip
    python3 -m pip install --upgrade pip
    wait
    python3 -m pip install python/cosmpy
'
if [ $? -eq 0 ] #-o $? -eq 1 ]
then
    akash_shell /usr/bin/env bash -c '
        echo "Waiting for port to listen ..."
        while ! curl --head --silent 127.0.0.1:1317
        do
            sleep 1
        done
    '
    kill "$LOG_PID" || true
    kill -9 "$LOG_PID" || true
    {
        echo "$CHAIN_ID"
        cat "$SEED_FILE"
        echo "$BECH32_PREFIX"
        echo "$FEE_DENOM"
        echo "$STAKING_DENOM"
    } | akash_shell /usr/bin/env bash -vxc '
        echo "Waiting for port to listen ..."
        while ! curl --head --silent 127.0.0.1:1317
        do
            sleep 1
        done
        set -e
        read CHAIN_ID
        read SEED_PHRASE
        read BECH32_PREFIX
        read FEE_DENOM
        read STAKING_DENOM
        export CHAIN_ID SEED_PHRASE BECH32_PREFIX FEE_DENOM STAKING_DENOM
        python3 fee.py
    '
else
    akash_findprovider
    SKIP_PROVIDERS="$AKASH_PROVIDER $SKIP_PROVIDERS"
    atexit
    export -n AKASH_PROVIDER
    export SKIP_PROVIDERS
    wait
    exec bash -x "$0" "$@"
fi

import asyncio, base64, sys
from small.client import Client
from small.cosmoschains import FileRegistry as Registry
from small.tx import Transaction
from small.wallet import Wallet

async def amain(chain_id, seed_phrase, urls):
    chain = Registry().chain(chain_id)
    wallet = Wallet(chain, seed_phrase)
    try:
        await wallet.__ainit__(urls=urls)
        client = wallet.client
        bond_denom = (await client.cosmos_staking.aParams()).bond_denom
        balance = await client.cosmos_bank.aBalance(wallet.address, bond_denom)
        assert balance.denom == bond_denom
        assert balance.amount
        print(wallet.address, balance.amount, balance.denom, file=sys.stderr)
    except Client.Errors as error:
        status, msg = Client.error_status_message(error)
        if status == Client.Statuses.NOT_FOUND:
            fundsrc = Wallet(chain, '../cosmos.wallet', client=wallet.client)
            try:
                await fundsrc.__ainit__(urls=urls)
                client = fundsrc.client
                bond_denom = (await client.cosmos_staking.aParams()).bond_denom
                avail = await client.cosmos_bank.aBalance(fundsrc.address, bond_denom)
                avail_amount = int(avail.amount)
            except Client.Errors as error:
                status, msg = Client.error_status_message(error)
                if status != Client.Statuses.NOT_FOUND:
                    raise
                avail_amount = 0
            if not avail_amount:
                raise RuntimeError(f'fund {fundsrc._bootstrap_address} for {chain_id} {os.environ["SEED_FILE"]} {wallet._bootstrap_address}')
            print(fundsrc.address, file=sys.stderr)
            tx = Transaction(fundsrc)
            tx.cosmos_bank_send(fundsrc.address, wallet._bootstrap_address, [tx.Coin(avail_amount // 10, bond_denom)])
            await tx.asign(gas_price=0, fee_denom=wallet.default_fee_denom)
            tx_bytes = base64.b64encode(tx.tx_bytes()).decode()
            print(f'--broadcast={tx_bytes}')
    
if __name__ == '__main__':
    import logging, os
    logging.basicConfig(level=logging.INFO)
    asyncio.run(amain(
        chain_id = os.environ['CHAIN_ID'],
        seed_phrase = os.environ.get('SEED_FILE') or os.environ.get('SEED_PHRASE'),
        urls = [url for url in os.environ['CHAIN_COSMPY_URLS'].strip().split('\n') if url.startswith('grpc+')],
    ))

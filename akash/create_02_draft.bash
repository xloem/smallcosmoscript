set -e

# NOTE: these wallet phrases have been output to stdout while recording on rhel7, return funds and regenerate

CHAIN_JSON="$1"
if [ ! -r "$CHAIN_JSON" ]
then
    if [ ! -r "../chain-registry/$CHAIN_JSON/chain.json" ]
    then
        echo cannot read from "'$CHAIN_JSON'"
        false
    else
        CHAIN_JSON="../chain-registry/$CHAIN_JSON/chain.json"
    fi
fi

git -C akash-tools checkout -- .
for patch in akash-tools-*.patch
do
    patch -p1 --directory=akash-tools --reject-file=- < "$patch"
done


#export LOGLEVEL=1
provider-services version
akash version
. akash-tools/cli-booster/akash.source || true

akash_mkcert 2>/dev/null || true

CHAIN_REGISTRY_NAME="$(jq -r .chain_name "$CHAIN_JSON")"
CHAIN_HOME="$(jq -r '.node_home | sub("\\$HOME";"/root")' "$CHAIN_JSON")"

CHAIN_ID="$(jq -r .chain_id < "$CHAIN_JSON")"
BECH32_PREFIX="$(jq -r .bech32_prefix < "$CHAIN_JSON")"
FEE_DENOM="$(jq -r .fees.fee_tokens[0].denom < "$CHAIN_JSON")"
GAS_PRICE_MIN="$(jq -r .fees.fee_tokens[0].fixed_min_gas_price < "$CHAIN_JSON")"
GAS_PRICE_LOW="$(jq -r .fees.fee_tokens[0].low_gas_price < "$CHAIN_JSON")"
STAKING_DENOM="$(jq -r .staking.staking_tokens[0].denom < "$CHAIN_JSON")"
P2P_SEEDS="$(jq -r '.peers.seeds | map(.id+"@"+.address) | join(",")' < "$CHAIN_JSON")"
P2P_PERSISTENT_PEERS="$(jq -r '.peers.persistent_peers | map(.id+"@"+.address) | join(",")' < "$CHAIN_JSON")"
GENESIS_URL="$(jq -r '.codebase.genesis.genesis_url? // .genesis.genesis_url? // .genesis?' < "$CHAIN_JSON")"
#BINARY_URL="$(jq -r '.codebase.binaries."linux/amd64"?' < "$CHAIN_JSON")"
PROJECT_BIN="$(jq -r '.codebase.daemon_name? // .daemon_name?' < "$CHAIN_JSON")"
REPOSITORY="$(jq -r '.codebase.git_repo' < "$CHAIN_JSON")"
FULL_DIR="$(jq -r '.codebase.node_home? // .node_home?' < "$CHAIN_JSON")"
PROJECT_DIR="${FULL_DIR#'$HOME/'}"
CHAIN_NODE="$(jq -r ".apis.rpc[$RANDOM%length].address" < "$CHAIN_JSON")"
[ "${CHAIN_NODE%://*:*}" = "$CHAIN_NODE" ] && CHAIN_NODE="${CHAIN_NODE}:443"
#for CHAIN_COSMPY in $(jq -r '(["rest+"+.apis.rest[].address]+["grpc+"+.apis.grpc[].address]) |.[]' < "$CHAIN_JSON" | sort -R)
#do
#  curl --head --silent "${CHAIN_COSMPY#*+}" >/dev/null && echo "$CHAIN_COSMPY" && break
#done
CHAIN_COSMPY_URLS="$(jq -r '(["rest+"+.apis.rest[].address]+["grpc+"+.apis.grpc[].address]) |.[]' < "$CHAIN_JSON" | sort -R)"

## if domain lookups are failing the main clone process will crash the whole system
## but this might be much simpler if the cloning happened in this script; it could just call run.sh
#REPOSITORY_HOST="${REPOSITORY#*//}"
#REPOSITORY_HOST="${REPOSITORY_HOST%%/*}"
#REPOSITORY_IP="$(host -t A "$REPOSITORY_HOST" | awk '{ print $4 }')"
#REPOSITORY_PROTO="${REPOSITORY%%$REPOSITORY_HOST*}"
#REPOSITORY_PATH="${REPOSITORY#*$REPOSITORY_HOST}"
#REPOSITORY="git://$REPOSITORY_IP$REPOSITORY_PATH"

DEPLOY_YAML="$CHAIN_REGISTRY_NAME".deploy.yaml
cat >"$DEPLOY_YAML" <<EOF
---
version: "2.0"

services:
  node:
    image: baffo32/cosmos-omnibus:v0.3.31-6-g345cac5b-build-20-2023-05-28
    env:
      - MONIKER=node_1
      #- CHAIN_JSON=file:///script/chain-registry/$CHAIN_REGISTRY_NAME/chain.json
      - CHAIN_ID=$CHAIN_ID
      - P2P_SEEDS=$P2P_SEEDS
      - P2P_PERSISTENT_PEERS=$P2P_PERSISTENT_PEERS
      - GENESIS_URL=$GENESIS_URL
      - PROJECT_BIN=$PROJECT_BIN
      - REPOSITORY=$REPOSITORY
      - PROJECT_DIR=$PROJECT_DIR
      - START_CMD=$PROJECT_BIN start
      - ADDRBOOK_POLKACHU=1
      - SNAPSHOT_POLKACHU=1
      #- DEBUG=1
      #- DEBUG=2
      - MINIMUM_GAS_PRICES=0.000$FEE_DENOM
    expose:
      - port: 26657
        as: 80
        to:
          - global: true
      - port: 26656
        to:
          - global: true
    # params:
    #   storage:
    #     data:
    #       mount: $CHAIN_HOME

profiles:
  compute:
    node:
      resources:
        cpu:
          units: 4
        memory:
          size: 8Gi
        storage:
          size: 100Gi
          # - size: 100Mi
          # - name: data
          #   size: 400Gi
          #   attributes:
          #     persistent: true
  placement:
    dcloud:
      attributes:
        host: akash
      signedBy:
        anyOf:
          - akash1365yvmc4s7awdyj3n2sav7xfx76adc6dnmlx63
      pricing:
        node:
          denom: uakt
          amount: 1000

deployment:
  node:
    dcloud:
      profile: node
      count: 1
EOF

akash_deploy "$DEPLOY_YAML"

function atexit {
    export LOGLEVEL=1
    echo "Closing order."
    akash_close >/dev/null &
}
trap atexit EXIT

while ! akash_accept
do
    sleep 1
done

akash_send_manifest "$DEPLOY_YAML"

if [ -z "$SEED_FILE" ]
then
    akash_findprovider
    SEED_FILE="$AKASH_PROVIDER".wallet
    export -n AKASH_PROVIDER
fi

if [ ! -e "$SEED_FILE" ]
then
    $AKASH_BIN keys mnemonic > "$SEED_FILE" 2>&1
fi

#AKASH_KEYS="$AKASH_BIN keys --keyring-dir=${SEED_FILE}.keyring"
#ADDR="$($AKASH_KEYS add "$SEED_FILE" --recover --output json < "$SEED_FILE" 2>&1 | jq -r .address)"
#ADDR="${BECH32_PREFIX}${ADDR#akash}" #oops checksum
##ADDR="$($AKASH_KEYS show "$SEED_FILE" --output json 2>&1 | jq -r .address)"
#yes '' | $AKASH_KEYS delete "$SEED_FILE" > /dev/null 2>&1
#rm -rf "${SEED_FILE}.keyring"

export SEED_FILE BECH32_PREFIX CHAIN_ID FEE_DENOM STAKING_DENOM CHAIN_COSMPY_URLS GAS_PRICE_MIN GAS_PRICE_LOW
#PYTHONPATH=.. python3 - <<EOF
cat > "$0".py <<EOF
import asyncio, base64, sys
from small.client import Client
from small.cosmoschains import FileRegistry as Registry
from small.tx import Transaction
from small.wallet import Wallet

async def amain(chain_id, seed_phrase, urls):
    chain = Registry().chain(chain_id)
    wallet = Wallet(chain, seed_phrase)
    try:
        await wallet.__ainit__(urls=urls)
        client = wallet.client
        bond_denom = (await client.cosmos_staking.aParams()).bond_denom
        balance = await client.cosmos_bank.aBalance(wallet.address, bond_denom)
        assert balance.denom == bond_denom
        assert balance.amount
        print(wallet.address, balance.amount, balance.denom, file=sys.stderr)
    except Client.Errors as error:
        status, msg = Client.error_status_message(error)
        if status == Client.Statuses.NOT_FOUND:
            fundsrc = Wallet(chain, '../cosmos.wallet', client=wallet.client)
            try:
                await fundsrc.__ainit__(urls=urls)
                client = fundsrc.client
                bond_denom = (await client.cosmos_staking.aParams()).bond_denom
                avail = await client.cosmos_bank.aBalance(fundsrc.address, bond_denom)
                avail_amount = int(avail.amount)
            except Client.Errors as error:
                status, msg = Client.error_status_message(error)
                if status != Client.Statuses.NOT_FOUND:
                    raise
                avail_amount = 0
            if not avail_amount:
                raise RuntimeError(f'fund {fundsrc._bootstrap_address} for {chain_id} {os.environ["SEED_FILE"]} {wallet._bootstrap_address}')
            print(fundsrc.address, file=sys.stderr)
            tx = Transaction(fundsrc)
            tx.cosmos_bank_send(fundsrc.address, wallet._bootstrap_address, [tx.Coin(avail_amount // 10, bond_denom)])
            await tx.asign(gas_price=0, fee_denom=wallet.default_fee_denom)
            tx_bytes = base64.b64encode(tx.tx_bytes()).decode()
            print(f'--broadcast={tx_bytes}')
    
if __name__ == '__main__':
    import logging, os
    logging.basicConfig(level=logging.INFO)
    asyncio.run(amain(
        chain_id = os.environ['CHAIN_ID'],
        seed_phrase = os.environ.get('SEED_FILE') or os.environ.get('SEED_PHRASE'),
        urls = [url for url in os.environ['CHAIN_COSMPY_URLS'].strip().split('\n') if url.startswith('grpc+')],
    ))
EOF
BROADCAST_PARAMS="$(PYTHONPATH=.. python3 "$0".py | tee /dev/stderr)"
#AKASH_NODE="$CHAIN_NODE" $AKASH_BIN query account "$ADDR"

{ akash_logs -f | stdbuf -i0 -o0 tr '\n' '\r\n'; } &
LOG_PID=$!
LOG_PID="$(cat /proc/$LOG_PID/task/$LOG_PID/children | awk '{ print $1; }')" # first pipeline cmd
while akash_status | grep -q '"available": 0' && ps "$LOG_PID" >/dev/null
do
    echo "Waiting for service availability ..."
    sleep 1
done

set -vx
set +e
{
    git -C "$(git rev-parse --show-toplevel)" bundle create - --all
    sleep 10
} | akash_shell /usr/bin/env bash -vxc '
    set -e
    mkdir /script
    cd /script
    git init .
    git bundle unbundle /dev/stdin > .git/packed-refs
    git checkout main
    git submodule update --init --checkout --recursive --progress &
    apt-get install -y python3-pip
    python3 -m pip install --upgrade pip
    wait
    python3 -m pip install mospy-wallet numpy tqdm semver diskcache bc_jsonpath_ng jsonpatch curl_cffi requests grpclib
    cd other/cosmospy-protobuf
    for branch in grpclib-cosmos grpclib-evmos grpclib-osmosis grpclib-akash
    do
        git checkout "$branch"
        python3 -m pip install .
    done
    cd ../..
'
if [ $rc -eq 0 ] #-o $rc -eq 1 ]
then
    akash_shell /usr/bin/env bash -c '
        echo "Waiting for port to listen ..."
        while ! { echo > /dev/tcp/127.0.0.1/9090; } 2>/dev/null
        do
            sleep 1
        done
    '
    kill $(</proc/$LOG_PID/task/$LOG_PID/children) $LOG_PID || true
    #kill $LOG_PID || true
    {
        echo "$CHAIN_ID"
        cat "$SEED_FILE"
        echo "$BECH32_PREFIX"
        echo "$FEE_DENOM"
        echo "$STAKING_DENOM"
        echo "$BROADCAST_PARAMS"
    } | akash_shell /usr/bin/env bash -vxc '
        echo "Waiting for port to listen ..."
        while ! { echo > /dev/tcp/127.0.0.1/9090; } 2>/dev/null
        do
            sleep 1
        done
        set -e
        read CHAIN_ID
        read SEED_PHRASE
        read BECH32_PREFIX
        read FEE_DENOM
        read STAKING_DENOM
        read BROADCAST_PARAMS
        export CHAIN_ID SEED_PHRASE BECH32_PREFIX FEE_DENOM STAKING_DENOM
        cd /script
        if [ -n "$BROADCAST_PARAMS" ]; then
            python3 fee_02.py $CHAIN_ID --url=grpc+http://127.0.0.1:9090/ "$BROADCAST_PARAMS" 2>&1
        else
            #python3 -m pdb fee_02.py $CHAIN_ID --url=grpc+http://127.0.0.1:9090/ 2>&1
        fi
    '
    akash_shell /usr/bin/env bash -vxc 'cd /script; SEED_PHRASE="'"$(cat "$SEED_FILE")"'" python3 -m pdb fee_02.py '"$CHAIN_ID"' --url=grpc+http://127.0.0.1:9090/'
else
    akash_findprovider
    SKIP_PROVIDERS="$AKASH_PROVIDER $SKIP_PROVIDERS"
    atexit
    export -n AKASH_PROVIDER
    export SKIP_PROVIDERS
    wait
    exec bash -x "$0" "$@"
fi

import asyncio, time

from .crypto import x25519_private, x25519_private_to_public, x25519_private_peer_to_shared, chacha20_encrypt, chacha20_decrypt, hkdf_sha256, randombytes, curve_private_to_public, curve_public_to_address, curve_sign, curve_verify
from . import codec
import small.util as util

import merlin # git+https://github.com/nalinbhardwaj/curdleproofs.pie#subdirectory=merlin

class ProtocolInformation(Exception):
    pass

class SessionClosed(ProtocolInformation):
    pass

class PeerConnection:
    def __init__(self, peer_address, max_packet_msg_payload = 1024):
        self._max_packet = 1024
        peer_id, peer_host, peer_port = codec.addr_decode(peer_address)
        self._peer_addr = peer_address
        self._peer_id = bytes.fromhex(peer_id)
        self._peer_host, self._peer_port = peer_host, peer_port

    async def connect(self,
            key_type:str, private_key:bytes,
            nodeinfo:dict, channels:list,
            signing_digest,
            authsig_encode, authsig_decode,
            nodeinfo_encode, nodeinfo_decode
    ):
        node_id_hex = curve_public_to_address[key_type](
            curve_private_to_public[key_type](private_key)
        ).hex() # just set it to this value
        assert node_id_hex == nodeinfo.setdefault('default_node_id', node_id_hex)
        channel_ids = bytes([ch.ID for ch in channels])
        assert channel_ids == nodeinfo.setdefault('channels', channel_ids)
        assert len(nodeinfo['moniker'].strip())

        self._recv_key, self._send_key = None, None
        #import pdb; pdb.set_trace()
        try:
            self._reader, self._writer = await asyncio.wait_for(
                    asyncio.open_connection(self._peer_host, self._peer_port),
                    3
            )
        except TimeoutError as e:
            #import pdb; pdb.set_trace()
            #self._reader, self._writer = await asyncio.wait_for(
            #        asyncio.open_connection(self._peer_host, self._peer_port),
            #        3
            #)
            raise ProtocolInformation('connection timed out', self._peer_addr) from e
        except ConnectionRefusedError as e:
            raise ProtocolInformation('connection refused', self._peer_addr) from e
        except OSError as e:
            raise ProtocolInformation('os error', self._peer_addr) from e
        self._recv_buf = bytearray()

        # from tendermint/spec/p2p/peer.md
        #    (possibly inaccurate documentation; refer to tendermint/p2p/conn/secret_connection.go:MakeSecretConnection)
        # - generate an ephemeral X25519 keypair
        ephemeral_private = x25519_private()
        ephemeral_public = x25519_private_to_public(ephemeral_private)
        # - send the ephemeral public key to the peer
        self._writer.write(codec.delimited_encode(codec.bytes_encode(ephemeral_public)))
        #await self.writer.drain()
        # - wait to receive the peer's ephemeral public key
        try:
            ephemeral_peer = codec.bytes_decode(await asyncio.wait_for(
                    codec.delimited_aread(self._reader.readexactly),
                    3
            ))
        except TimeoutError as e:
            raise ProtocolInformation('reading any data from peer timed out', self._peer_addr) from e
        except Exception as e:
            raise ProtocolInformation('peer connection lost without data', self._peer_addr) from e
        if not ephemeral_peer:
            raise ProtocolInformation('peer sent empty public key', self._peer_addr)
        assert len(ephemeral_peer) == 32
        # - create a new Merlin Transcript with the string "TENDERMINT_SECRET_CONNECTION_TRANSCRIPT_HASH"
        merlin_transcript = merlin.MerlinTranscript(b'TENDERMINT_SECRET_CONNECTION_TRANSCRIPT_HASH')
        # - Sort the ephemeral keys and add the high labeled "EPHEMERAL_UPPER_PUBLIC_KEY" and the low keys labeled "EPHEMERAL_LOWER_PUBLIC_KEY" to the Merlin transcript.
        ephemeral_keys = [ephemeral_public, ephemeral_peer]
        ephemeral_keys.sort()
        merlin_transcript.append_message(b'EPHEMERAL_LOWER_PUBLIC_KEY', ephemeral_keys[0])
        merlin_transcript.append_message(b'EPHEMERAL_UPPER_PUBLIC_KEY', ephemeral_keys[-1])
        # - compute the Diffie-Hellman shared secret using the peers ephemeral public key and our ephemeral private key
        shared_secret = x25519_private_peer_to_shared(ephemeral_private, ephemeral_peer)
        # - add the DH secret to the transcript labeled DH_SECRET.
        merlin_transcript.append_message(b'DH_SECRET', shared_secret)
        # - generate two keys to use for encryption (sending and receiving) and a challenge for authentication as follows:
        #     - create a hkdf-sha256 instance with the key being the diffie hellman shared secret, and info parameter as
        #     `TENDERMINT_SECRET_CONNECTION_KEY_AND_CHALLENGE_GEN`
        #     - get 64 bytes of output from hkdf-sha256
        shared_secrets = hkdf_sha256(key=shared_secret, info=b'TENDERMINT_SECRET_CONNECTION_KEY_AND_CHALLENGE_GEN', length=64)
        #     - if we had the smaller ephemeral pubkey, use the first 32 bytes for the key for receiving, the second 32 bytes for sending; else the opposite.
        if ephemeral_keys[0] is ephemeral_public:
            self._recv_key, self._send_key = shared_secrets[:32], shared_secrets[32:64]
        else:
            assert ephemeral_keys[-1] is ephemeral_public
            self._send_key, self._recv_key = shared_secrets[:32], shared_secrets[32:64]
        # - use a separate nonce for receiving and sending. Both nonces start at 0, and should support the full 96 bit nonce range
        self._recv_nonce = 0
        self._send_nonce = 0
        # - all communications from now on are encrypted in 1400 byte frames (plus encoding overhead),
        #   using the respective secret and nonce. Each nonce is incremented by one after each use.
        # - we now have an encrypted channel, but still need to authenticate
        # - extract a 32 bytes challenge from merlin transcript with the label "SECRET_CONNECTION_MAC"
        self._auth_challenge = merlin_transcript.challenge_bytes(b'SECRET_CONNECTION_MAC', 32)
        # - sign the common challenge obtained from the hkdf with our persistent private key
        signature = curve_sign[key_type](private_key, self._auth_challenge, signing_digest)
        # - send the amino encoded persistent pubkey and signature to the peer
        self._send_auth = (
            key_type,
            curve_private_to_public[key_type](private_key),
            signature,
        )
        self._send_delimited(
            authsig_encode(*self._send_auth)
        )
        # - wait to receive the persistent public key and signature from the peer
        self._recv_auth = authsig_decode(
            await self._recv_delimited()
        )
        peer_key_type, peer_key, peer_signature = self._recv_auth
        # - verify the signature on the challenge using the peer's persistent public key
        assert curve_verify[peer_key_type](
            peer_key,
            peer_signature,
            self._auth_challenge,
            signing_digest,
        )
        # If this is an outgoing connection (we dialed the peer) and we used a peer ID,
        # then finally verify that the peer's persistent public key corresponds to the peer ID we dialed,
        # ie. `peer.PubKey.Address() == <ID>`.
        real_peer_id = curve_public_to_address[peer_key_type](peer_key)
        if real_peer_id != self._peer_id:
            #recv_nodeinfo = nodeinfo_decode(
            #    await self._recv_delimited()
            #)
            raise ProtocolInformation('peer gave wrong public key', real_peer_id)

        # version handshake, from tendermint/p2p/transport.go:handshake
        if 'version' in nodeinfo and 'network' in nodeinfo and 'protocol_version' in nodeinfo:
            self._send_delimited(
                nodeinfo_encode(nodeinfo)
            )
        recv_nodeinfo = nodeinfo_decode(
            await self._recv_delimited()
        )
        if not ('version' in nodeinfo and 'network' in nodeinfo and 'protocol_version' in nodeinfo and 'listen_addr' in nodeinfo):
            nodeinfo = {
                **recv_nodeinfo,
                'listen_addr': 'tcp://0.0.0.0:26656',
                **nodeinfo,
            }
            self._send_delimited(
                nodeinfo_encode(nodeinfo)
            )
        print('our nodeinfo:', nodeinfo)
        print('peer nodeinfo:', recv_nodeinfo)
        self._send_auth = [*self._send_auth, nodeinfo]
        self._recv_auth = [*self._recv_auth, recv_nodeinfo]
        assert recv_nodeinfo['default_node_id'] == real_peer_id.hex()
        assert recv_nodeinfo['protocol_version']['block'] == nodeinfo['protocol_version']['block']
        assert recv_nodeinfo['network'] == nodeinfo['network']
        if 'version' in nodeinfo:
            assert len(recv_nodeinfo.get('version','').strip())
        assert len(recv_nodeinfo['moniker'].strip())
        assert set(nodeinfo['channels']) & set(recv_nodeinfo['channels'])

        self._pong_deadline = None

        self.channels = {
            ch.ID: ch.bind(self)
            for ch in channels
            if ch.ID in recv_nodeinfo['channels']
        }

    @property
    def recv_nodeinfo(self):
        key_type, key, signature, nodeinfo = self._recv_auth
        return nodeinfo

    def close(self):
        self._writer.close()

    def _send_frame(self, data:bytes):
        assert len(data) <= 1024 # + 4 bytes for length and 16 bytes for poly 1305
        clear_frame = len(data).to_bytes(4,'little') + data + randombytes(1024 - len(data))
        cipher_frame = chacha20_encrypt(clear_frame, None, codec.nonce_encode(self._send_nonce), self._send_key)
        assert len(cipher_frame) == 4+1024+16
        self._writer.write(cipher_frame)
        self._send_nonce += 1

    async def _recv_frame(self):
        # big possible race condition with _recv_nonce, might want to use lock or queue or whatnot
        try:
            cipher_frame = await self._reader.readexactly(4+1024+16)
        except asyncio.IncompleteReadError as incomplete_read:
            if not incomplete_read.partial:
                raise SessionClosed('connection closed', self._peer_addr) from incomplete_read
            else:
                raise SessionClosed('connection broken', self._peer_addr) from incomplete_read
        except ConnectionError as error:
            raise SessionClosed('connection broken', self._peer_addr) from error
        clear_frame = chacha20_decrypt(cipher_frame, None, codec.nonce_encode(self._recv_nonce), self._recv_key)
        self._recv_nonce += 1
        datalen = int.from_bytes(clear_frame[:4], 'little')
        assert datalen <= 1024
        return clear_frame[4:4+datalen]

    def _send_delimited(self, bytes):
        bytes = codec.uvarint_encode(len(bytes)) + bytes
        if self._send_key:
            for offset in range(0, len(bytes), 1024):
                chunk = bytes[offset:offset+1024]
                self._send_frame(chunk)
        else:
            self._writer.write(bytes)

    async def _recv_delimited(self):
        if self._recv_key:
            async def aread(n):
                while n > len(self._recv_buf):
                    self._recv_buf.extend(await self._recv_frame())
                data = self._recv_buf[:n]
                self._recv_buf = self._recv_buf[n:]
                return bytes(data)
        else:
            aread = self._reader.readexactly
        return await codec.delimited_aread(aread)

    def send_ping(self):
        self._send_delimited(codec.packet_encode('ping'))
        self._pong_deadline = time.time() + 45

    def _send_pong(self):
        self._send_delimited(codec.packet_encode('pong'))

    # transfer of larger messages will eventually need more than one frame, e.g. a buffer

    def send_msg(self, chID, bytes):
        for offset in range(0, len(bytes), self._max_packet):
            payload = bytes[offset:offset+self._max_packet]
            eof = offset + self._max_packet >= len(bytes)
            self._send_delimited(
                codec.packet_encode('msg', chID, eof, payload)
            )

    #async def send_pings(self, seconds = 60):
    #    self._ping_mark = time.time()
    #    while not self._writer.is_closing():
    #        await asyncio.sleep(self._ping_mark + seconds - time.time())
    #        self.send_ping()
    #        self._ping_mark += 60
    #        if self._ping_mark < time.time() + 1:
    #            self._ping_mark = time.time() + seconds

    async def recv_msg(self, ping_timeout=60):
        while True:
            packet_fut = asyncio.Task(self._recv_delimited())
            util.async_ignore_uncaught(packet_fut)
            _, still_pending = await asyncio.wait([packet_fut], timeout=ping_timeout)
            if still_pending:
                self.send_ping()
            try:
                packet_raw = await asyncio.wait_for(packet_fut, ping_timeout)
            #    packet_raw = await packet_fut
            except TimeoutError as timeout_error:
                raise SessionClosed('no pong from peer', self._peer_addr) from timeout_error
            packet_type, msg = codec.packet_decode(packet_raw)
            if packet_type == 'packet_ping':
                self._send_pong()
            elif packet_type == 'packet_msg':
                chID, eof, new_data = msg
                if len(new_data) > self._max_packet:
                    print('configuration mismatch: raising packet payload size')
                    self._max_packet = len(new_data)
                ch = self.channels.get(chID)
                if ch is None:
                    print(self._peer_addr, 'sent msg on unrequested channel', hex(chID))
                    continue
                msg = ch.on_packet(eof, new_data)
                if msg:
                    msg_type, msg = msg
                    return ch, msg_type, msg

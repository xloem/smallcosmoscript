import asyncio, base64, copy, datetime, random, time
import tqdm

from pybft import block, channel, codec, crypto, peer_connection

import cosmospy_protobuf
from cosmospy_protobuf.cosmos.tx.v1beta1.tx_pb2 import Tx as pb_Tx
from cosmospy_protobuf.cosmos.tx.v1beta1.tx_pb2 import TxRaw as pb_TxRaw

from small import db, util

class Broadcaster:
    def __init__(self,
            #database,
            chain_id,
            digest,
            moniker='node',
            private_key = None
    ):
        self._private_key = private_key
        self._chain_id = chain_id
        self._digest = digest
        self._moniker = moniker
        self._peers = {}
        self._peer_excs = []
        self._min_fee = None
        self._min_fee_peers = {}
        self.min_fee_peer_fut = asyncio.Future()
    async def get_peer(self, addr):#id, ip = None, port = None):
        self._do_exc()
        # this isn't using threads because it might help the bulk() context manager in add_peers()
        peer = self._peers.get(addr)
        if peer is None:
            peer = await self.Peer.adbget(addr)
            print(len(peer.peers),len(peer.in_peers))
        return peer
    async def add_peer(self, addr, raise_exceptions = True):
        peer = await self.get_peer(addr)
        try:
            assert not peer.is_connected
            await peer.aconnect(self)
            self._peers[peer.addr] = peer
            return peer
        except Exception as e:
            if raise_exceptions:
                raise
            print(e)
            return None
    async def add_peers(self, *addrs):
        #peers = await asyncio.gather(*[self.get_peer(addr) for addr in addrs])
        peers = [await self.get_peer(addr) for addr in tqdm.tqdm(addrs)]
        assert not any ([peer.is_connected for peer in peers])
        excs = []
        peers_ = []
        #peers = await asyncio.gather(*[peer.aconnect(self) for peer in peers], return_exceptions=True)
        #for peer in peers:
        #    if isinstance(peer, Exception):
        #        if type(peer) in util.BUILTIN_EXCEPTIONS:
        #            raise peer
        #        excs.append(peer)
        #    else:
        #        peers_.append(peer)
        for fut in asyncio.as_completed([peer.aconnect(self) for peer in peers], timeout=None):
            try:
                peer = await fut
                peers_.append(peer)
                self._peers[peer.addr] = peer
            except Exception as exc:
                if type(exc) in util.BUILTIN_EXCEPTIONS:
                    raise exc
                excs.append(exc)
        #await asyncio.gather(*[peer.dbfuture for peer in peers])
        return peers_, excs
        #return [peer for peer in peers if not isinstance(peer, Exception)], [exc for exc in peers if isinstance(exc, Exception)]
    def close(self):
        self._do_exc()
        #for key in list(self._peers):
        #    peer = self._peers.pop(key)
        for peer in list(self._peers.values()):
            #if peer is not None and peer.is_connected():
            if True: # thinking of only holding peers if they're connected. otherwise can pull from db.
                peer.close()
    def _peer_exc(self, peer, exc):
        self._peer_excs.append([peer,peer._pump_task])
    def _do_exc(self):
        if self._peer_excs:
            peer, task = self._peer_excs.pop()
            peer.close()
        
    class Peer(db.Instance):
        addr = db.Column(str, primary = True)
        nodeinfo = db.Column(dict, migrate = lambda row, ni: [util.UNSET,ni][type(ni) is dict])
        connectstatus = db.Column(str)
        connecttime = db.Column(int)
        minfee = db.Column(int)
        minfeetime = db.Column(int)
        @staticmethod
        def _migrate_peers(row, peers = util.UNSET):
            if type(peers) is list:
                peers = {peer: 0 for peer in peers}
            elif type(peers) is object:
                peers = util.UNSET
            assert type(peers) is dict or peers is util.UNSET
            return peers
        @staticmethod
        def _merge_peers(peers, new):
            #new_peers = set(new.keys()) - set(peers.keys())
            if type(peers) is not dict or type(new) is not dict:
                print('not dict passed to merge peers!!!!!!')
                import pdb; pdb.set_trace()

            for key, time in new.items():
            #    assert peers.get(key, 0) <= time
            #peers.update(new)
                if peers.get(key,0) < time:
                    peers[key] = time

            #for new_peer in new_peers:
            #    cls.dbget(addr=new_peer, peers
            return peers
        peers = db.Column(dict, default = {}, migrate = _migrate_peers, merge = _merge_peers)
        in_peers = db.Column(dict, default = {}, migrate = _migrate_peers, merge = _merge_peers)
        #    default = {}
        #    merge = lambda old, new: list(set(old + new))
        #)
        def __str__(self):
            return f'<Peer {self.addr}>'
    
        _pump_task = None
        async def aconnect(self, broadcaster):
            print('aconnect', self.addr)
            #import pdb; pdb.set_trace()
            self.broadcaster = broadcaster
            self.transport = peer_connection.PeerConnection(self.addr)
            private_key = broadcaster._private_key or crypto.ed25519_private()
            connecttime = time.time()
            try:
                await self.transport.connect(
                    'ed25519',
                    private_key, 
                    {
                        'network': broadcaster._chain_id,
                        'moniker': broadcaster._moniker,
                    } if broadcaster._chain_id else {
                        'moniker': broadcaster._moniker,
                    },
                    [
                        channel.ChannelPex(), # peers
                        channel.ChannelConsensusState(), # to indicate we can receive mempool txs, and to learn the latest block
                        channel.ChannelMempool(), # txs
                        channel.ChannelConsensusData(), # blocks
                    ],
                    hashlib.sha256,
                    codec.authsig_encode, codec.authsig_decode,
                    codec.nodeinfo_encode, codec.nodeinfo_decode
                )
                self.connecttime = connecttime
                self.pex = self.transport.channels.get(channel.ChannelPex.ID)
                self.state = self.transport.channels.get(channel.ChannelConsensusState.ID)
                self.mempool = self.transport.channels.get(channel.ChannelMempool.ID)
                self.data = self.transport.channels.get(channel.ChannelConsensusData.ID)
                if self.pex is None and self.mempool is None:
                    self.transport.close()
                    raise peer_connection.ProtocolInformation('peer has neither pex nor mempool')
                self.connectstatus = True
            except peer_connection.ProtocolInformation as pi:
                self.connecttime = connecttime
                self.connectstatus = [pi.args[0], type(pi.__cause__).__name__, *(pi.__cause__.args if pi.__cause__ else [])]
                print(self.addr[:5],'connect fail',self.connectstatus)
                await self.adbput()
                raise
            self.peer_queue = asyncio.Queue()
            self.mempool_futs = {}
    
            self._peer_timestamp = time.time()

            self.nodeinfo = self.transport.recv_nodeinfo
            #import pdb; pdb.set_trace()
            print('aconnect putting', self.addr)
            await self.adbput()
            print('aconnect done put', self.addr)
            self._pump_task = asyncio.Task(self._pump())
            if self.pex is not None:
                self.pex.send('pex_request')
            return self
        @property
        def is_connected(self):
            if self._pump_task is None:
                return False
            if self._pump_task.done():
                exc = self._pump_task.exception()
                if exc is not None:
                    raise exc from exc
                self._pump_task.result()
                return False
            return True
        async def wait(self):
            if self._pump_task is not None:
                return await self._pump_task
        def close(self):
            if self._pump_task is not None:
                if not self._pump_task.done():
                    #self._pump_task.cancel()
                    self.transport.close()
                else:
                    self._pump_task.result()
        async def mempool_tx(self, txhash):
            assert txhash not in self.mempool_futs
            try:
                return await self.mempool_futs.setdefault(txhash, asyncio.Future())
            finally:
                del self.mempool_futs[txhash]
        async def _pump(self):
          try:
            first_gossip_block = None
            gossip_txs = set()
            blocks = block.Index.get_index(self.nodeinfo['network'], cosmospy_protobuf)
            state = None
            msgct = 0
            txct = 0
            start_time = time.time()
            mintxct = 0
            mintxfee = None
            msg_type = ''
            pending_futs = []
            while True:
                if mintxfee != self.broadcaster._min_fee:
                    mintxct = 0
                if msg_type != 'has_vote':
                    print(self.addr[:5], self.nodeinfo['network'], self.nodeinfo['moniker'], 'peer waiting', mintxct, '/', txct, '/', msgct)
                ch, msg_type, msg = await self.transport.recv_msg()
                now = time.time()
                msgct += 1
                assert not any(fut.cancelled() for fut in pending_futs)
    
                if self.state is not None and ch is self.state and msg_type == 'new_round_step':
                    print(self.addr[:5], 'peer state gossip')
                    state = msg
                    ch.send(msg_type, **state)
                elif self.state is not None and ch is self.state and msg_type == 'new_valid_block':
                #    # {'height': '16720973', 'block_part_set_header': {'total': 1, 'hash': 'yJwkX/8bE0tRthfd0wPqrznzEyyzkivGg47QYdBZwHM='}, 'block_parts': {'bits': '1', 'elems': ['1']}, 'round': 0, 'is_commit': False}
                    blocks.new(msg)
                elif self.data is not None and ch is self.data and msg_type == 'block_part':
                    # ChannelConsensusData block_part
                    # {'height': '16733594', 'part': {'bytes': base64data, 'proof': {'total': '1', 'leaf_hash': 'H/327kxpAs78jiSQtJQ3l7487+r7BROnbjAbnntaIJE=', 'index': '0', 'aunts': []}, 'index': 0}, 'round': 0}
                    blk = blocks.add_part(msg)
                    if blk is not None:
                        print(self.addr[:5], blk.height, 'block gossiped with txlen =', len(blk.txs))
                        # - if this is first received block, store the height
                        if first_gossip_block is None:
                            first_gossip_block = blk
                        # - if no txs are gossiped after so many blocks seen, and this block has a tx, consider peer to not be gossiping txs
                        if not self.gossip_txs and blk.proto.header.time.seconds - first_gossip_block.proto.header.time.seconds > 60 * 5 and blk.txs:
                            raise PritocolInformation('no txs gossiped')
                        for txhash, remote_tx in blk.txs.items():
                            try:
                                gossip_txs.remove(txhash)
                                # - if a tx has been gossiped and then mined, start tracking what is gossiped. eventually simplifies into 2 items above.
                            except KeyError:
                                pass
                            fee = sum([int(coin.amount) for coin in remote_tx.auth_info.fee.amount])
                elif self.pex is not None and ch is self.pex and msg_type == 'pex_addrs':
                    self._peer_timestamp = now
                    print(self.addr[:5], 'peer pex')# truncated for dbg')
                    random.shuffle(msg['addrs'])
                    peers = { #[codec.addr_encode(**addr) for addr in msg['addrs']]
                        codec.addr_encode(**addr): now
                             for addr in msg['addrs']#[:8]
                    }
                    new_peers = set(peers.keys()) - set(self.peers.keys() if type(self.peers) is dict else self.peers)
                    self.peers = self._merge_peers(self.peers, peers)
                    if new_peers:
                        #new_peers = [self.dbget(addr = new_peer, in_peers = {self.addr:now}).dbfuture for new_peer in new_peers]
                        pending_futs.extend([asyncio.Task(self.adbget(addr = new_peer, in_peers = {self.addr:now})) for new_peer in new_peers])
                        #self.peers[:0] = new_peers
                    print(self.addr[:5], 'peer pex to db')
                    #await asyncio.gather(self.adbput(), *new_peers)
                    pending_futs.append(asyncio.Task(self.adbput()))
                            #self.broadcast.get_peer(new_peer)
                    #for addr in msg['addrs']:
                    #    addr = codec.addr_encode(addr)
                        #peer = await self.broadcast.get_peer(addr.id)
                        #if addr.ip != peer.ip or addr.port != peer.port:
                        #    peer.ip = addr.ip
                        #    peer.port = addr.port
                        #    updated_peers.append(peer)
                        #pair = [self.id, peer.id]
                        #pair.sort()
                        #pair.append(now)
                        #pairs.append(pair)
    
                elif self.mempool is not None and ch is self.mempool and msg_type == 'txs':
                    txct += 1
                    remote_txs = msg['txs']
                    print(self.addr[:5], 'peer tx gossip')
                    for remote_tx in remote_txs:
                        remote_tx = base64.b64decode(remote_tx.encode())
                        txhash = hashlib.sha256(remote_tx).hexdigest()
                        gossip_txs.add(txhash)
                        remote_tx = pb_Tx.FromString(remote_tx)
                        #remote_tx = pb_TxRaw.FromString(remote_tx)
                        #remote_tx = pb_Tx.FromString(remote_tx.body_bytes)
                        fee = sum([int(coin.amount) for coin in remote_tx.auth_info.fee.amount])
                        print(self.addr[:5], fee, self.minfee, self.broadcaster._min_fee)
                        if self.minfee is None or type(self.minfee) is db.Column or self.minfee >= fee:
                            print(self.addr[:5], 'new peer-specific minfee', fee)
                            self.minfee = fee
                            self.minfeetime = now
                            pending_futs.append(asyncio.Task(self.adbput()))
                            #await self.adbput()
                        if self.broadcaster._min_fee is None or fee < self.broadcaster._min_fee:
                            self.broadcaster._min_fee = fee
                            self.broadcaster._min_fee_peers = {self}
                            print(self.addr, txhash, fee)
                            mintxct += 1
                            mintxfee = fee
                            if fee == self.minfee:
                                raise peer_connection.SessionClosed(f'new minfee {fee}')
                        elif fee == self.broadcaster._min_fee:
                            mintxfee = fee
                            mintxct += 1
                            if len(self.broadcaster._min_fee_peers) < 8 or fee:
                                self.broadcaster._min_fee_peers.add(self)
                                print(len(self.broadcaster._min_fee_peers), self.addr, txhash, fee)
                            elif not self.broadcaster.min_fee_peer_fut.done():
                                self.broadcaster._min_fee_peers.pop()
                                self.broadcaster._min_fee_peers.add(self)
                                self.broadcaster.min_fee_peer_fut.set_result(self.broadcaster._min_fee_peers)
                                print('found enough min peers')
                            if fee == self.minfee:
                                raise peer_connection.SessionClosed(f'new minfee {fee}')
                            
                        if txhash in self.mempool_futs:
                            self.mempool_futs[txhash].set_result([self.addr, state, remote_tx])
                    #for local_tx, fut in self.mempool_futs.items():
                    #    if local_tx is None or local_tx in remote_txs:
                    #        fut.set_result([self.addr, state, local_tx or remote_txs[0]])
                elif msg_type != 'has_vote':
                    print(self.addr[:5], 'peer unk', type(ch).__name__, msg_type)
                    #import pdb; pdb.set_trace()
                    #print(self.addr[:5], msg)
                assert not any(fut.cancelled() for fut in pending_futs)
    
                if self.pex is not None and self._peer_timestamp is not None and now > self._peer_timestamp + 10:
                    print(self.addr[:5], 'requesting pex')
                    self._peer_timestamp = None
                    self.pex.send('pex_request')
                #print(self.addr[:5], 'iterating pending futs', pending_futs)
                assert not any(fut.cancelled() for fut in pending_futs)
                [fut.result() for fut in pending_futs if fut.done()]
                pending_futs = [fut for fut in pending_futs if not fut.done()]
          except peer_connection.SessionClosed as session_closed:
              print(self.addr[:5], 'peer closed', session_closed)
              self.close()
          except Exception as exc:
              print(self.addr[:5], 'peer exc', exc)
              self.broadcaster._peer_exc(self, exc)
              raise exc
          finally:
              if self.addr in self.broadcaster._peers:
                  del self.broadcaster._peers[self.addr]
              try:
                  await self.adbput()
              except Exception as exc:
                  self.broadcaster._peer_exc(self, exc)
                  raise exc
          return True


if __name__ == '__main__':
    import hashlib, contextlib
    async def amain():

        import os
        if 'libproxychains4.so' in os.environ.get('LD_PRELOAD',''): # proxychains ignores nonblocking connect
            loop = asyncio.get_running_loop()
            orig_sock_connect = loop._sock_connect
            def background_sock_connect(fut, sock, address):
                fut.__task = asyncio.Task(util.async_in_thread(orig_sock_connect, fut, sock, address))
            loop._sock_connect = background_sock_connect

        obj = Broadcaster(
        #    peewee.SqliteDatabase('pybft.db'),
            None,#'cosmoshub-4',
            hashlib.sha256
        )
        #peeraddr = '9edd51012df3a09395a48eb68a84723d6308e08c@cabnmnb7rmf6kasmhv00.bdnodes.net:26656'
        #peeraddr = '0e4e52ded073c04726627d4ee9abded18e994bea@95.217.202.49:28656'
        #initial_peer = await obj.add_peer(peeraddr)
        initial_peers = [
 #           '9f68e8cd5f1def9f9d6f523fb8d2b10023a0fb7a@150.136.124.196:26656',
#            '0e4e52ded073c04726627d4ee9abded18e994bea@95.217.202.49:28656',
 #          'b366e8610713700ef5251d984a00c65b461056a1@89.149.218.110:26656',
#            'bcc31cf6af907b163b0ec591f1314796862fece4@129.213.158.8:26656',
            '199557c7769ba1469af0b171c816bb69267cd3b7@65.109.82.144:28656',
            '99da322d4038c4f6281e59bb15dda20058471e06@5.78.101.137:26656',
            '9edd51012df3a09395a48eb68a84723d6308e08c@cabnmnb7rmf6kasmhv00.bdnodes.net:26656',
            '4ebf074e8b4a24438bd0bd503b62b4728dfb8eae@35.212.101.35:26656',
            '213857e741833d17275ea559bb2d0342398cec99@35.245.206.45:26656',
            '3ddcd917b078dc61d2ff6b7281996acde35ad4fb@141.95.202.13:26656',
        ]
        #for initial_peer in initial_peers:
        #    await obj.add_peer(initial_peer)
        peers, excs = await obj.add_peers(*initial_peers)
        #    initial_peer = await initial_peer_task
        #    if initial_peer:
        #        break
        #for peer in more_peers:
        #    await obj.add_peer(peer)
        with contextlib.closing(obj):
            #import pdb; pdb.set_trace()
            #peer_addrs = await initial_peer.peers()
            #print(peer_addrs)
            #peer_objs = await obj.add_peers(*peer_addrs)
            #print('connected to initial peer')
            ##await asyncio.gather(*initial_peer_tasks)
            #await initial_peer_tasks[0]
            #await initial_peer_tasks[1]
            #await initial_peer_tasks[2]
            #print('connected to initial peers')
            polled_peers = set()
            while not obj.min_fee_peer_fut.done() and len(obj._peers):
                peerset = list(obj._min_fee_peers or obj._peers.values())
                for peer in peerset:
                    #if peer is None:# or type(peer) is asyncio.Task:
                    #    continue
                    #if peer in polled_peers:
                    #    peer.close()
                    #    try:
                    #        await peer.aconnect(peer.broadcaster, peer.addr)
                    #    except:
                    #        continue
                    #else:
                    #    polled_peers.add(peer)
                    #peer_peers = await peer.peers()
                    new_peers = set(peer.peers) - polled_peers#set([addr for addr in obj._peers.keys()])
                    new_peers = list(new_peers)
                    random.shuffle(new_peers)
                    new_peers = new_peers[:3]
                    polled_peers.update(new_peers)
                    #for peer in new_peers:
                    #    peer = await obj.add_peer(peer, raise_exceptions=False)
                    await obj.add_peers(*new_peers)
                    #peer_tasks.extend(obj.add_peers(*new_peers))
                await asyncio.sleep(1)
            #print('connecting to lots of peers')
            
            #print(len(peer_objs))
            #print(await obj._peers[peeraddr].mempool_tx())
            #min_fee_peers = await obj.min_fee_peer_fut
            #print('found 8 min peers')
            #print(min_fee_peers)
            min_fee_peers = obj._min_fee_peers
            print({peer.addr:(peer.minfee, datetime.datetime.fromtimestamp(peer.minfeetime).isoformat()) for peer in min_fee_peers})
            #await obj._peers[0].wait()
    #loop = asyncio.get_event_loop()
    #loop.run_until_complete(amain())
    asyncio.run(amain())
    #loop.close()

try:
    try:
        from pysodium import (
            crypto_scalarmult_curve25519_base as x25519_private_to_public,
            crypto_scalarmult_curve25519 as x25519_private_peer_to_shared,
            crypto_aead_chacha20poly1305_ietf_encrypt as chacha20_encrypt,
            crypto_aead_chacha20poly1305_ietf_decrypt as chacha20_decrypt,
            randombytes,
        )
    except:
        from nacl.bindings import ( # note this also wraps libsodium, not nacl
            crypto_scalarmult_base as x25519_private_to_public,
            crypto_scalarmult as x25519_private_peer_to_shared,
            crypto_aead_chacha20poly1305_ietf_encrypt as chacha20_encrypt,
            crypto_aead_chacha20poly1305_ietf_decrypt as chacha20_decrypt,
            randombytes,
        )
except:
    from x25519 import ( # pure python
        scalar_base_mult as x25519_private_to_public,
        scalar_mult as x25519_private_peer_to_shared,
    )
    assert not "all the pure python modules used"
import hashlib, hmac

import ecdsa # copied this quickly. pure python, recommends to use a binary wrapper instead such as pyca/cryptography

def x25519_private():
    return ed25519_private()

def secp256k1_private():
    return ecdsa.SigningKey.generate(curve=ecdsa.SECP256k1).to_string()

def ed25519_private():
    return ecdsa.SigningKey.generate(curve=ecdsa.Ed25519).to_string()

def hkdf_sha256(key:bytes, info:bytes, length:int, salt:bytes = b'\0' * 32):
    PRK = hmac.digest(key=salt, msg=key, digest=hashlib.sha256)
    N = (length + 31) // 32 # ceil(length/hashlen)
    T = [b'']
    while len(T) <= N:
        T.append(
            hmac.digest(key=PRK, msg = T[-1] + info + bytes([len(T)]), digest=hashlib.sha256)
        )
    return b''.join(T)[:length]

def secp256k1_private_to_public(private_key:bytes):
    private_obj = ecdsa.SigningKey.from_string(private_key, curve=ecdsa.SECP256k1)
    public_obj = private_obj.get_verifying_key()
    raw = False
    return bytes(public_obj.to_string("raw") if raw else public_obj.to_string("compressed"))

def secp256k1_public_to_address(public_key:bytes):
    sha = hashlib.sha256(public_key).digest()
    return hashlib.new('ripemd160', sha).digest()

def secp256k1_sign(private_key:bytes, data:bytes, digest):
    private_obj = ecdsa.SigningKey.from_string(private_key, curve=ecdsa.SECP256k1)
    return bytes(private_obj.sign_deterministic(data, hashfunc=digest, sigencode=ecdsa.util.sigencode_string_canonize))

def secp256k1_verify(public_key:bytes, signature:bytes, data:bytes, digest):
    public_obj = ecdsa.VerifyingKey.from_string(public_key, curve=ecdsa.SECP256k1)
    return public_obj.verify(signature, data, hashfunc=digest, sigdecode=ecdsa.util.sigdecode_string)

def ed25519_private_to_public(private_key:bytes):
    private_obj = ecdsa.SigningKey.from_string(private_key, curve=ecdsa.Ed25519)
    public_obj = private_obj.get_verifying_key()
    raw = False
    return bytes(public_obj.to_string("raw") if raw else public_obj.to_string("compressed"))

def ed25519_public_to_address(public_key:bytes):
    return hashlib.sha256(public_key).digest()[:20]

def ed25519_sign(private_key:bytes, data:bytes, digest):
    private_obj = ecdsa.SigningKey.from_string(private_key, curve=ecdsa.Ed25519)
    return bytes(private_obj.sign_deterministic(data, hashfunc=digest, sigencode=ecdsa.util.sigencode_string_canonize))

def ed25519_verify(public_key:bytes, signature:bytes, data:bytes, digest):
    public_obj = ecdsa.VerifyingKey.from_string(public_key, curve=ecdsa.Ed25519)
    return public_obj.verify(signature, data, hashfunc=digest, sigdecode=ecdsa.util.sigdecode_string)

curve_private_to_public = dict(
    secp256k1 = secp256k1_private_to_public,
    ed25519 = ed25519_private_to_public,
)

curve_public_to_address = dict(
    secp256k1 = secp256k1_public_to_address,
    ed25519 = ed25519_public_to_address,
)

curve_sign = dict(
    secp256k1 = secp256k1_sign,
    ed25519 = ed25519_sign,
)

curve_verify = dict(
    secp256k1 = secp256k1_verify,
    ed25519 = ed25519_verify,
)

if __name__ == '__main__':
    private = b'1' * 32
    public = x25519_private_to_public(private)
    print(public.hex())
    assert public.hex() == '04f5f29162c31a8defa18e6e742224ee806fc1718a278be859ba5620402b8f3a'
    peer = b'2' * 32
    shared = x25519_private_peer_to_shared(private, peer)
    print(shared.hex())
    assert shared.hex() == 'a6d830c3561f210fc006c77768369af0f5b3e3e502e74bd3e80991d7cb7bfa50'
    key = bytes.fromhex('0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b')
    info = bytes.fromhex('f0f1f2f3f4f5f6f7f8f9')
    derived = hkdf_sha256(key, info, 64)
    print(derived.hex())
    assert derived.hex() == 'abbafb13f5c1bc489d4203135817956dd521b39e3bd61d1cc85cef884d1f8e2e2ca9c19f23df620dd394b45cb724b6a13b65f2be0e062b21837ac04ce8b9c037'


class Channel:
    ID = None
    msg_encode = None
    msg_decode = None
    _conn = None
    
    def bind(self, connection):
        assert self._conn == None
        self._conn = connection
        self._recv_data = bytearray()
        return self
    def on_packet(self, eof:bool, new_data:bytes):
        data = self._recv_data
        data.extend(new_data)
        if eof:
            data = bytes(data)
            self._recv_data = bytearray()
            return self.on_message(data)
    def on_message(self, bytes):
        msg = self.msg_decode(bytes)
        #print(type(self).__name__, msg)
        return msg
    def send(self, *params, **kwparams):
        bytes = self.msg_encode(*params, **kwparams)
        self._conn.send_msg(self.ID, bytes)

class ChannelGenericProto(Channel):
    pb_Message = None

    def msg_encode(self, message_type, **kwparams):
        from google.protobuf.json_format import ParseDict
        message = ParseDict(
            { message_type: kwparams },
            self.pb_Message(),
            # ignore_unknown_fields = True,
        )
        return message.SerializeToString()

    def msg_decode(self, bytes):
        from google.protobuf.json_format import MessageToDict
        message = self.pb_Message.FromString(bytes)
        type = message.WhichOneof('sum')
        data = MessageToDict(
            getattr(message, type),
            including_default_value_fields=True,
            preserving_proto_field_name=True,
        )
        return type, data

class ChannelPex(ChannelGenericProto):
    '''A channel for PEX messages'''
    ID = 0x00
    from cosmospy_protobuf.tendermint.p2p.pex_pb2 import Message as pb_Message

class _ChannelConsensus(ChannelGenericProto):
    from cosmospy_protobuf.tendermint.consensus.types_pb2 import Message as pb_Message
class ChannelConsensusState(_ChannelConsensus):
    ID = 0x20
class ChannelConsensusData(_ChannelConsensus):
    ID = 0x21
class ChannelConsensusVote(_ChannelConsensus):
    ID = 0x22
class ChannelConsensusVoteSetBits(_ChannelConsensus):
    ID = 0x23

class ChannelMempool(ChannelGenericProto):
    ID = 0x30
    from cosmospy_protobuf.tendermint.mempool.types_pb2 import Message as pb_Message

class ChannelEvidence(ChannelGenericProto):
    ID = 0x38
    from cosmospy_protobuf.tendermint.types.evidence_pb2 import Evidence as pb_Message

class ChannelBlockchain(ChannelGenericProto):
    '''A channel for blocks and status updates (`BlockStore` height)'''
    ID = 0x40
    from cosmospy_protobuf.tendermint.blockchain.types_pb2 import Message as pb_Message

class _ChannelConsensus(ChannelGenericProto):
    from cosmospy_protobuf.tendermint.statesync.types_pb2 import Message as pb_Message
class ChannelConsensusSnapshot(_ChannelConsensus):
    '''Exchanges snapshot metadata'''
    ID = 0x60
class ChannelConsensusChunk(_ChannelConsensus):
    '''Exchanges chunk contents'''
    ID = 0x61

def all():
    return [
        #name[len('Channel'):].lower():
        ChannelType()
        for name, ChannelType in globals().items()
        if
            name[0] != '_' and
            type(ChannelType) is type and
            ChannelType is not ChannelGenericProto and
            issubclass(ChannelType, ChannelGenericProto)
            
    ]

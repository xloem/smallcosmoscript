
# it is not complex to inline the 2 authentication protobufs
def bytes_encode(bytes):
    from google.protobuf.wrappers_pb2 import BytesValue as pb2_BytesValue
    return pb2_BytesValue(value=bytes).SerializeToString()

def bytes_decode(bytes):
    from google.protobuf.wrappers_pb2 import BytesValue as pb2_BytesValue
    return pb2_BytesValue.FromString(bytes).value

def authsig_encode(key_type, key, signature):
    from cosmospy_protobuf.tendermint.p2p.conn_pb2 import AuthSigMessage as pb2_AuthSigMessage
    return pb2_AuthSigMessage(
        pub_key = {
            key_type: key
        },
        sig = signature,
    ).SerializeToString()

def authsig_decode(bytes):
    from cosmospy_protobuf.tendermint.p2p.conn_pb2 import AuthSigMessage as pb2_AuthSigMessage
    authsig = pb2_AuthSigMessage.FromString(bytes)
    key_type = authsig.pub_key.WhichOneof('sum')
    key = getattr(authsig.pub_key, key_type)
    signature = authsig.sig
    return key_type, key, signature

def nodeinfo_encode(dict):
    from cosmospy_protobuf.tendermint.p2p.types_pb2 import DefaultNodeInfo as pb2_NodeInfo
    return pb2_NodeInfo(**dict).SerializeToString()

def nodeinfo_decode(bytes):
    from cosmospy_protobuf.tendermint.p2p.types_pb2 import DefaultNodeInfo as pb2_NodeInfo
    nodeinfo = pb2_NodeInfo.FromString(bytes)
    return {
        descriptor.name:
            {
                subdesc.name: subval
                for subdesc, subval in value.ListFields()
            } if descriptor.message_type else value
        for descriptor, value in nodeinfo.ListFields()
    }

def packet_encode(packet_type, channel_id=-1, eof=True, data=None):
    from cosmospy_protobuf.tendermint.p2p.conn_pb2 import Packet as pb2_Packet, PacketMsg as pb2_PacketMsg
    if packet_type == 'ping':
        return pb2_Packet(packet_ping={}).SerializeToString()
    elif packet_type == 'pong':
        return pb2_Packet(packet_pong={}).SerializeToString()
    elif packet_type == 'msg':
        return pb2_Packet(
            packet_msg = pb2_PacketMsg(
                channel_id = channel_id,
                eof = eof,
                data = data,
            )
        ).SerializeToString()

def packet_decode(bytes):
    from cosmospy_protobuf.tendermint.p2p.conn_pb2 import Packet as pb2_Packet
    packet = pb2_Packet.FromString(bytes)
    packet_type = packet.WhichOneof('sum')
    if packet_type == 'packet_msg':
        packetmsg = packet.packet_msg
        return packet_type, [packetmsg.channel_id, packetmsg.eof, packetmsg.data]
    else:
        return packet_type, None

def delimited_encode(bytes):
    return uvarint_encode(len(bytes)) + bytes

def delimited_decode(bytes):
    length, offset = uvarint_decode(bytes, True)
    tail = length + offset
    assert tail == len(bytes)
    return bytes[offset:tail]

async def delimited_aread(aread):
    return await aread(await uvarint_aread(aread))

# there are probably faster google protobuf functions to read and write uvarints
def uvarint_encode(uint:int):
    buf = bytearray()
    while uint >= 0x80:
        buf.append(uint & 0x7f | 0x80)
        uint >>= 7
    buf.append(uint)
    return buf

def uvarint_decode(bytes, trailing_data=False):
    idx = 0
    uint = 0
    byte = 0x80
    while byte >= 0x80:
        byte = bytes[idx]
        uint |= (byte & 0x7f) << (idx*7)
        idx += 1
    assert trailing_data or idx == len(bytes)
    if trailing_data:
        return uint, idx
    else:
        return uint

async def uvarint_aread(aread):
    buf = bytearray(await aread(1))
    while buf[-1] >= 0x80:
        buf.extend(await aread(1))
    return uvarint_decode(buf)

def addr_encode(id, ip, port):
    return f'{id}@{ip}:{port}'

def addr_decode(addr):
    id, host = addr.split('@',1)
    #id = bytes.fromhex(id)
    host, port = host.rsplit(':',1)
    port = int(port)
    return id, host, port

def addr_to_dict(addr):
    id, host, port = addr_decode(addr)
    return dict(id=id, host=host, port=port)

def nonce_encode(nonce):
    # tendermint originally used little-endian 64-bit nonce in the high bytes of the 96-bit aead nonce.
    assert nonce >= 0 and nonce <= 0xffffffffffffffff
    return (nonce << 32).to_bytes(12, 'little')

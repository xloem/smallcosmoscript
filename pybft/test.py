import asyncio, hashlib

from .crypto import ed25519_private, ed25519_private_to_public, ed25519_public_to_address
from . import codec
from . import channel
from .peer_connection import PeerConnection

peer_queue = asyncio.Queue()
seen_addrs = set()

async def dopeer(addr):
    transport = PeerConnection(addr)
    private_key = ed25519_private()
    #node_id = ed25519_public_to_address(
    #    ed25519_private_to_public(private_key)
    #).hex()
    channels = channel.all()
    nodeinfo = {
        #'protocol_version': {
        #    'block': 11 # matching block version
        #},
        #'listen_addr': 'tcp://0.0.0.0:26656', # parseable
        #'network': 'cosmoshub-4', # matching network
        #'version': '0.34.28', # any tab-free ascii
        'moniker': 'node', # nonempty tab-free ascii
    }
    try:
        await transport.connect(
            'ed25519', private_key, nodeinfo,
            channels,
            hashlib.sha256,
            codec.authsig_encode, codec.authsig_decode,
            codec.nodeinfo_encode, codec.nodeinfo_decode,
        )
    except ConnectionRefusedError:
        return
    
    pex = transport.channels[channel.ChannelPex.ID]
    #pex.send('pex_request')
    while True:
        ch, msg_type, msg = await transport.recv_msg()
        if type(ch) is channel.ChannelConsensusState and msg_type == 'new_round_step':
            # inform the peer we are up-to-date by mirroring their state
            ch.send(msg_type, **msg)
        if msg_type not in ['new_round_step','has_vote','vote','vote_set_maj23']:
            print(type(ch).__name__, ch.ID, hex(ch.ID), msg_type, msg)
        if ch is pex and msg_type == 'pex_addrs':
            break
    addrs = [codec.addr_encode(**addr) for addr in msg['addrs']]
    print(addrs)
    #import pdb; pdb.set_trace()
    new_addrs = set(addrs) - seen_addrs
    seen_addrs.update(new_addrs)
    await peer_queue.put(new_addrs)
    #print(addrs)
    ch, msg_type, msg = await transport.recv_msg()
    print(addr, type(ch).__name__, msg_type, msg)
    import pdb; pdb.set_trace()
    print(addr, type(ch).__name__, msg_type, msg)


async def amain():
    loop = asyncio.get_running_loop()
    orig_sock_connect = loop._sock_connect
    def background_sock_connect(fut, sock, address):
        fut.__task = asyncio.Task(asyncio.to_thread(orig_sock_connect, fut, sock, address))
    loop._sock_connect = background_sock_connect
#._sock_connect(fut, sock, address)

    peeraddr = '9edd51012df3a09395a48eb68a84723d6308e08c@cabnmnb7rmf6kasmhv00.bdnodes.net:26656'
    await peer_queue.put([peeraddr])
    seen_addrs.update([peeraddr])
    tasks = []
    #while True:
    if True:
        peers = await peer_queue.get()
        tasks.extend([asyncio.Task(dopeer(addr)) for addr in peers])
    await asyncio.gather(*tasks)

asyncio.run(amain())

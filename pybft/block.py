import base64, hashlib, weakref

class Index:
    indices = dict()
    def __init__(self, network):
        assert network not in self.indices
        self.indices[network] = self
        self.network = network
        #self.by_height = dict()
        self.by_height = weakref.WeakValueDictionary()
        self.incomplete = set()
        self.first_block = None
    def set_protobuf(self, protobuf):
        if protobuf is not None:
            self.pb_Block = protobuf.tendermint.types.block_pb2.Block
            self.pb_TxRaw = protobuf.cosmos.tx.v1beta1.tx_pb2.TxRaw
            self.pb_Tx = protobuf.cosmos.tx.v1beta1.tx_pb2.Tx
        assert hasattr(self, 'pb_Block')
        return self
    @classmethod
    def get_index(cls, network, protobuf = None):
        index = cls.indices.get(network) or cls(network)
        return index.set_protobuf(protobuf)
    class NetworkBlock:
        pass
    def new(self, msg):
        block = self.by_height.get(msg['height'])
        if block is None:
            block = self.NetworkBlock()
            self.incomplete.add(block)
            block.index = self
            block.proto = None
            block.height = msg['height']
            if self.first_block is None:
                self.first_block = block.height
            setresult = self.by_height.setdefault(block.height, block)
            assert setresult is block
            partset = msg['block_part_set_header']
            block.hash = partset['hash']
            block.parts = [None] * partset['total']
            block.round = msg['round']
            parts = msg['block_parts']
            # is_commit
        assert block.height == msg['height']
        assert block.round == msg['round']
        assert block.hash == msg['block_part_set_header']['hash']
        assert len(block.parts) == msg['block_part_set_header']['total']
        return block
    def add_part(self, msg):
        height = msg['height']
        #if not self.first_block or self.first_block >= height:
        #    return
        if height not in self.by_height:
            return
        #import pdb; pdb.set_trace()
        # {'height': '16733594', 'part': {'bytes': base64data, 'proof': {'total': '1', 'leaf_hash': 'H/327kxpAs78jiSQtJQ3l7487+r7BROnbjAbnntaIJE=', 'index': '0', 'aunts': []}, 'index': 0}, 'round': 0}
        block = self.by_height[height]
        assert msg['round'] == block.round
        part = msg['part']
        index = part['index']
        bytes = base64.b64decode(part['bytes'].encode())
        if block.parts[index] is None:
            block.parts[index] = bytes
        else:
            assert block.parts[index] == bytes
        partct = sum([bytes is not None for bytes in block.parts])
        print(f'{height} {partct}/{len(block.parts)}')
        if partct >= len(block.parts):
            if block.proto is None:
                self.incomplete.discard(block)
                proto = self.pb_Block.FromString(b''.join(block.parts))
                block.proto = proto
                #from google.protobuf.json_format import MessageToDict
                #json = MessageToDict(
                #    proto,
                #    including_default_value_fields=True,
                #    preserving_proto_field_name=True,
                #)
                txs = {}
                for tx_bytes in proto.data.txs:
                    txid = hashlib.sha256(tx_bytes).hexdigest()
                    tx = self.pb_Tx.FromString(tx_bytes)
                    #tx = self.pb_TxRaw.FromString(tx_bytes)
                    #tx = self.pb_Tx.FromString(tx.body_bytes)
                    txs[txid] = tx
                block.txs = txs
            return block
        else:
            return None

from contextlib import contextmanager
from decimal import Decimal
from datetime import datetime
from time import sleep

from cosmpy2 import LedgerClient, NetworkConfig, get_paginated
from cosmpy.aerial.wallet import LocalWallet
from cosmpy.bank.rest_client import QueryBalanceRequest
from cosmpy.tendermint.rest_client import CosmosBaseTendermintRestClient, GetBlockByHeightRequest
from cosmpy.staking.rest_client import QueryDelegatorDelegationsRequest, QueryValidatorsRequest
from cosmpy.distribution.rest_client import QueryDelegationRewardsRequest

wallet = LocalWallet.from_mnemonic(open('omniflix.wallet').read().strip(), 'omniflix')
address = wallet.address()
cfg = NetworkConfig('omniflixhub-1', 0, 'uflix', 'uflix', url='rest+http://127.0.0.1:1317')
client = LedgerClient(cfg, api_timeout_secs=5)

PRICE_CENTS = 10

def show(data, line):
    if len(data) < 2:
        return
    height, time, balance = data[-1]
    line = f'{height},{datetime.fromtimestamp(time).isoformat()},{balance*PRICE_CENTS/100}'
    import math
    logdata = [
        (time, math.log(balance))
        for height, time, balance in data[-1024:]
    ]

    logyavg = sum([y for x, y in logdata]) / len(logdata)
    logxavg = sum([x for x, y in logdata]) / len(logdata)

    #x0, logy0 = logdata[0]
    #xf, logyf = logdata[-1]
    #logm = (logyf - logy0) / (xf - x0) # underestimate, assumes no significant balance activity (endpoints only)

    #logmavg = sum([(y - logyavg) / (x - logxavg) for x, y in logdata]) / len(logdata) # appears poor
    #logbavg = logyavg - logmavg * logxavg

    logm = sum([x * (y - logyavg) for x,y in logdata]) / sum([x * (x - logxavg) for x,y in logdata]) # overestimate (least squares)

    #y = m * x + b
    #b = y - m * x
    logb = logyavg - logm * logxavg
    #m * x = y - b
    #y = m * x + b
    #y = exp(m * x + b)
    #y = exp(m * x) * exp(b)
    #y = exp(b) * exp(m) ** x
    #y = coeff * base ** x
    coeff = math.exp(logb)
    base = math.exp(logm)
    #coeff = math.exp(logbavg)
    #base = math.exp(logmavg)
        # i am noting that any particular balance, with an exponential growth, can be mapped back to a balance near a time
        # in the past, where it was very small.
        # here, there is some arbitrary value at x=1 that makes the curve work out. an imaginary starting past value.
      # base would now be in seconds
    weekly = base ** (60*60*24*7)
    wpr = (weekly - 1) * 100
    td = math.log(2) / math.log(base) / (60*60*24)
        # let's calculate the std deviation?
    print(f'{line},wpr={wpr}%,Td={math.ceil(td)}d', flush=True)

fn = f'{address}.csv'
try:
    csv =  open(fn, 'r+')
except FileNotFoundError:
    csv =  open(fn, 'w+')
data = [line.strip().split(',') for line in csv]
data = [
    (int(height), datetime.fromisoformat(date).timestamp(), Decimal(balance))
    for idx, (height, date, balance) in enumerate(data)
]
data.sort()
tip_height = client.query_height()
if len(data):
    reverse = False
    height, _, last_balance = data[-1]
    last_balance *= 1000000
else:
    reverse = True
    height = tip_height
    last_balance = None
while True:
    if reverse:
        height -= 1
    else:
        height += 1
        while height > tip_height - 1:
            sleep(5)
            tip_height = client.query_height()
    block = client.query_block(height)
    height_client = client.for_height(height)
    assert int(block.height) == height
    balance = height_client.query_bank_balance(address)
    for validator, (amount, reward) in height_client.get_amounts_rewards(str(address)).items():
        balance += amount
    if last_balance is not None and balance != last_balance:
        if reverse:
            new_block = last_block
            new_balance = last_balance
        else:
            new_block = block
            new_balance = balance
        time = new_block.time.timestamp()
        new_balance = Decimal(new_balance) / 1000000
        new_height = new_block.height
        new_time = time
        if len(data):
            assert new_balance != data[0][-1] and new_balance != data[-1][-1]
        data.append((new_height, new_time, new_balance))
        line = f'{new_height},{datetime.fromtimestamp(new_time).isoformat()},{new_balance}'
        csv.write(line + '\n')
        show(data[::-1] if reverse else data, line)
        if balance == 0 and reverse:
            data.sort()
            reverse = False
            height, _, _ = data[-1]
            last_balance = None
    last_balance = balance
    last_block = block

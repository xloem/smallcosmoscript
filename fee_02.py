import asyncio, base64, datetime, fractions, os, sys
import logging, warnings
logging.basicConfig(level=logging.INFO)

import numpy as np

from small.wallet import Wallet
from small.tx import Transaction, BroadcastException
from small.cosmoschains import *
import small.util as util
import small.osmosis as osmosis

banned_urls = []
async def make_client(wallet_or_chain, url=None):
    if type(wallet_or_chain) is Wallet:
        wallet = wallet_or_chain
        chain = wallet.chain
    else:
        chain = wallet_or_chain
        wallet = None
    client = await chain.aclient(protobuf=chain.baseline_protobuf(), url=url, banned_urls=banned_urls)
    client.retry_conn_failures = True
    if wallet is not None:
        wallet.client = client
    return client

async def fee(chain_id, retained_fraction, unstake=False, redelegate=True, broadcast_txs=[], url=None):
    #chain = MultiResource.all().chain(chain_id)
    chain = FileRegistry().chain(chain_id)
    client = await make_client(chain, url)
    wallet = Wallet(
        chain=chain,
        seed_phrase=os.environ.get('SEED_PHRASE','cosmos.wallet'),
        client=client,
        broadcast_tx_bytes=[
            base64.b64decode(broadcast_tx.encode())
            for broadcast_tx in broadcast_txs
        ]
    )
    await wallet.__ainit__()

    account = await client.cosmos_auth.aAccount(wallet.address)
    bond_denom = (await client.cosmos_staking.aParams()).bond_denom
    mint_params = await client.cosmos_mint.aParams()
    block_time = util.YEAR / mint_params.blocks_per_year
    mint_denom = mint_params.mint_denom
    staking_denoms = set([mint_denom, bond_denom])
    if 'staking' in chain.chainjson:
        assert staking_denoms == set([
            token['denom'] for token in chain.chainjson['staking']['staking_tokens']
        ])
    fee_denoms = [denom['denom'] for denom in chain.chainjson['fees']['fee_tokens']]
    assert len(fee_denoms) == 1

    #community_tax = fractions.Fraction((await client.cosmos_distribution.aParams()).community_tax)
    #annual_provisions = fractions.Fraction((await client.cosmos_mint.aAnnualProvisions()).decode())
    ## block provisions truncated according to https://docs.cosmos.network/v0.50/modules/mint#blockprovision
    #block_provisions = int(annual_provisions / mint_params.blocks_per_year)

    fee_block_confirm_hist = util.ConfirmationBlocksHist()

    data = None

    print(wallet.address)

    tip_block = await client.cosmos_base_tendermint.aGetLatestBlock()
    height = tip_block.block.header.height
    timestamp = datetime.datetime.fromtimestamp(tip_block.block.header.time.ToNanoseconds() / 1000000000)
    accumulated_rewards = 0
    accumulated_amounts = 0
    accumulated_blocks = 0
    total_txs = 0
    confirmation = None
    #last_confirmation = None

    while True:
     try:
        metadata = {client.X_COSMOS_BLOCK_HEIGHT:str(height)}
        metadata_1 = {client.X_COSMOS_BLOCK_HEIGHT:str(height-1)}
        bonded_validators = await util.aget_validators(client, metadata=metadata)
        all_validators = await util.aget_validators(client,None,metadata=metadata)#wallet.address, metadata=metadata)
        #all_validators.update(bonded_validators)
        min_commission, min_shareprice, min_validators = await util.aget_min_validators(client, bonded_validators)
        #print('min_validators:', min_validators)
        balance = {
            coin.denom: fractions.Fraction(coin.amount)
            async for coin in client.cosmos_bank.aAllBalances(wallet.address, metadata=metadata)
        }
        print({await osmosis.Osmosis.aparse_denom(client, denom): int(bal) for denom, bal in balance.items()})

        #bonded_tokens = fractions.Fraction((await client.cosmos_staking.aPool()).bonded_tokens)
        ## from https://docs.cosmos.network/v0.50/modules/mint#blockprovision:
        ## - funds are distributed to the "distribution" ModuleAccount
        ##   - fees from previous block
        ##   - inflationary rewards from previous block
        ## - community tax is charged
        ## - remainder is distributed to validators based on voting power
        ##   validator rewards are truncated to integers. remainder is sent to conmmunity tax pool.
        ##   int(fees_and_provisions * (1 - community_tax) * validator_power / total_bonded_validator_power)
        ##    validators have both commission and self-delegation. the self-delegation is treated like another
        ##    delegation. but the commission sends a fraction of rewards to ValidatorAccumulatedCommission
        ##    and retains delegator rewards in ValidatorCurrentRewards, which then are distributed with F1
        ## - however, according to
        ##   https://hub.cosmos.network/main/validators/validator-faq.html#how-are-block-rewards-distributed
        ##   fees and block rewards are treated differently, with inflation simpler.
        ##  
        ##  delegation rewards are taken from the moduleaccount when requested.

        if confirmation is not None and height - last_height > 1:
            positions_0 = positions # last_positions
            positions_h = await util.aget_amounts_rewards_unbondings(client, wallet.address, validators=all_validators, metadata=metadata_1)#denom=mint_denom
            for validator in positions_h:
                amount_h, rewards_h, unbonding_h = positions_h.get(validator, (0,{},0))
                amount_0, rewards_0, unbonding_0 = positions_0.get(validator, (0,{},0))
                reward_h = rewards_h.get(bond_denom,0)
                reward_0 = rewards_0.get(bond_denom,0)
                if reward_0 == 0 and reward_h != 0 and amount_0 == amount_h:
                    accumulated_amounts += amount_0 * (height - 1 - last_height)
                    accumulated_rewards += reward_h
                    #print(validator, float(reward_h / (height - 1 - last_height) / amount_0))

        #expected_reward_per_block = accumulated_reward / accumulated_blocks
        #expected_reward = accumulated_blocks / total_txs
        if accumulated_amounts and total_txs:
            #import pdb; pdb.set_trace()
            expected_reward_per_amount_per_block = accumulated_rewards / accumulated_amounts
            expected_reward_per_amount = expected_reward_per_amount_per_block * accumulated_blocks / total_txs
        else:
            expected_reward_per_amount_per_block = 0
            expected_reward_per_amount = 0
        print('expected_reward_per_amount =',float(expected_reward_per_amount))
        #if last_confirmation is not None:
        #    expected_reward = sum([
        #        int(tx.Coin(event.attributes[0].value.decode()).amount)
        #        for event in confirmation.tx_response.events
        #        if event.type == 'withdraw_rewards'
        #    ])
        #    expected_reward = balance.get(bond_denom, 0) - last_balance.get(bond_denom, 0)
        #    expected_reward_per_block = expected_reward / (height - last_height)
        #expected_reward_per_validator = expected_reward / len(min_validators)

        positions = await util.aget_amounts_rewards_unbondings(client, wallet.address, validators=all_validators, metadata=metadata)#denom=mint_denom
                
            

        #async with wallet.atx() as tx:
        tx = Transaction(wallet)

        redelegation_srcs = []
        total_balance = dict(balance)
        locked_balance = {bond_denom:0}
        #import pdb; pdb.set_trace()
        for validator, (amount, rewards, unbonding) in positions.items():
            #tokenshares = validators[validator][1]/validators[validator][2]
            ##validator_expected_reward_per_block = block_shares / tokenshares * amount / bonded_tokens
            #validator_expected_reward_per_block = block_provisions * amount / bonded_tokens
            ##if validator in validators and expected_reward_per_validator > rewards.setdefault(bond_denom,0):
            ##    rewards[bond_denom] = expected_reward_per_validator #rewards.get(bond_denom,0) + expected_reward_per_validator
            total_balance[bond_denom] += amount
            locked_balance[bond_denom] += amount
            if hasattr(tx, 'cancelunbondingdelegation'):
                total_balance[bond_denom] += unbonding
            for denom, reward in rewards.items():
                if denom == bond_denom and validator in min_validators:
                    reward = max(reward, expected_reward_per_amount * amount)
                total_balance[denom] = total_balance.get(denom,0) + int(reward)
            if validator not in min_validators:
                trunc_reward = int(rewards.get(bond_denom, 0))
                if redelegate:
                    if trunc_reward > 0:
                        print(f'withdraw {trunc_reward} from {validator} preceding redelegation')
                        tx.withdrawdelegatorreward(wallet.address, validator)
                        rewards = {}
                        #print(f'delegate {trunc_reward} to {validator} for later redelegation')
                        #tx.delegate(wallet.address, validator, tx.Coin(trunc_reward, bond_denom))
                        #amount += trunc_reward
                        positions[validator] = (amount, rewards, unbonding)
                    trunc_amount = int(amount)
                    if trunc_amount > 0:
                        redelegation_srcs.append((validator, trunc_amount))

        retained_fraction = fractions.Fraction(retained_fraction)
        retained_balance = retained_fraction * total_balance[bond_denom]
        if not unstake or not hasattr(tx, 'cancelunbondingdelegation'):
            retainable_balance = total_balance[bond_denom] - locked_balance[bond_denom]
            if retainable_balance < retained_balance:
                retained_balance = retainable_balance
                retained_fraction = retained_balance / total_balance[bond_denom]
                warnings.warn(f'dropped retained_fraction to {float(retained_fraction)}% as {int(retained_balance)} is locked and unstaking is limited')

        per_validator_balance = fractions.Fraction(total_balance[bond_denom] - retained_balance) / len(min_validators)
        all_validator_balance = 0
        for validator in set(min_validators) | set(positions):
            amount, rewards, unbonding = positions.get(validator, (0, {}, 0))
            all_validator_balance += amount
            dest_balance = per_validator_balance if validator in min_validators else 0
            trunc_remaining = int(dest_balance - amount)
            bond_reward = rewards.get(bond_denom,0)
            bond_balance = balance.get(bond_denom,0)
            if validator in min_validators:
                expected_bond_reward = expected_reward_per_amount * amount
                print(float(bond_reward), float(expected_bond_reward))
                if bond_reward < expected_bond_reward:
                    bond_reward = expected_bond_reward
            while trunc_remaining > 0 and len(redelegation_srcs):
                validator_src, avail = redelegation_srcs.pop()
                if avail > trunc_remaining:
                    redelegation_srcs.append((validator_src, avail - trunc_remaining))
                    avail = trunc_remaining
                print(f'{validator_src[-3:]}>{avail}>{validator[-3:]}', end=';')
                tx.beginredelegate(wallet.address, validator_src, validator, tx.Coin(avail, bond_denom))
                src_amount, src_reward, src_unstake = positions[validator_src]
                src_amount -= avail
                positions[validator_src] = src_amount, src_reward, src_unstake
                trunc_remaining -= avail
            if trunc_remaining > 0 and unbonding > 0 and hasattr(tx, 'cancelunbondingdelegation'): # cosmos-sdk 0.46
                to_add = min(trunc_remaining, int(unbonding))
                print(f'{to_add}X>{validator[-3:]}', end=';')
                tx.cancelunbondingdelegation(wallet.address, validator, tx.Coin(to_add, bond_denom))
                #all_validator_balance += to_add
                amount += to_add
                trunc_remaining -= to_add
                unbonding -= to_add
            if trunc_remaining > 0 and (bond_balance > 0 or bond_reward > 0):
                to_add = min(int(bond_balance + bond_reward), trunc_remaining)
                print(f'{to_add}>{validator[-3:]}',end=';')
                tx.delegate(wallet.address, validator, tx.Coin(to_add, bond_denom))
                amount += to_add
                #all_validator_balance += to_add
                trunc_remaining -= to_add
                bond_balance += bond_reward - to_add
                bond_reward = 0
            elif unstake and trunc_remaining < 0:
                print(f'{-trunc_remaining}<,<{validator}',end=';')
                tx.undelegate(wallet.address, validator, tx.Coin(-trunc_remaining, bond_denom))
                #all_validator_balance += trunc_remaining
                trunc_remaining = 0
            elif int(bond_reward) > 0 and bond_reward - int(bond_reward) < int(bond_reward) / (100/0.001):
                warnings.warn('withdrawing because bond roundoff loss is < 0.001% but this should be calculated instead')
                trunc_reward = int(bond_reward)
                print(f'{trunc_reward}<{validator[-5:]}',end=';')
                tx.withdrawdelegatorreward(wallet.address, validator)

        if len(tx) == 0:
            tip_block = await client.cosmos_base_tendermint.aGetLatestBlock()
            timestamp = datetime.datetime.fromtimestamp(tip_block.block.header.time.ToNanoseconds() / 1000000000)
            height = tip_block.block.header.height
            continue
        print()
        bonded_amounts = [ amount for validator, (amount, _, _) in positions.items() if validator in bonded_validators ]
        prev_timestamp = datetime.datetime.now()
        cur_height = (await client.cosmos_base_tendermint.aGetLatestBlock()).block.header.height
        broadcast_height = cur_height
        if confirmation is not None:
            if confirmation.tx_response.height > broadcast_height:
                print('chain forked/rewound and our tx is missing?')
                #confirmation = None
                await wallet.update()
                _tx, _tx_response = await client.cosmos_tx.aGetTxsEvent([f"tx.fee_payer='{wallet.address}'"], order_by='ORDER_BY_DESC').__anext__()
                confirmation = type(confirmation)(tx=_tx,tx_response=_tx_response)
                #confirmation = await tx.asyncwait(broadcast_bytes=confirmation.tx.SerializeToString(), txhash=confirmation.tx_response.txhash)
            fee, delay = fee_block_confirm_hist.select_fee_delay_for_reward(expected_reward_per_amount_per_block, bonded_amounts, retained_fraction, broadcast_height - confirmation.tx_response.height)
        else:
            fee, delay = 0, 0
        print(f'fee={fee}, delay={delay}')
        if delay > 0:
            delay_height = broadcast_height + delay
            async with util.await_poll(client, desc='Timing broadcast', unit='blk', dividor=6, total=delay) as aupdate:
                while True:
                    prev_height = cur_height
                    cur_height = (await client.cosmos_base_tendermint.aGetLatestBlock()).block.header.height
                    if cur_height >= delay_height:
                        if cur_height != delay_height:
                            print('lateness =', cur_height - delay_height)
                        #broadcast_height = cur_height
                        broadcast_height = delay_height # so the distribution used is the one planned around, include the delay of this code itself (when ratelimited or slow)
                        break
                    else:
                        cur_timestamp = datetime.datetime.now()
                        if cur_height == prev_height:
                            if (cur_timestamp - prev_timestamp).seconds > block_time * 10:
                                raise Exception('node stalled')
                        else:
                            prev_timestamp = cur_timestamp
                    await aupdate(n_abs = cur_height - broadcast_height)
        try:
            #last_confirmation = confirmation
            confirmation = await tx.asignbroadcast(gas_price=fee,flat=True,fee_denom=fee_denoms[0])
        except client.Errors as error:
            status, message = client.error_status_message(error)
            if status == client.Statuses.UNKNOWN and 'account sequence mismatch' in message:
                print('bumped into a concurrent tx! waiting for it to settle...')
                await wallet.update()
                tip_block = await client.cosmos_base_tendermint.aGetLatestBlock()
                timestamp = datetime.datetime.fromtimestamp(tip_block.block.header.time.ToNanoseconds() / 1000000000)
                height = tip_block.block.header.height
                confirmation = None
                continue
            if status == client.Statuses.UNKNOWN and 'insufficient fee' in message and url is None:
                banned_urls.append(client._api_url)
                client = await make_client(wallet)
                continue
            raise
        except BroadcastException as exception:
            print(exception)
            if confirmation:
                import pdb; pdb.set_trace()
            if 'insufficient fee' in exception.args[0].raw_log and url is None:
                banned_urls.append(client._api_url)
                client = await make_client(wallet)
                continue
            raise

        fee_block_confirm_hist.add(client, bond_denom, broadcast_height, confirmation)

        x1 = timestamp.timestamp()
        y1_exp = all_validator_balance + balance.get(bond_denom,0)
        y1 = np.log(float(y1_exp))
        if data is None:
            # discard first balance as parameters could differ
            data = np.empty([0,2])
        elif len(data) < 1024:
            datapoint = np.array([[x1, y1]])
            data = np.concatenate([data, datapoint])
        else:
            data[1:] = data[:-1]
            data[-1] = x1, y1
        if len(data) >= 3:
            polyfit, (resid, rank, sv, rcond) = util.regression(data[:,0], data[:,1])
            # solve for ttd
            y2 = np.log(float(y1_exp*2))
            poly2 = (polyfit - y2)
            x2s = [x for x in poly2.roots() if x > x1]
            if len(x2s) ==0 or x2s[0].imag or polyfit.coef[-1] > 0:
                # if x2.imag is nonzero then degree=2 predicted downturn (never reaches y2),
                # but ignoring this because the process of interest only rises
                # if polyfit[-1] > 0 then degree=2 predicted acceleration, which is unnecessarily optimistic for now
                polyfit, (resid, rank, sv, rcond) = util.regression(data[:,0], data[:,1], degree=1)
                poly2 = (polyfit - y2)
                x2s = [x for x in poly2.roots() if x > timestamp.timestamp()]
            print('polyfit, y = exp(', polyfit, ')')
            if len(x2s):
                x2 = x2s[0]
                #if len(data) == 6:
                #    import pdb; pdb.set_trace()
                #print(data)
                print('TTD:', str(datetime.timedelta(seconds=x2 - x1)))
                # calc wpr
                if x2 - x1 > util.MONTH:
                    x1, y1 = data[-1]
                    wpr = np.exp((y2 - y1) * 60*60*24*7 / (x2 - x1))
                    print('WPR: ', float(wpr * 100 - 100), '%')
        
        last_balance = balance
        last_height = height
        height = confirmation.tx_response.height

        #last_amount_rewards = {
        #    attrs['validator']: (
        #        positions[attrs['validator']][0],
        #        {
        #            coin.denom: int(coin.amount)
        #            for coin in [tx.Coin(attrs['amount'])]
        #        }.get(mint_denom, 0),
        #    )
        #    for event in confirmation.tx_response.events
        #    if event.type == 'withdraw_rewards'
        #    for attrs in [{
        #        attr.key.decode(): attr.value.decode()
        #        for attr in event.attributes
        #    }]
        #    if positions[attrs['validator']][1].get(mint_denom,0) == 0
        #}
        #if len(last_amount_rewards):
        #    accumulated_amounts += sum([amount for amount, reward in last_amount_rewards.values()]) * (height - last_height)
        #    accumulated_rewards += sum([reward for amount, reward in last_amount_rewards.values()])
        if True:
            accumulated_blocks += (height - last_height)
            total_txs += 1

        

        tip_block = await client.cosmos_base_tendermint.aGetBlockByHeight(height)
        timestamp = datetime.datetime.fromtimestamp(tip_block.block.header.time.ToNanoseconds() / 1000000000)
        #timestamp = datetime.datetime.strptime(confirmation.tx_response.timestamp, '%Y-%m-%dT%H:%M:%SZ')
     except client.Errors as error:
        status, message = client.error_status_message(error)
        print(status, message)
        if status == client.Statuses.UNKNOWN and (
                'invalid height' in message or
                'version does not exist' in message
        ):
            tip_block = await client.cosmos_base_tendermint.aGetLatestBlock()
            if 'invalid height' in message and tip_block.block.header.height >= height:
                tip_block = await client.cosmos_base_tendermint.aGetBlockByHeight(height - 1)
            height = tip_block.block.header.height
            timestamp = datetime.datetime.fromtimestamp(tip_block.block.header.time.ToNanoseconds() / 1000000000)
            confirmation = None
            if data is not None and len(data):
                data = data[:-1]
            continue
        if status in (client.Statuses.UNKNOWN, client.Statuses.UNAVAILABLE) and '502' in message: # cloudflare says no response from server
            banned_urls.append(client._api_url)
            client = await make_client(wallet)
            continue
        raise
     except Exception as exc:
        if exc.args == ('node stalled',):
            banned_urls.append(client._api_url)
            client = await make_client(wallet)
            continue
        raise
            

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('chain_id', type=str)
    parser.add_argument('--url', '-u', type=str, default=None)
    parser.add_argument('--broadcast', '-b', nargs='*', type=str, default=[])
    parser.add_argument('--retained-fraction', '-r', type=float, default=0.125)
    parser.add_argument('--unstake', action='store_true')
    parser.add_argument('--no-redelegate', action='store_true')
    args = parser.parse_args()
    asyncio.run(fee(args.chain_id, retained_fraction=args.retained_fraction, url=args.url, unstake=args.unstake, redelegate=not args.no_redelegate, broadcast_txs=args.broadcast))

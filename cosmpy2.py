from contextlib import contextmanager
from datetime import datetime, timedelta, timezone
from typing import Any, Dict, List, Optional, Tuple
from decimal import Decimal
from fractions import Fraction
#from fractions import Fraction, Fraction as Decimal
from dataclasses import dataclass
import time

import google.protobuf.json_format
_Parse = google.protobuf.json_format.Parse
def ParseLeniently(*params, ignore_unknown_fields=True, **kwparams):
    return _Parse(*params, ignore_unknown_fields=ignore_unknown_fields, **kwparams)
google.protobuf.json_format.Parse = ParseLeniently

from google.protobuf.timestamp_pb2 import Timestamp

from cosmpy.aerial.client import LedgerClient, NetworkConfig, DEFAULT_QUERY_TIMEOUT_SECS, DEFAULT_QUERY_INTERVAL_SECS, COSMOS_SDK_DEC_COIN_PRECISION
from cosmpy.aerial.client import get_paginated, ValidatorStatus
from cosmpy.aerial.client import Protocol, parse_url
from cosmpy.common.rest_client import RestClient
from cosmpy.crypto.hashfuncs import sha256
from cosmpy.protos.cosmos.base.tendermint.v1beta1.query_pb2 import (
    GetBlockByHeightRequest,
    GetLatestBlockRequest,
)
from cosmpy.protos.cosmos.base.tendermint.v1beta1.query_pb2_grpc import (
    ServiceStub as TendermintQueryGrpcClient,
)
from cosmpy.protos.tendermint.types.block_pb2 import Block as PbBlock
from cosmpy.protos.cosmos.mint.v1beta1.query_pb2 import (
    QueryParamsRequest as QueryMintParamsRequest,
    QueryAnnualProvisionsRequest
)
from cosmpy.protos.cosmos.mint.v1beta1.query_pb2_grpc import (
    QueryStub as MintQueryGrpcClient,
)
from cosmpy.bank.interface import QueryTotalSupplyRequest
from cosmpy.tendermint.rest_client import CosmosBaseTendermintRestClient, GetBlockByHeightRequest
from cosmpy.staking.interface import (
    QueryDelegatorDelegationsRequest,
    QueryValidatorsRequest,
    QueryValidatorDelegationsRequest,
    QueryPoolRequest,
)
from cosmpy.tendermint.rest_client import (
    CosmosBaseTendermintRestClient as TendermintRestClient,
)
from cosmpy.mint.rest_client import MintRestClient
from cosmpy.distribution.interface import QueryDelegationRewardsRequest

import certifi
import grpc

#@dataclass
#class Block:
#    """Block."""
#
#    height: int
#    time: datetime
#    chain_id: str
#    tx_hashes: List[str]
#
#    @staticmethod
#    def from_proto(block: PbBlock) -> "Block":
#        """Parse the block.
#
#        :param block: block as PbBlock
#        :return: parsed block as Block
#        """
#        return Block(
#            height=int(block.header.height),
#            time=Block._parse_timestamp(block.header.time),
#            tx_hashes=[sha256(tx).hex().upper() for tx in block.data.txs],
#            chain_id=block.header.chain_id,
#        )
#
#    @staticmethod
#    def _parse_timestamp(timestamp: Timestamp):
#        """Parse the timestamp.
#
#        :param timestamp: timestamp
#        :return: parsed timestamp
#        """
#        return datetime.fromtimestamp(timestamp.seconds, tz=timezone.utc) + timedelta(
#            microseconds=timestamp.nanos // 1000
#        )

class Client(LedgerClient):
    def __init__(self,
        cfg: NetworkConfig,
        query_interval_secs: int = DEFAULT_QUERY_INTERVAL_SECS, # 2
        query_timeout_secs: int = DEFAULT_QUERY_TIMEOUT_SECS, # 15
        api_timeout_secs = 2,
    ):
        super().__init__(cfg, query_interval_secs, query_timeout_secs)
        self.api_timeout_secs = api_timeout_secs

        parsed_url = parse_url(cfg.url)
        #assert parsed_url.protocol != Protocol.GRPC
        if parsed_url.protocol == Protocol.GRPC:
            self.dec_scale = COSMOS_SDK_DEC_COIN_PRECISION
            if parsed_url.secure:
                with open(certifi.where(), "rb") as f:
                    trusted_certs = f.read()
                credentials = grpc.ssl_channel_credentials(
                    root_certificates=trusted_certs
                )
                self._grpc_client = grpc.secure_channel(parsed_url.host_and_port, credentials)
            else:
                self._grpc_client = grpc.insecure_channel(parsed_url.host_and_port)
            #self.tendermint = TendermintQueryGrpcClient(self._grpc_client)
            self.mint = MintQueryGrpcClient(self._grpc_client)
        else:
            self.dec_scale = 1
            self._rest_api = self.bank._rest_api
            #self.tendermint = CosmosBaseTendermintRestClient(self._rest_api)
            self.mint = MintRestClient(self._rest_api)
            def timeoutget(get):
                def timeoutget(*params, **kwparams):
                    return get(*params, timeout=self.api_timeout_secs, **kwparams)
                return timeoutget
            for val in self.__dict__.values():
                if isinstance(val, RestClient):
                    val._session.get = timeoutget(val._session.get)

    #def query_latest_block(self) -> Block:
    #    """Query the latest block.

    #    :return: latest block
    #    """
    #    req = GetLatestBlockRequest()
    #    resp = self.tendermint.GetLatestBlock(req)
    #    return Block.from_proto(resp.block)

    #def query_block(self, height: int) -> Block:
    #    """Query the block.

    #    :param height: block height
    #    :return: block
    #    """
    #    req = GetBlockByHeightRequest(height=height)
    #    resp = self.tendermint.GetBlockByHeight(req)
    #    return Block.from_proto(resp.block)

    #def query_height(self) -> int:
    #    """Query the latest block height.

    #    :return: latest block height
    #    """
    #    return self.query_latest_block().height

    #def query_chain_id(self) -> str:
    #    """Query the chain id.

    #    :return: chain id
    #    """
    #    return self.query_latest_block().chain_id

GRPCBlockHeightHeader = "x-cosmos-block-height"
    
class Client2(Client):

    def for_height(client, height):
        new_client = LedgerClient(client.network_config)
        new_client.height = lambda: height
        if hasattr(new_client.bank, '_rest_api'):
            rest_api = new_client.bank._rest_api
            rest_api._session.headers[GRPCBlockHeightHeader] = str(height)
        else:
            import grpc
            def wrap(method):
                def wrapped(request):
                    try:
                        return method(request, metadata = ((GRPCBlockHeightHeader, str(height)),))
                    except grpc._channel._InactiveRpcError:
                        time.sleep(1)
                        return method(request, metadata = ((GRPCBlockHeightHeader, str(height)),))
                return wrapped
            for member in new_client.__dict__.values():
                try:
                    for name, method in member.__dict__.items():
                        if type(method) is grpc._channel._UnaryUnaryMultiCallable:
                            setattr(member, name, wrap(method))
                except AttributeError:
                    continue
            assert type(new_client.bank.Params) is not grpc._channel._UnaryUnaryMultiCallable
        return new_client

    def query_mint_params(client):
        try:
            return client.mint.Params(QueryMintParamsRequest()).params
        except TypeError:
            return client.mint.Params().params
    #def get_real_bpy(client):
        # information on the rewards
        # so, first get the delegators of the validators
        # then see how much reward accumulates
    def get_blocks_per_year(client):
        #return Fraction(client.query_params('mint', 'BlocksPerYear'))
        return client.query_mint_params().blocks_per_year
    def get_bpr(client, blocks_per_year = None):
        supply = client.bank.TotalSupply(QueryTotalSupplyRequest()) # has 'stake' supply
        try:
            try:
               annual_provisions = Fraction(client.mint.AnnualProvisions(QueryAnnualProvisionsRequest()).annual_provisions.decode()) / COSMOS_SDK_DEC_COIN_PRECISION
            except TypeError:
               annual_provisions = Fraction(client.mint.AnnualProvisions().annual_provisions.decode())
        except RuntimeError:
            import warnings
            warnings.warn(f'{client.network_config.url} failed to get annual_provisions', stacklevel=2)
            return 0
        if blocks_per_year is None:
            blocks_per_year = client.get_blocks_per_year()
        staking_pool = client.staking.Pool(QueryPoolRequest())
        bonded_tokens = Fraction(staking_pool.pool.bonded_tokens)
        block_provision = annual_provisions / blocks_per_year
        if block_provision > bonded_tokens:
            # scale bonded tokens up by the size of the display denom
            # could maybe also infer from inflation rate and supply
            # there's also a way to get the denoms from the api
            denoms = client.get_denoms()
            assert False#bpr < 1000
            bonded_tokens *= 1000000 # most things seem to be micros?
        bpr = block_provision / bonded_tokens
        return bpr

    def iterate_txs(client, from_height=None, to_height=None, reverse=True):
        if from_height is None:
            if reverse:
                from_height = 0
            else:
                from_height = 1
        if from_height <= 0:
            height = client.query_height() + from_height
            if to_height <= 0:
                to_height = height - from_height + to_height
        else:
            height = from_height
            if to_height <= 0:
                to_height = client.query_height() + to_height
        while height > 1 and height != to_height:
            try:
                block = client.query_block(height)
            except grpc._channel._InactiveRpcError:
                continue # retry
            except RuntimeError:
                if not reverse:
                    block = client.query_latest_block()
                    if block.height > height:
                        raise
                    elif block.height < height:
                        break
                else:
                    raise
            for tx_hash in block.tx_hashes:
                time.sleep(0.05)
                #if hasattr(client.txs, 'rest_client'):
                #    txjson = client.txs.rest_client.get(f"{client.txs.API_URL}/txs/{tx_hash}").json()
                #    tx = lambda: None
                #    for key, val in tx['tx_response'].items():
                #        setattr(tx, key, val)
                #else:
                if True:
                    try:
                        tx = client.query_tx(tx_hash)
                    except:
                        continue
                yield tx
            if reverse:
                height = block.height - 1
            else:
                height = block.height + 1

    # code for listing validators after amoubts rewards func

    def get_all_rewards(client, delegator, validator):
        rewards_resp = client.distribution.DelegationRewards(
            QueryDelegationRewardsRequest(
                delegator_address=delegator,
                validator_address=validator,
            )
        )
    
        return {
            reward.denom: Decimal(reward.amount)
            for reward in rewards_resp.rewards
        }
    def get_reward(client, delegator, validator):
        for denom, amount in client.get_all_rewards(delegator, validator).items():
            if denom == client.network_config.staking_denomination:
                return amount
        else:
            return 0

    def get_amounts_rewards(client, delegator):
        positions = {}
    
        for resp in get_paginated(
            QueryDelegatorDelegationsRequest(delegator_addr=delegator),
            client.staking.DelegatorDelegations#, per_page_limit=1
        ):
            for item in resp.delegation_responses:
                validator = str(item.delegation.validator_address)
                #amount = Decimal(item.balance.amount)#graviton denomiates the balance wrongly
                amount = Decimal(item.delegation.shares)
                positions[validator] = amount, client.get_reward(delegator, validator)
        return positions

    def get_validators(client):
        return (
            validator
            for resp in get_paginated(
                QueryValidatorsRequest(status=ValidatorStatus.BONDED.value),
                client.staking.Validators
            )
            for validator in resp.validators
        )
    
    def get_validator_commissions(client):
        return {
            validator.operator_address: Decimal(validator.commission.commission_rates.rate)
            for validator in client.get_validators()
        }
    
    def get_min_commission_validators(client):
        validator_commissions = client.get_validator_commissions()
        min_commission = min(validator_commissions.values())
        return min_commission, [validator for validator, commissions in validator_commissions.items() if commissions == min_commission]

    def get_validator_delegations(client, validator_addr):
        return (
            delegation
            for resp in get_paginated(
                QueryValidatorDelegationsRequest(validator_addr = validator_addr),
                client.staking.ValidatorDelegations
            )
            for delegation in resp.delegation_responses
        )

    def get_assets_json(client):
        from cosmpy.protos.cosmos.bank.v1beta1.query_pb2 import QueryDenomsMetadataRequest
        from google.protobuf.json_format import MessageToJson
        import json
        assets_message = client.bank.DenomsMetadata(QueryDenomsMetadataRequest())
        assets_json = json.loads(MessageToJson(assets_message)).get('metadatas', ())
        return assets_json
    def get_denoms(client):
        assets_json = client.get_assets_json()
        denoms = client.parse_denoms(assets_json, client.network_config.chain_id)
        return denoms
    @staticmethod
    def parse_denoms(assets_json, prefix):#=''):
        denoms = {}
        for asset in assets_json:
            denom_digits = {}
            for unit in asset.get('denomUnits', asset.get('denom_units')):
                for denom in [unit['denom']] + unit.get('aliases',[]):
                    digits = unit.get('exponent',0)
                    if denom in denom_digits:
                        import warnings
                        warnings.warn(f'{prefix}: multiple units for {denom}, assuming the largest is {denom[1:]}', stacklevel=3)
                        denom_digits[denom] = min(denom_digits[denom], digits)
                        digits = max(denom_digits[denom], digits)
                        denom = denom[1:]
                    denom_digits[denom] = digits
            for display_denom in (
                asset['display'],
                asset.get('symbol','').lower(),
                prefix.split('-',1)[0].split('net',1)[0]+asset['display'],
                prefix.split('-',1)[0].split('net',1)[0]+asset['symbol'].lower(),
            ):
                if display_denom in denom_digits:
                    display_digits = denom_digits[display_denom]
                    display = asset.get('symbol',display_denom.upper())
                    break
            for name, digits in denom_digits.items():
                denoms[name] = (display_digits - digits, display)
        return denoms

LedgerClient = Client2

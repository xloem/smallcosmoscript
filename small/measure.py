from . import cosmoschains as chains
from . import osmosis
from . import util
from .client import Client
from .rpc_client import RPCClient
import asyncio, base64
from fractions import Fraction
import time

import grpc._channel
import google.protobuf.json_format

#import logging; logging.basicConfig(level=logging.DEBUG)

import tqdm

class Async:
    @classmethod
    async def acreate(cls, *params, **kwparams):
        self = cls(*params, **kwparams)
        await self.__ainit__()
        return self

def parse_txjson_events(txjson):
    for event in txjson['tx_result']['events']:
        yield event['type'], {
            base64.b64decode(attr['key']).decode(): base64.b64decode(attr['value']).decode()
            for attr in event['attributes']
        }
def parse_tx_response_events(tx_response):
    for event in tx_response.events: 
        yield event.type, {
            attr.key.decode(): attr.value.decode()
            for attr in event.attributes
        }

class MeasuredChain:
    # put anything here
    def __init__(self, chainobj):
        self.chainobj = chainobj
    async def __ainit__(self):
        #self.chain = await self.chainobj.aclient()
        apislist = self.chainobj.urls('grpc')
        apierrs={}
        for api in tqdm.tqdm(apislist,desc=f'trying grpc apis for {self.chainobj.chain_id}',leave=False):
            try:
                client = await self.chainobj.aclient(url=api)
                cached_delegations = {}
                tip_block = await client.cosmos_base_tendermint.aGetLatestBlock()
                commission, tokenshares, validators = await util.aget_min_validators(client)
                assert len(validators)
                #else:
                #    assert False
                for validator in validators[:1]:
                    delegations = client.cosmos_staking.aValidatorDelegations(validator_addr = validator)
                    async for delegation in delegations:
                        reward = await client.cosmos_distribution.aDelegationRewards(
                            delegator_address = delegation.delegation.delegator_address,
                            validator_address = delegation.delegation.validator_address,
                        )
                        break
                break
            except AssertionError as err:
                apierrs[api] = err
            except Client.Errors as err:
                apierrs[api] = err
            except google.protobuf.json_format.ParseError as err:
                apierrs[api] = err
            except util.requests_exceptions.ConnectionError as err:
                apierrs[api] = err
            except util.requests_exceptions.ReadTimeout as err:
                apierrs[api] = err
            except RuntimeError as err:
                apierrs[api] = err
        else:
            import socket
            for type, id, addr in chain.peers.values():
                try:
                    socket.create_connection(addr.rsplit(':',1), timeout=1).close()
                    print(f'{chain.name} {chain.chain_id} peer responds but no listed api')
                    print(apierrs)
                    break
                except:
                    pass
            return None
        self.grpcclient = client
        self.staking_params = await self.grpcclient.cosmos_staking.aParams()
        self.bond_denom = self.staking_params.bond_denom
        self.mint_params = await self.grpcclient.cosmos_mint.aParams()
        self.block_time_nominal = Fraction(util.YEAR) / self.mint_params.blocks_per_year
        self.mint_denom = self.mint_params.mint_denom
        # unsure regarding indexing clients by height ... does seem helpful

        rpcslist = self.chainobj.urls('rpc')
        for api in tqdm.tqdm(rpcslist,desc=f'trying rpc apis for {self.chainobj.chain_id}',leave=False):
            rpcclient = RPCClient(api)
            try:
                status = rpcclient.status()
                break
            except Exception as e:
                exc = e
        else:
            raise exc
        self.rpcclient = rpcclient
            
    class Spot:
        def __init__(self, measuredchain, api, height = None):
            self.measuredchain = measuredhcain
            self.api = api
            self.height = height
        async def __ainit__(self):
            if self.height is None:
                self.client = await self.measuredchain.chainobj.aclient(url=self.api)
                self.block = await self.client.cosmos_base_tendermint.aGetLatestBlock()
                self.height = int(self.block.header.height)
                self.client = await self.measuredchain.chainobj.aclient(url=self.api, height=self.height)
            elif type(self.height) is int:
                self.client = await self.measuredchain.chainobj.aclient(url=self.api, height=self.height)
                self.block = await self.client.cosmos_base_tendermint.GetBlockByHeight(height = self.height)
            else:
                self.block = self.height
                self.height = int(self.block.header.height)
                self.client = await self.measuredchain.chainobj.aclient(url=self.api, height=self.height)
            self.commission, self.validators = await util.aget_min_commission_validators(self.client)
            return self

    async def ameasure_address(self, address):
        #for tx in rpcclient.tx_search(query="message.action='/cosmos.distribution.v1beta1.MsgWithdrawDelegatorReward'"):#, match_events=True):

        # might make a speedier response to go through blocks in chunks like 1000 blocks at once or such
        # tx.minheight, tx.maxheight are fake query keys that can do this
        # limit= is an undocumented parameter to limit output count
        #all_validators = await util.aget_validators(self.grpcclient)#wallet.address, metadata=metadata)
        #import pdb; pdb.set_trace()
        assert self.mint_denom == self.bond_denom
        #height_rewards_amounts = []
        height = None
        positions = None
        rewards = None
        block = None
        import pdb; pdb.set_trace()
        #async for tx in self.rpcclient.atx_search(query=f"transfer.recipient='{address}' AND message.module='distribution'"):#, order_by='desc'):
        #async for tx in self.rpcclient.atx_search(query=f"coin_received.receiver='{address}'", order_by='desc'):
        async for tx, tx_response in self.grpcclient.cosmos_tx.aGetTxsEvent([f"coin_received.receiver='{address}'"], order_by='ORDER_BY_DESC'):
        #for tx in rpcclient.tx_search(query=f"message.module='distribution'",order_by='desc'): # raises server error
            #events = list(parse_txjson_events(tx))
            events = list(parse_tx_response_events(tx_response))
            last_rewards = rewards
            rewards = {
                attrs['validator']:
                {
                    coin.denom: int(coin.amount)
                    for coin in
                    [self.grpcclient.Coin(amt) for amt in attrs['amount'].split(',')]
                }
                for type, attrs in events
                if type == 'withdraw_rewards'
            }
            if not len(rewards):
                continue
            
            last_height = height
            height = tx_response.height #tx['height']
            metadata = {self.grpcclient.X_COSMOS_BLOCK_HEIGHT:str(height)}
            try:
                last_positions = positions
                last_block = block
                positions = await util.acache(util.aget_amounts_rewards_unbondings)(self.grpcclient, address, metadata=metadata)
                block = await util.acache(self.grpcclient.cosmos_base_tendermint.aGetBlockByHeight)(int(height))
            except self.grpcclient.Errors as exc:
                status, message = self.grpcclient.error_status_message(exc)
                if status == self.grpcclient.Statuses.UNKNOWN and 'either been pruned, or is for a future block' in message:
                    last_block = None
                    last_positions = None
                    last_height = None
                    last_rewards = None
                    continue
                raise
            # get staked amounts for height, maybe useful to use normal client
            #print(tx['height'], tx['hash'], *parse_txjson_events(tx))
            withdrawn_validators = list(rewards.keys())
            mint_rewards = {validator: amounts.get(self.mint_denom, 0) for validator, amounts in rewards.items()}
            validator_amounts = {validator: amount for validator, (amount, rewards, unbondings) in positions.items()}
            minted_from_amounts = {validator: validator_amounts[validator] - reward for validator, reward in mint_rewards.items()}
            minted_total = sum(mint_rewards.values())
            minted_from_total = sum(minted_from_amounts.values())
            #amount_total = sum([amount + rewards.get(self.mint_denom, 0) for validator, (amount, rewards, unbondings) in positions.items()]) # includes mint_rewards
            amount_total = sum([amount for validator, (amount, rewards, unbondings) in positions.items()])
            print(height, float(amount_total))
            if last_height is not None:
                last_minted_total = sum([amounts.get(self.mint_denom, 0) for amounts in last_rewards.values()])
                last_amount_total = sum([amount for validator, (amount, rewards, unbondings) in last_positions.items()])
                if last_height < height:
                    #earlier_height, later_height = last_height, height
                    earlier_amount, later_amount = last_amount_total, amount_total
                    earlier_minted, later_minted = last_minted_total, minted_total
                    earlier_timestamp, later_timestamp = last_block.block.header.time, block.block.header.time
                else:
                    #earlier_height, later_height = height, last_height
                    earlier_amount, later_amount = amount_total, last_amount_total
                    earlier_minted, later_minted = minted_total, last_minted_total
                    earlier_timestamp, later_timestamp = block.block.header.time, last_block.block.header.time
                net_withdrawn = later_minted - (later_amount - earlier_amount )
                ratio = Fraction(later_amount) / earlier_amount
                duration = Fraction(later_timestamp.ToNanoseconds() - earlier_timestamp.ToNanoseconds()) / 1000000000
                wpr = ratio ** ( util.WEEK / duration ) - 1
                print(earlier_timestamp.ToJsonString(), wpr*100)
            #reward_total = {}
            #for validator, reward_amounts in rewards.items():
            #    for denom, amount in reward_amounts.items():
            #        reward_total[denom] = reward_total.get(denom, 0) + amount
            #print(height, amount_total, reward_total.get(self.mint_denom,0))
            #height_rewards_amounts.append([
            #    height, reward_total.get(self.mint_denom,0), {validator:amount for (validator, (amount, reward, unbonding)) in positions.items()}
            #])
        import pdb; pdb.set_trace()
        

#from requests.adapters import TimeoutSauce
#TimeoutSauce.DEFAULT_TIMEOUT = 1
#import urllib3.util.timeout
#urllib3.util.timeout._Default = 1

chains = chains.FileRegistry()#MultiResource.all()#PingPub()

async def amain():
    from .wallet import Wallet
    chain = chains.chain('cosmoshub')
    wallet = Wallet(chain, 'cosmos.wallet')
    await wallet.__ainit__()
    address = wallet.address
    measured_chain = MeasuredChain(chain)
    await measured_chain.__ainit__()
    await measured_chain.ameasure_address(address)
    
    
    for chain_id in ('gitopia', *chains.chain_ids()):
        chain = chains.chain(chain_id)
        if chain.status == 'killed' and not len(chain.apis) and not len(chain.peers):
            continue
        variants = list(chain.variants())
        assert len(variants)
        #apislist = list(chain.apis.values())
        #apislist.sort(key=lambda api: 0 if api.startswith('grpc') else 1)
        #apislist = chain.urls('grpc')
        #apislist = [await Client.arank_api(api=url) for url in tqdm.tqdm(chain.urls('grpc'),unit='url',desc=f'ranking urls for {chain_id}')]
        apislist = chain.urls('grpc')
        Client('grpc+http://') # ensure runtime initialization occurs
        apispeedvers = await tqdm.asyncio.tqdm.gather(*[
            Client.arank_api(api=url) for url in apislist
        ],unit='url',desc=f'ranking urls for {chain_id}')
        apislist = [(*apispeedvers[idx], apislist[idx]) for idx in range(len(apislist)) if apispeedvers[idx][0] > 0]
        apislist.sort(reverse=True)
        apislist = [url for speed, ver, url in apislist]
        #apislist += [x.replace('http:','https:') if 'http:' in x else x.replace('https:','http:') for x in apislist]
        apierrs={}
        for api in tqdm.tqdm(apislist,desc=f'trying apis for {chain_id}',leave=False):#chain.apis.values():
            try:
                client = await chain.aclient(url=api)
                cached_delegations = {}
                #tip_spot = await self.Spot.acreate(self, api)
                block1 = (await client.cosmos_base_tendermint.aGetLatestBlock()).block.header
                commission, validators = await util.aget_min_commission_validators(client)
                #next(await client.cosmos_staking.aValidatorDelegations(validator_addr = validators[0]))
                for validator in validators[:1]:
                    delegations = client.cosmos_staking.aValidatorDelegations(validator_addr = validator)
                    async for delegation in delegations:
                        reward = await client.cosmos_distribution.aDelegationRewards(
                            delegator_address = delegation.delegation.delegator_address,
                            validator_address = delegation.delegation.validator_address
                        )
                        break
                break
            except AssertionError as err:
                apierrs[api] = err
            except Client.Errors as err:
                apierrs[api] = err
            except google.protobuf.json_format.ParseError as err:
                apierrs[api] = err
            except util.requests_exceptions.ConnectionError as err:
                apierrs[api] = err
            except util.requests_exceptions.ReadTimeout as err:
                apierrs[api] = err
            except RuntimeError as err:
                apierrs[api] = err
        else:
            import socket
            for type, id, addr in chain.peers.values():
                try:
                    socket.create_connection(addr.rsplit(':',1), timeout=1).close()
                    print(f'{chain.name} {chain.chain_id} peer responds but no listed api')
                    print(apierrs)
                    break
                except:
                    pass
            continue
        client = await chain.aclient(url=api, height=block1.height - 1)
        client2 = await chain.aclient(url=api, height=block1.height - 16)
        block2 = (await client.cosmos_base_tendermint.aGetBlockByHeight(height = block1.height - 16)).block.header
        est_blocks_per_year = 365.25 * 24 * 60 * 60 * (block1.height - block2.height) * 1000000000 / (block1.time.ToNanoseconds() - block2.time.ToNanoseconds())
        height3 = max(block1.height - int(est_blocks_per_year/4), 1)
        try:
            block3 = (await client.cosmos_base_tendermint.aGetBlockByHeight(height = height3)).block.header
        #except RuntimeError as err:
        #    msg = err.args[0]
        #    if 'lowest height is ' in msg:
        #        height3 = int(msg.split('lowest height is ',1)[1].split(':',1)[0].split('"',1)[0])
        #        if height3 + 300000 < block1.height:
        #            height3 += 300000
        #        else:
        #            height3 += 5
        #        block3 = client.cosmos_base_tendermint.GetBlockByHeight(height = height3).block.header
        #    else:
        #        raise
        except client.Errors as err:
            status, msg = client.error_status_message(err)
            if 'lowest height is ' in msg:
                height3 = int(msg.split('lowest height is ',1)[1].split(':',1)[0].split('"',1)[0])
                if height3 + 300000 < block1.height:
                    height3 += 300000
                else:
                    height3 += 5
                block3 = (await client.cosmos_base_tendermint.aGetBlockByHeight(height = height3)).block.header
            else:
                raise
        days_per_year = Fraction('365.25')
        real_blocks_per_year = days_per_year * 24 * 60 * 60 * (block1.height - block3.height) * 1000000000 / (block1.time.ToNanoseconds() - block3.time.ToNanoseconds())
        real_blocks_per_year = ((block1.height - block3.height)*days_per_year*60*60*24) * 1000000000 /(block1.time.ToNanoseconds() - block3.time.ToNanoseconds())
        try:
            nominal_bpr = await util.aget_bpr(client)#nominal_blocks_per_year)
            nominal_max_wpr = (1 + nominal_bpr) ** (real_blocks_per_year / days_per_year * 7) - 1
            print(f'{chain_id} nominal max wpr = {float(nominal_max_wpr)} %')
        except grpc.RpcError:
            pass#continue
        #nominal_blocks_per_year = client.get_blocks_per_year()
        maxwpr = (0,)
        for validator in validators[:2]:
            dct = 0
            delegations = cached_delegations.get(validator)
            if delegations is None:
                #delegations = client.get_validator_delegations(validator)
                delegations = client.cosmos_staking.aValidatorDelegations(validator_addr = validator)
            async for delegation in delegations:
                if dct >= 2:
                    break
                #reward = client.get_reward(delegation.delegation.delegator_address, delegation.delegation.validator_address)
                reward = await client.cosmos_distribution.aDelegationRewards(
                    delegator_address = delegation.delegation.delegator_address,
                    validator_address = delegation.delegation.validator_address,
                )
                try:
                    #reward2 = client2.get_reward(delegation.delegation.delegator_address, delegation.delegation.validator_address)
                    reward2 = await client2.cosmos_distribution.aDelegationRewards(
                        delegator_address = delegation.delegation.delegator_address,
                        validator_address = delegation.delegation.validator_address,
                    )
                except:
                    continue
                reward = [int(r.amount) for r in reward]
                reward2 = [int(r.amount) for r in reward2]
                assert len(reward) == len(reward2)
                #print(delegation, reward - reward2)
                client_height = int(client.metadata[client.X_COSMOS_BLOCK_HEIGHT])
                client2_height = int(client2.metadata[client2.X_COSMOS_BLOCK_HEIGHT])
                bpr = max([Fraction(reward[idx] - reward2[idx]) / (client_height - client2_height) / Fraction(delegation.delegation.shares) for idx in range(len(reward))])
                if bpr < 0:
                    continue
                wpr = (1+bpr) ** (real_blocks_per_year/365.25*7)-1
                apr = (1+bpr) ** (real_blocks_per_year)-1
                apr = bpr * real_blocks_per_year
                #apr = bpr * nominal_blocks_per_year
                print(chain.name, validator, float(wpr*100), '%')
                if (wpr,) > maxwpr:
                    maxwpr = (wpr, validator)
                    print(maxwpr)
                dct += 1
    
        continue
        nominal_blocks_per_year = client.get_blocks_per_year()
        real_blocks_per_year = ((block1.height - block2.height)*days_per_year*60*60*24)/(block1.time.timestamp() - yearblock.time.timestamp())
        annual_provisions = Fraction(client.mint.AnnualProvisions().annual_provisions.decode())
        bonded_tokens = Fraction(client.staking.Pool(None).pool.bonded_tokens)
        block_provision = annual_provisions / blocks_per_year
        bpr = block_provision / bonded_tokens
        max_wpr = (1 + bpr) ** (real_blocks_per_year / days_per_year * 7) - 1
        print(float(max_wpr), chain.name)
        for variant in variants:
            cfg = chain.config(variant, api)
            client = cosmpy2.LedgerClient(cfg)
            delegations = {}
            with tqdm.tqdm(total=5, desc=f'{chain.chain_id}.{variant[0]}') as pbar:
                for tx in client.iterate_txs(None, -1000):
                    for txlog in tx.logs:
                        evs = txlog.events
                        #if 'WithdrawDelegatorReward' in evs.get('message',{}).get('action'):
                        if 'withdraw_rewards' in evs:
                            print(evs['message']['action'])
                            delegator = evs['message']['sender']
                            validator = evs['withdraw_rewards']['validator']
                            amount = evs['transfer']['amount']
                            assert amount == evs['withdraw_rewards']['amount']
                            height = tx.height
                            delegations.setdefault(delegator,{}).setdefault(validator,{})[height]=amount
                            pbar.update(len(delegations)-pbar.n)
                    if len(delegations) > 5:
                        break
            print(chain.name, variant, delegations)
            print(chain.explorers)

if __name__ == '__main__':
    asyncio.run(amain())

import contextlib, datetime, decimal, fractions, os, warnings

import asn1crypto, certbuilder
import aiohttp, ssl
import yaml

from . import util
from .client import Client
from .tx import Transaction
from ._akash import SDL, AsyncShellTransport

CHAIN_ID = 'akashnet-2'
PROTOBUF = 'akash_protobuf'

class Provider:
    # protocol in provider/gateway/rest notably router.go client.go
    def __init__(self, client, cert_fn, owner, key_fn=None, password=None):
        self.akash = client
        self.address = owner
        ssl_ = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
        ssl_.check_hostname = False
        ssl_.verify_mode = ssl.CERT_NONE
        ssl_.load_cert_chain(cert_fn, key_fn, password=password)
        self._aiohttp_connector = self._PeerValidatingConnector(provider=self, ssl=ssl_)
    async def __ainit__(self):
        self.provider = await self.akash.akash_provider2.aProvider(self.address)
        # provider.host_uri
        # provider.attributes
        # provider.info

            # it looks like a simpler way to do this would be to pass the certificate from the
            # chain as the CA or fingerprint to the clientsession, although it would reduce
            # the checks
        self.asession = aiohttp.ClientSession(
            base_url=self.provider.host_uri,
            connector=self._aiohttp_connector,
            raise_for_status=True,
        )
    async def aclose(self):
        if hasattr(self, 'asession'):
            await self.asession.close()


    async def asubmitmanifest(self, dseq:int, manifest):
        async with self.asession.put(
                f'/deployment/{dseq}/manifest',
                allow_redirects = False,
                json = manifest,
        ) as response:
            return await response.text() # read() text() json()

    async def aleasestatus(self, dseq:int,gseq:int=1,oseq:int=1):
        async with self.asession.get(
                f'/lease/{dseq}/{gseq}/{oseq}/status',
                allow_redirects = False,
        ) as response:
            return await response.json() # read() text() json()

    async def aleaselogs(self, dseq, gseq=1, oseq=1, follow=True, services=None):
        params = dict(
            follow = 'true' if follow else 'false'
        )
        if services is not None:
            params['services'] = services
        async with self._ws(
            f'/lease/{dseq}/{gseq}/{oseq}/logs',
            **params
        ) as ws:
            async for log in ws:
                logdict = log.json()
                yield logdict['name'], logdict['message']
    @contextlib.asynccontextmanager
    async def _ws(self, url, **params):
        while True:
            try:
                async with self.asession.ws_connect(url, params=params) as ws:
                    yield ws
                    return
            except aiohttp.ClientConnectionError as exc:
                if self.akash.retry_conn_failures:
                    print(exc)
                    continue
                else:
                    raise

    @contextlib.asynccontextmanager
    async def aleaseshell(self, dseq:int, service:str, cmd:list, tty:bool = False, stdin:bool = True, gseq:int=1, oseq:int=1, pod_index:int=0):
        params=dict(
            service=service,
            podIndex=str(int(pod_index)),
            tty=int(tty),
            stdin=int(stdin),
        )
        if type(cmd) is str:
            cmd = [cmd]
        for cmd_idx in range(len(cmd)):
            params[f'cmd{cmd_idx}'] = cmd[cmd_idx]
        async with self._ws(
                f'/lease/{dseq}/{gseq}/{oseq}/shell',
                **params,
        ) as ws:
            transport = AsyncShellTransport(ws)
            yield transport._stdin, transport._stdout, transport._stderr, transport.terminal_resize

    class _PeerValidatingConnector(aiohttp.TCPConnector):
        def __init__(self, provider, *params, **kwparams):
            super().__init__(*params, **kwparams)
            self.__provider = provider
        async def __validate_peer_cert(self, transport):
            peercert_der = transport._ssl_protocol._extra['ssl_object'].getpeercert(True)
            peercert = certbuilder.asymmetric.load_certificate(peercert_der)
            #peercert = asn1crypto.x509.Certificate.load(peercert_der)
            peercertsubject = {typeval['type'].human_friendly: typeval['value'].native for typeval in peercert.asn1.subject.chosen[0]}
            #assert peercert.Subject.CommonName == peercert.Issuer.CommonName
            assert peercertsubject['Common Name'] == self.__provider.address
            assert peercert.asn1.serial_number
            matching_certs = [cert async for cert in self.__provider.akash.akash_cert2.aCertificates(
                filter = dict(
                    owner = self.__provider.address,
                    serial = str(peercert.asn1.serial_number),
                    state = 'valid',
                )
            )]
            assert len(matching_certs) == 1
            assert matching_certs[0].certificate.state == matching_certs[0].certificate.State.valid
            # the peer cert is then verified by placing it in an empty pool and verifying it against that pool
            # using DNSName = hostname, CurrentTime = now, KeyUsages = [ExtKeyUsageServerAuth], MaxConstraintComparisons = 0
            # in provider/gateway/rest/client.go verifyPeerCertificate (2023-07-19)
            # i'm guessing instead the certificate from the chain should be placed in the pool
            # but it's likely more robust to simply compare that it's the same certificate
            # although that doesn't include the hostname verification
            onchain_cert_pem = matching_certs[0].certificate.cert
            onchain_pubkey_pem = matching_certs[0].certificate.pubkey
            session_cert_pem = certbuilder.pem_armor_certificate(peercert)
            session_pubkey_pem = Booster._pem_armor_public_key(peercert.public_key)
            assert onchain_cert_pem == session_cert_pem
            assert onchain_pubkey_pem == session_pubkey_pem

        async def _create_proxy_connection(self, *params, **kwparams):
            transp, proto = await super()._create_proxy_connection(*params, **kwparams)
            await self.__validate_peer_cert(transp)
            return transp, proto
        async def _create_direct_connection(self, *params, **kwparams):
            transp, proto = await super()._create_direct_connection(*params, **kwparams)
            await self.__validate_peer_cert(transp)
            return transp, proto


class Booster:
    def __init__(self, wallet, password, certkey_fn = None, concurrent = False, **kwparams):
        self.wallet = wallet
        akash = self.wallet.client
        GroupSpec_1_3 = util.import_module(akash.protobuf).akash.deployment.v1beta3.groupspec_pb2.GroupSpec
        GroupSpec_1_2 = util.import_module(akash.protobuf).akash.deployment.v1beta2.groupspec_pb2.GroupSpec
        self._GroupSpec = GroupSpec_1_2
        self._certkeyfn = certkey_fn
        self.__password = password
        self._concurrent = concurrent
        try:
            self._mkcert(**kwparams)
        except FileExistsError:
            self._rdcert()
    async def __ainit__(self):
        async for cert in self.certs():
            if cert.asn1.contents == self.cert.asn1.contents:
                break
        else:
            await self._publishcert(self.cert, self.pubkey)

    @staticmethod
    def _pem_armor_public_key(pubkey):
        der = certbuilder.asymmetric.dump_public_key(pubkey, 'der')
        pem = asn1crypto.pem.armor(f'{pubkey.algorithm.upper()} PUBLIC KEY', der)
        return pem
    @staticmethod
    def _pem_armor_private_key(privkey, passphrase, target_ms=200):
        der = certbuilder.asymmetric.dump_private_key(privkey, passphrase, 'der', target_ms)
        #pem = asn1crypto.pem.armor(f'{"ENCRYPTED " if passphrase else ""}{privkey.algorithm.upper()} PRIVATE KEY', der)
        # the key algorithm is removed for easy compatibility with ssl.SSLContext
        pem = asn1crypto.pem.armor(f'{"ENCRYPTED " if passphrase else ""}PRIVATE KEY', der)
        return pem

    async def _publishcert(self, cert, pubkey):
        cert_pem = certbuilder.pem_armor_certificate(cert)
        pubkey_pem = self._pem_armor_public_key(pubkey)
        async with self.wallet.atx(concurrent=self._concurrent, desc='publishing new cetrificate') as tx:
            tx.cert2_createcertificate(self.wallet.address, cert_pem, pubkey_pem)

    @contextlib.asynccontextmanager
    async def deploy(self, deployment, deposit=5000000, depositor=None, dseq=None):
        akash = self.wallet.client
        dseq, manifest = await self._deploy(deployment, deposit, depositor, dseq)
        try:
            while True:
                provider_address = await self.accept(dseq)
                try:
                    id = dict(dseq=dseq, owner=self.wallet.address, provider=provider_address, oseq=1, gseq=1)
                    async with util.await_poll(akash) as await_poll:
                        lease = None
                        while lease is None or lease.lease.state != lease.lease.State.active:
                            await await_poll(f'{dseq}:{lease and lease.lease.state}')
                            lease = await akash.akash_market2.aLease(id)
                    break
                except self.wallet.client.Errors as error:
                    status, message = self.wallet.client.error_status_message(error)
                    if status == self.wallet.client.Statuses.UNKNOWN and 'lease not found' in manifest:
                        continue
                    import pdb; pdb.set_trace()
                    raise
            provider = Provider(
                    akash,
                    self._certkeyfn,
                    provider_address,
                    password = self.__password,
            )
            await provider.__ainit__()
            # this inlines the send_manifest function
            # likely clearer to cache provider
            #import pdb; pdb.set_trace()
            async with contextlib.aclosing(provider):
                await provider.asubmitmanifest(dseq, manifest)
                async with util.await_poll(self.wallet.client, desc='waiting for at least 1 service to become available') as aupdate:
                    while True:
                        try:
                            status_result = await provider.aleasestatus(dseq)
                        except aiohttp.client_exceptions.ClientResponseError as exc:
                            if exc.status == 404:
                                print(exc)
                                continue
                            raise
                        if any([service['available'] for service in status_result['services'].values()]):
                            break
                        await aupdate()
                yield provider, dseq, list(status_result['services'].keys())
        finally:
            await self.close_deployment(dseq)

    async def _bids(self, dseq, oseq=1, gseq=1, owner=None):
        if owner is None:
            owner = self.wallet.address
        return [bid async for bid in self.wallet.client.akash_market2.aBids(filters=dict(
            owner=owner, dseq=dseq, oseq=oseq, gseq=gseq
        ))]

    async def min_bid(self, dseq, oseq=1, gseq=1, owner=None):
        async with util.await_poll(self.wallet.client) as aupdate:
            bids = await self._bids(dseq)
            while not bids:
                await aupdate(desc='looking for bids')
                bids = await self._bids(dseq)
        bids.sort(key=lambda bid: int(bid.bid.price.amount))
        bid = bids[0]
        return bid.bid.bid_id.provider, bid

    async def accept(self, dseq, provider=None, gseq=1, oseq=1, owner=None):
        if owner is None:
            owner = self.wallet.address
        if provider is None:
            provider, bid = await self.min_bid(dseq, gseq, oseq, owner)
        id = dict(dseq=dseq,provider=provider,gseq=gseq,oseq=oseq,owner=owner)
        async with self.wallet.atx(concurrent=self._concurrent, desc=f'leasing from {provider}') as tx:
            tx.market2_createlease(id)
        return provider

    async def close_deployment(self, dseq, owner = None):
        akash = self.wallet.client
        if owner is None:
            owner = self.wallet.address
        id = dict(owner=owner, dseq=dseq)
        deployment = await akash.akash_deployment2.aDeployment(id)
        assert deployment.deployment.state != 'closed'
        async with self.wallet.atx(concurrent=self._concurrent, desc=f'closing deployment {dseq}') as tx:
            tx.deployment2_closedeployment(id)

    async def _deploy(self, deployment, deposit=5000000, depositor=None, dseq=None):
        akash = self.wallet.client
        B = float((await akash.cosmos_bank.aBalance(self.wallet.address, 'uakt')).amount) / 1000000
        assert B >= 5.5 # make sure to have at least 5.5. AKT
    
        sdl = SDL.from_yaml(deployment)
        groups, manifest, version = sdl.groups_manifest_version(GroupSpec = self._GroupSpec)
        
        if dseq is None:
            dseq = (await akash.cosmos_base_tendermint.aGetLatestBlock()).block.header.height
    
        if depositor is None:
            depositor = self.wallet.address
    
        tx = Transaction(self.wallet)
        tx.deployment2_createdeployment(
            id=dict(owner=self.wallet.address, dseq=dseq),
            groups=groups,
            version=version,
            deposit=tx.Coin(amount=deposit,denom='uakt'),
            depositor=depositor,
        )
        tx = await tx.asignbroadcast(0, 'uakt', concurrent=self._concurrent, desc=f'creating deployment {dseq}')
        
        id = akash._resolve_any(tx.tx.body.messages[0]).id
        # oseq defaults to 1
        # there may be multiple gseq in the depoyment
        return id.dseq, manifest
        
    def _mkcert(self, start_time = None, valid_duration = datetime.timedelta(days=365), domains=[]): # , password
        with open(self._certkeyfn, 'xb') as certkeyfile:
            if start_time is None:
                start_time = datetime.datetime.now()
            else:
                start_time = datetime.datetime.fromisoformat(start)
            start_time = start_time.astimezone()
            end_time = start_time + valid_duration
            # https://github.com/akash-network/node/blob/main/x/cert/utils/key_pair_manager.go#L129 generateImpl
            #AuthVersion = certbuilder.x509.NameTypeAndValue(dict(
            #    type = 'platform_version',#'2.23.133.2.6',
            #    value = 'v0.0.1',
            #))
            pubkey, priv = certbuilder.asymmetric.generate_pair("ec", curve="secp256r1")
            subject = asn1crypto.x509.Name(name='', value=asn1crypto.x509.RDNSequence([
                asn1crypto.x509.RelativeDistinguishedName([asn1crypto.x509.NameTypeAndValue(dict(type='common_name',value=asn1crypto.x509.DirectoryString(name='utf8_string',value=asn1crypto.core.UTF8String(self.wallet.address))))]),
                asn1crypto.x509.RelativeDistinguishedName([asn1crypto.x509.NameTypeAndValue(dict(type='platform_version',value=asn1crypto.core.UTF8String('v0.0.1')))]),
            ]))
            issuer = asn1crypto.x509.Name(name='', value=asn1crypto.x509.RDNSequence([
                asn1crypto.x509.RelativeDistinguishedName([asn1crypto.x509.NameTypeAndValue(dict(type='common_name',value=asn1crypto.x509.DirectoryString(name='utf8_string',value=asn1crypto.core.UTF8String(self.wallet.address))))]),
            ]))
            cert = certbuilder.CertificateBuilder(
                #dict(
                #    common_name = self.wallet.address,
                ##    platform_version = 'v0.0.1',
                ##    #extra_names = AuthVersion,
                #),
                subject,
                pubkey
            )
            cert._issuer = issuer
            cert.begin_date = start_time
            cert.end_date = end_time
            cert.key_usage = {'data_encipherment', 'key_encipherment'}
            if domains:
                cert.extended_key_usage = {'client_auth', 'server_auth'}
                raise NotImplementedError('adding domains and ips to certs')
            else:
                cert.extended_key_usage = {'client_auth'}
            #cert = certbuilder.pem_armor_certificate(cert.build(priv))
            #key = certbuilder.asymmetric.dump_private_key(priv, password, 'pem')
            cert = cert.build(priv)
            #pub_pem = certbuilder.asymmetric.dump_public_key(pubkey, 'pem')
            certbytes = certbuilder.pem_armor_certificate(cert)
            keybytes = self._pem_armor_private_key(priv, self.__password)
            certkeyfile.write(certbytes+keybytes)
            self.cert = cert
            self.pubkey = pubkey
            self.privkey = priv
    def _rdcert(self):
        warnings.warn('certs expire, need code for handling')
        with open(self._certkeyfn, 'rb') as certkeyfile:
            certkey_bytes = certkeyfile.read()
        cert_bytes, key_bytes = [
            asn1crypto.pem.armor(name, der_bytes, headers)
            for name, headers, der_bytes
            in asn1crypto.pem.unarmor(certkey_bytes, multiple=True)
        ]
        assert b' EC PRIVATE KEY' not in key_bytes # removed ' EC' for compatibility with ssl.SSLContext
        #key_bytes = key_bytes.replace(b' EC PRIVATE KEY-----\n', b' PRIVATE KEY-----\n')
        cert = certbuilder.asymmetric.load_certificate(cert_bytes)
        key = certbuilder.asymmetric.load_private_key(key_bytes, self.__password)
        self.cert = cert
        self.pubkey = key.public_key
        self.privkey = key

    async def certs(self):
        akash = self.wallet.client
        async for cert in akash.akash_cert2.aCertificates(filter=dict(owner=self.wallet.address)):
            yield certbuilder.asymmetric.load_certificate(cert.certificate.cert)
    async def deployments(self, dseq=None, state=None, owner=''):
        if owner == '':
            owner = self.wallet.address
        akash = self.wallet.client
        return [deployment async for deployment in akash.akash_deployment2.aDeployments(filters=dict(owner=owner,dseq=dseq,state=state))]



async def amain():
    from .cosmoschains import FileRegistry as Registry
    from .wallet import Wallet
    chain = Registry().chain('akashnet-2')
    akash = await chain.aclient(protobuf = 'akash_protobuf')
    akash.retry_conn_failures = True
    wallet = Wallet(chain, client=akash)
    await wallet.__ainit__()
    booster = Booster(wallet, wallet.seed_phrase, 'akash.cert', concurrent=True)
    await booster.__ainit__()
    for deployment in await booster.deployments(state='active'):
        if True: #deployment.deployment.deployment_id.dseq != 12119865:
            #print(deployment.deployment.deployment_id)
            await booster.close_deployment(deployment.deployment.deployment_id.dseq)
    assert os.path.exists('akash/planq.deploy.yaml')
    async with booster.deploy('akash/planq.deploy.yaml') as (provider, dseq, (service,)):
        print(dseq)
        logs = util.AccumulateStreams((msg async for name, msg in provider.aleaselogs(dseq)), store=False, verbose=True)
        await asyncio.sleep(8)
        async with provider.aleaseshell(dseq, service, ['/usr/bin/env', 'bash'], tty=True) as (stdin, stdout, stderr, terminal_resize):
            output = util.AccumulateStreams(stdout, stderr, verbose=True, store=False)
            stdin.write(b'ls -la\n')
            stdin.write(b'exit\n')
            await output.join()
    await logs.join()

if __name__ == '__main__':
    import asyncio
    asyncio.run(amain())

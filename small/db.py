import asyncio, json, os, tqdm, weakref

from .util import diskcache, deterministic_hash, UNSET, AsyncDictProcessor, async_in_thread
import sqlite3

class Column:
    # this uses static class members but would maybe be easier to use if it used annotations instead
    def __init__(self, type, default = None, primary = False, migrate = None, merge = None):
        self.default = default
        self.primary = primary
        self.merge = merge
        self.migrate = migrate
        self.type = type

        self.sql_type = {
            str: ' TEXT',
            int: ' INTEGER',
            bytes: ' BLOB',
            float: ' REAL',
        }.get(self.type, '')

        self.sql_val = {
            int: repr,
            float: repr,
            str: repr,
            dict: json.dumps,
        }[self.type]

    def _name(self, name):
        self.name = name
        return self
    def sql_def(self, include_primary=True):
        sql = ''

        sql += self.name

        sql += self.sql_type

        null = self.default is None
        if not null:
            sql += ' NOT NULL'
        
        if self.default is not None:
            sql += ' DEFAULT ' + repr(self.default)

        if include_primary:
            if self.primary:
                sql += ' PRIMARY KEY'
        
        return sql
    def __bool__(self):
        return False

class Instance:
    @classmethod
    async def adbget(cls, *values, **kwvalues):
        instance = await async_in_thread(cls.dbget, *values, **kwvalues)
        row = await instance.dbfuture
        assert instance.__row == row and "concurrent writes to db not implemented"
        return instance

    @classmethod
    def dbget(cls, *values, **kwvalues):
        table = cls.table()
        with table.writing_queue.lock_threading:
            vals = table.values_to_row(*values, **kwvalues)
            key = table.row_to_key(vals)
            instance = cls._instantiated.get(key)
            if instance is None:
                old, new, fut = table.row_put_to_row(key, None, vals)
                instance = cls(key, new, fut)
                #print("dbget", instance.addr[:5], "created = ", hash(repr(instance.__row)))
            else:
                row = instance.__row
                #print("dbget", row['addr'][:5], "row = ", hash(repr(row)))
                old, new, fut = table.row_put_to_row(key, row, vals)
                instance.__update(new, fut)
            return instance

    # i left off considering the uses of dbget and dbput.
    # some are appropriate to be async (use the return value), others sync.
    # figuring for design good to stay async to ease exception propagation
    @classmethod
    def dball(cls):
        table = cls.table()
        for key in table.all_keys():
            val = cls._instantiated.get(key, UNSET)
            if val is not UNSET:
                yield val
            else:
                row = table.key_to_row(key)
                yield cls(key, row)

    def dbput(self):#, *columns):
        #print('dbput start', self.addr)
        table = self.table()
        with table.writing_queue.lock_threading:
            row = table.instance_to_row(self)
            #print("dbput 1", self.__row['addr'][:5], "row = ", hash(repr(self.__row)), 'upd to', hash(repr(row)))
            row_ = row
            assert all([getattr(self, key) == val for key, val in row.items()])
            old, row, fut = table.row_put_to_row(self.__key, self.__row, row)
            #print('dbput 15', self.__row['addr'][:5], 'rptr old =', hash(repr(old)), 'row =', hash(repr(row)))
            assert self.__row == old
            assert all([getattr(self, key) == val for key, val in row.items()])
            self.__update(row, fut)
            #print("dbput 2", hash(self.__key), "row = ", hash(repr(self.__row)))
            assert self.__row == row
            #print('dbput assert', self.addr, self.dbfuture, fut)
            assert fut is None or self.dbfuture == fut
       #     print('dbput return', self.addr)
            return self

    async def adbput(self):
        #import pdb; pdb.set_trace()
        row = await self.dbput().dbfuture
        assert self.__row == row
        return self

    #@classmethod
    #def bulk(cls):
    #    table = cls.table()
    #    return table.db.transact()

    _instantiated = weakref.WeakValueDictionary()
    def __init__(self, key, row, fut = None):
        assert key not in self._instantiated
        self._instantiated[key] = self
        table = self.table()
        assert not (set(row.keys()) - table.colnames_set) and "unknown row elements"
        self.__key = key
        self.__update(row, fut)
        print('len(_instantiated) =', len(self._instantiated))
    def __update(self, row, fut):
        #print('UPDATE', row['addr'], fut)
        if row is not None:
            self.__row = row
            #print("__update", self.__row['addr'][:5], "row set to ", hash(repr(self.__row)))
            self.__dict__.update(row)
        if hasattr(self, 'dbfuture') and self.dbfuture is not fut:
            if fut is None:
                fut = self.dbfuture
                #return
            else:
                #if self.dbfuture.cancelled():
                #    exc = self.dbfuture.exception()
                #    raise exc from exc
                if not self.dbfuture.done():
                    import threading
                    print(threading.current_thread().ident, self.__key, 'replacing incomplete future !!', self.__row['addr'])
                    import pdb; pdb.set_trace()
                    # the dbfuture should have been reused via dictqueue and passed back
                    print(threading.current_thread().ident, self.__key, 'replacing incomplete future !!')
                assert self.dbfuture.result() # raises if not done
        elif fut is None:
            #print(threading.current_thread().ident, self.__key, 'making new completed fut')
            fut = asyncio.Future()
            #print("__update", self.__row['addr'][:5], "set result = ", hash(repr(self.__row)))
            fut.set_result(self.__row)
        #print(threading.current_thread().ident, self.__key, 'assigning dbfut')
        self.dbfuture = fut

    @classmethod
    def table(cls):
        _table = cls._table
        cls._table = Table(cls)
        cls.table = _table
        return _table()
    @classmethod
    def _table(cls):
        return cls._table

class Table:
    def __init__(self, Instance, dbver=0):
        self.name = Instance.__name__
        self.Instance = Instance
        self.cols_dict = {
            name: attr._name(name)
            for name, attr in Instance.__dict__.items()
            if type(attr) is Column
        }
        self.cols_num = len(self.cols_dict)
        self.cols_list = list(self.cols_dict.values())
        self.colnames_list = list(self.cols_dict.keys())
        self.colnames_set = set(self.colnames_list)
        self.primarynames_list = [
            name
            for name, col in self.cols_dict.items()
            if col.primary
        ]
        self.nonprimarynames_list = [
            name
            for name, col in self.cols_dict.items()
            if not col.primary
        ]
        self.primaryidcs_list = [
            idx
            for idx in range(self.cols_num)
            if self.cols_list[idx].primary
        ]
        self.default_values_dict = {
            name: col.default
            for name, col in self.cols_dict.items()
            if col.default is not None
            and type(col.default) is not type
        }
        self.default_constructors_dict = {
            name: col.default
            for name, col in self.cols_dict.items()
            if col.default is not None
            and type(col.default) is type
        }
        self.migrates_dict = {
            name: col.migrate
            for name, col in self.cols_dict.items()
            if col.migrate is not None
        }
        self.merges_dict = {
            name: col.merge
            for name, col in self.cols_dict.items()
            if col.merge is not None
        }
        self.writing_queue = AsyncDictProcessor(
            #process = self._do_writing,
            process = lambda dict: async_in_thread(self._do_writing, dict),
            merge = self._rows_to_merged_row
        )
        dbver0path = os.path.join('_db', self.name) + '.db'
        dbver1path = '_db.sqlite3'
        if os.path.exists(dbver0path):
            '''there were nondeterministic keys in db0'''
            self.dbver = 0
            self.db = diskcache.persistent.Index(dbver0path)
            if not self.db.get('__rekeyed'):
                for key in tqdm.tqdm(list(self.all_keys()), desc= 'rekeying db0', unit='row'):
                    row = self.key_to_row(key, invalid_keys=True)
                    if row is None:
                        continue
                    if self.primarynames_list and not any([name in row for name in self.primarynames_list]):
                        del self.db[key]
                        continue
                    rowkey = self.row_to_key(row)
                    if rowkey != key:
                        row0 = self.key_to_row(rowkey, {})
                        row = self._rows_to_merged_row(row0, row)
                        self.db[rowkey] = row
                        del self.db[key]
                self.db['__rekeyed'] = 1
        self.dbver = dbver
        assert sum([os.path.exists(path) for path in [dbver0path, dbver1path]]) <= 1 and "was a migration interrupted maybe delete the new one and remigrated" # it could use merge callback to unify the rows but it's a little confusing because the keys are made differently, maybe break key function up
        if dbver == 0:
            assert not os.path.exists(dbver1path)
            self.db = diskcache.persistent.Index(dbver0path)
        else:
            if not hasattr(self, 'db_connection'):
                assert sqlite3.threadsafety and "otherwise nred lock for dbget and possible different tables"
                cls = type(self)
                cls.db_conn = sqlite3.Connection(dbver1path, cached_statements=1024*1024, check_same_thread=False)
                cls.db_cu = cls.db_conn.cursor()
                cls.db_cu_writing = cls.db_conn.cursor()
            self.primarynames_sql = ', '.join(self.primarynames_list)
            self.colnames_sql = ', '.join(self.colnames_list)
            self.nonprimarynames_sql = ', '.join(self.nonprimarynames_list)
            self._sql_params_positional = ', '.join(['?'] * self.cols_num)
            self._sql_params_named = ', '.join([f':{name}' for name in self.colnames_list])
            self._sql_rows_put_to_rows_positional = f'INSERT OR REPLACE INTO {self.name} ( {self.colnames_sql} ) VALUES ( {self._sql_params_positional} )'
            self._sql_rows_put_to_rows_named = f'INSERT OR REPLACE INTO {self.name} ( {self.colnames_sql} ) VALUES ( {self._sql_params_named} )'
            self._sql_select_keys = f'SELECT {self.primarynames_sql} FROM {self.name}'
            self._sql_select_nonkeys = f'SELECT {self.nonprimarynames_sql} FROM {self.name}'
            self._sql_key_cond = ' AND '.join([f'{name} = ?' for name in self.primarynames_list])
            self._sql_select_row = f'SELECT {self.colnames_sql} FROM {self.name} WHERE {self._sql_key_cond} LIMIT 1'
            with self.db_conn:
                db = self.db_cu
                db.execute('PRAGMA journal_mode=WAL')
                db.execute(
                    'CREATE TABLE IF NOT EXISTS ' + self.name + ' (' +
                    ', '.join([col.sql_def() for col in self.cols_list])
                    + ')')
                while True:
                    try:
                        db.execute(
                            'SELECT ' +
                            ', '.join([col.name for col in self.cols_list])
                            + ' FROM ' + self.name + ' LIMIT 0'
                        )
                        break
                    except sqlite3.OperationalError as err:
                        errstr = str(err)
                        if errstr.startswith('no such column: '):
                            colname = errstr[len('no such column: '):]
                            coldef = self.cols_dict[col].sql_def()
                            db.execute('ALTER TABLE ' + self.name + ' ADD COLUMN ' + coldef)
                            continue
                        else:
                            raise
            if os.path.exists(dbver0path):
                print('db migration')
                dbver0 = diskcache.persistent.Index(dbver0path)
                assert len(list(self.all_keys())) == 0
                with self.db_conn:
                    self.db_cu.executemany(self._sql_rows_put_to_rows_named, dbver0.values())
                #dbver0.clear()
                dbver0.cache.close()
                os.rename(dbver0path, dbver0path + '.migrated')
    def all_keys(self):
        if self.dbver == 0:
            return self.db.keys()
        else:
            # make new cursor so iterator stays valid
            return self.db_conn.execute(self._sql_select_keys)
    def key_to_row(self, key, default = None, invalid_keys = False):
        try:
            row, fut, dbrow = self.writing_queue[key]
            #print(row['addr'][:5], "fetched row from dict", hash(repr(row)))
        except KeyError:
            if self.dbver == 0:
                row = self.db.get(key, default)
            else:
                values = self.db_cu.execute(self._sql_select_row, key).fetchone()
                if values is None:
                    row = default
                else:
                    row = {self.colnames_list[idx]: values[idx] for idx in range(self.cols_num)}
            #if row is not default:
            #    print(row['addr'][:5], "made row via database", hash(repr(row)))
        if row is not default:
            for colname, migrate in self.migrates_dict.items():
                migrated_col = migrate(row, row.get(colname, UNSET))
                if migrated_col is not UNSET:
                    assert migrated_col is not None
                    row[colname] = migrated_col
                elif colname in row:
                    del row[colname]
            if not invalid_keys:
                assert key == self.row_to_key(row)
        return row
    def values_to_row(self, *positionals, **keywords):
        positional_names = self.colnames_list[:len(positionals)]
        keys_set = set(keywords.keys())
        assert not (set(positional_names) & keys_set) and "positional params overlap keywords"
        assert not (keys_set - self.colnames_set) and "unknown keywords"
        row = {
            positional_names[idx] : positionals[idx]
            for idx in range(len(positionals))
        }
        row.update(keywords)
        return row
    def row_to_key(self, row):
        return self.primaries_to_key([
            row[colname] for colname in self.primarynames_list
        ])
    def instance_to_row(self, instance):
        return {
            colname: getattr(instance, colname)
            for colname in self.colnames_list
        }
            
    def row_put_to_row(self, key, old_row, new_row):
        with self.writing_queue.lock_threading: # speedup if this lock is per-item, or other solution to address race of get during put
            if old_row is None:
                old_row = self.key_to_row(key, {})
                #print('row_put_to_row', new_row['addr'][:5], 'from db', hash(repr(old_row)), hash(repr(new_row)))
            #else:
            #    print('row_put_to_row', new_row['addr'][:5], 'from mem', hash(repr(old_row)), hash(repr(new_row)))
            old_vals = [old_row.get(colname,UNSET) for colname in self.colnames_list]#list(old_row.values())
            #old_vals = [old_row.get(colname) for colname in self.colnames_list]#list(old_row.values())
            new_row = self._rows_to_merged_row(old_row, new_row)
            for name, constructor in self.default_constructors_dict:
                if name not in new_row:
                    new_row[name] = constructor()
            for colname in self.colnames_list:
                new_row.setdefault(colname, self.default_values_dict.get(colname))
            assert len(new_row) == self.cols_num
            #assert list(new_row.keys()) == self.colnames_list and "if this always passes then the use of named bindings could be dropped"
            #if list(new_row.values()) != old_vals:
            if [new_row[colname] for colname in self.colnames_list] != old_vals:
                #print(new_row['addr'][:5], "enqueueing row", hash(repr(new_row)), "from", hash(repr(old_row)))
                fut, new_row = self.writing_queue.enqueue_sync(key, new_row, old_row)
            else:
                fut = None # row.__update then constructs a clashing future :/
            return old_row, new_row, fut
    def _rows_to_merged_row(self, row, *new_rows):
        row = {colname:row.get(colname,UNSET) for colname in self.colnames_list}
        for new_row in new_rows:
            for colname, merge in self.merges_dict.items():
                new_col = new_row.pop(colname, UNSET)
                if new_col is not UNSET:
                    old_col = row[colname]
                    if old_col is not UNSET:
                        new_col = merge(old_col, new_col) or old_col
                    row[colname] = new_col
            row.update(new_row)
        for colname, value in list(row.items()):
            if value is UNSET:
                del row[colname]
        return row
        
    def primaries_to_key(self, primary_keys):
        primary_keys = tuple(primary_keys)
        if self.dbver == 0:
            return deterministic_hash(primary_keys)
        else:
            return primary_keys
    def row_to_instance(self, row):
        raise NotImplementedError('use instance constructor')
    #async def _do_writingikeys_rowfutures:dict):
    #    with self.db.transact():
    #        for key, (row, fut) in keys_rowfutures.items():
    #            fut.set_result(self._row_put_to_row(key, row))
    #async def _do_writing(self, key_rows_futs:dict):
    def _do_writing(self, key_rows_futs:dict):
        print('beginning writing')
        # AsyncDictProcessor made kinda hastily, this stuff could be more organized
        if self.dbver == 0:
            with self.db.transact():
                print('write pass')
                with self.writing_queue.lock_threading:
                    keys = list(key_rows_futs.keys())
                print('write count of', len(keys))
                for key in tqdm.tqdm(keys, unit='row'):#, leave=False):
                    with self.writing_queue.lock_threading:
                        row, fut, orig_row = key_rows_futs.pop(key)
                        #print('writing', row['addr'])
                    try:
                        self.db[key] = row
                        #print('wrote', row['addr'])
                        if not fut.cancelled():
                            #print("db write", row['addr'][:5], "set result = ", hash(repr(row)))
                            fut.set_result(row)
                    except Exception as e:
                        print('exc', row['addr'], e)
                        import pdb; pdb.set_trace()
                        fut.set_exception(e)
        else:
            with self.db_conn:
                print('write pass')
                with self.writing_queue.lock_threading:
                    #inserting_rows = [row for key, (row, fut, orig) in key_rows_futs if not orig]
                    #updating_key_rows = [key, row for key, (row, fut, orig) in key_rows if orig]
                    rows_futs = list(key_rows_futs.values())
                    key_rows_futs.clear()
                print('write count of', len(key_rows_futs))
                try:
                    rowvals = [row for row, fut, orig in rows_futs]
                    self.db_cu_writing.executemany(self._sql_rows_put_to_rows_named, rowvals)
                    for row, fut, orig in rows_futs:
                        if not fut.cancelled():
                            fut.set_result(row)
                except Exception as e:
                    print('exc', e)
                    import pdb; pdb.set_trace()
                    for row, fut, orig in rows_futs:
                        fut.set_exception(e)
        print('done writing')

if __name__ == '__main__':
    class Record(Instance):
        name = Column(str, primary=True)
        number = Column(int)
    async def amain():
        for record in Record.dball():
            print(record.name, record.number)
        hello = await Record.adbget('hello')
        world = await Record.adbget('world')
        if not hello.number:
            hello.number = 1
            hello.dbput()
        if not world.number:
            world.number = 2
            world.dbput()
        await asyncio.gather(hello.dbfuture, world.dbfuture)
        for record in Record.dball():
            print(record.name, record.number)
    asyncio.run(amain())

import aiohttp, fractions, hashlib, json, os, re, struct
import asyncio.transports, asyncio.streams

import yaml
from google.protobuf.internal.encoder import _VarintBytes as VarintBytes

from . import util

class SDL:
    def __init__(self, yaml_dict):
        assert type(yaml_dict) is dict
        self.data = yaml_dict
        assert self.data['version'] == '2.0'

    @classmethod
    def from_yaml_dict(cls, yaml):
        return cls(yaml)
    @classmethod
    def from_yaml_str(cls, str):
        return cls.from_yaml_dict(yaml.safe_load(str))
    @classmethod
    def from_yaml_fn(cls, filename):
        with open(filename) as fh:
            return cls.from_yaml_str(fh.read())
    @classmethod
    def from_yaml(cls, fn_or_str_or_dict):
        if type(fn_or_str_or_dict) is str:
            if os.path.exists(fn_or_str_or_dict):
                return cls.from_yaml_fn(fn_or_str_or_dict)
            else:
                return cls.from_yaml_str(fn_or_str_or_dict)
        else:
            return cls.from_yaml_dict(fn_or_str_or_dict)

    def groups_manifest_version(self, GroupSpec = None):
        sdl = self.data
        groups = {}
        manifest3 = []
        _endpoint_names = [
            to.get('IP')
            for service_name in sdl['deployment']
            for expose in sdl['services'][service_name]['expose']
            for to in expose['to']
            if not to.get('global') or to.get('IP')
        ]
        _endpoint_names.sort()
        ip_endpoint_names = {
            endpoint_name: idx + 1
            for idx, endpoint_name in enumerate(_endpoint_names)
        }

        for svc_name, depl in sdl['deployment'].items():
            for placement_name, svcdepl in depl.items():
                compute = sdl['profiles']['compute'][svcdepl['profile']]
                svc = sdl['services'][svc_name]
                infra = sdl['profiles']['placement'][placement_name]
                price = infra['pricing'][svcdepl['profile']]

                group = groups.get(placement_name)
                if group is None:
                    attributes = list(infra['attributes'].items())
                    attributes.sort()
                    signed_by = infra.get('signedBy',infra.get('signed_by',{}))
                    group = groups.setdefault(placement_name,
                        dict(
                            name = placement_name,
                            requirements = dict(
                                attributes = [dict(key=key,value=val) for key, val in attributes],
                                signed_by = dict(
                                    all_of = signed_by.get('allOf',signed_by.get('all_of')),
                                    any_of = signed_by.get('anyOf',signed_by.get('any_of')),
                                )
                            ),
                            resources = [],
                        )
                    )
                    manifest3.append({
                        'Name': placement_name,
                        'Services': []
                    })
                sdlresources = compute['resources']
                resunits = {}
                resmanif = {}
                if sdlresources.get('cpu'):
                    val = sdlresources['cpu']['units']
                    if type(val) is str:
                        val = parse_cereal(val)
                    else:
                        val = int(val * 1000)
                    resmanif['cpu'] = {
                        'units': dict(val=str(val)),
                    }
                    resunits['cpu'] = {
                        'units': dict(val=str(val).encode()),#VarintBytes(val)),
                        'attributes': sdlresources['cpu'].get('attributes'),
                    }
                if sdlresources.get('memory'):
                    val = parse_cereal(sdlresources['memory']['size'])
                    resmanif['memory'] = {
                        'size': dict(val=str(val)), # 'quantity'
                    }
                    resunits['memory'] = {
                        'quantity': dict(val=str(val).encode()),#VarintBytes(val)),
                        'attributes': sdlresources['memory'].get('attributes'),
                    }
                storages = sdlresources['storage']
                if type(storages) is not list:
                    storages = [storages]
                resmanif['storage'] = [{
                    'name': storage.get('name', 'default'),
                    # 'quantity'
                    'size': dict(val=str(parse_cereal(storage['size']))),
                    **({'attributes': storage['attributes']} if 'attributes' in storage else {}),
                } for storage in storages]
                resunits['storage'] = [{
                    'name': storage.get('name', 'default'),
                    #'quantity': dict(val=VarintBytes(parse_cereal(storage['size']))),
                    'quantity': dict(val=str(parse_cereal(storage['size'])).encode()),
                    'attributes': storage.get('attributes'),
                } for storage in storages]
                resources = dict(
                    resources = resunits,
                    price = dict(denom=price['denom'],amount=str(price['amount']*10**18)),
                    count = svcdepl['count'],
                )
                manifest2_expose = []
                endpoints = []
                for expose in svc['expose']:
                    http_options = {
                        'max_body_size': 1048576,
                        'read_timeout': 60000,
                        'send_timeout': 60000,
                        'next_tries': 3,
                        'next_timeout': 0,
                        'next_cases': [
                            'error',
                            'timeout',
                        ],
                        **expose.get('http_options',{}),
                    }
                    for to in expose['to']:
                        if not to.get('global'):
                            continue
                        if to.get('global') and to.get('IP'):
                            seq_no = ip_endpoint_names[to['IP']]
                            endpoints.append(dict(kind='LEASED_IP', sequence_number=seq_no))
                        manifest2_expose.append({
                            'Port': expose['port'],
                            'ExternalPort': expose.get('as') or 0,
                            'Proto': expose.get('proto','').upper() or 'TCP',
                            'Service': to.get('service', ''), # akashjs hardcodes this to ''
                            'Global': to.get('global') or False,
                            'Hosts': expose.get('accept',{}).get('items') or None,
                            'HTTPOptions': util.CamelCase(http_options),
                            'IP': '',
                            'EndpointSequenceNumber': 0,
                        })

                        kind = 'RANDOM_PORT'
                        if expose.get('proto','tcp').lower() == 'tcp' and to.get('global') and (expose.get('as') or expose.get('port')) == 80:
                            kind = 'SHARED_HTTP'
                        endpoints.append(dict(kind=kind))
                resources['resources']['endpoints'] = endpoints
                resmanif['endpoints'] = None

                group['resources'].append(resources)

                manifest3_service = {
                    'Name': svc_name,
                    'Image': svc['image'],
                    'Command': svc.get('command') or None,
                    'Args': svc.get('args') or None,
                    'Env': svc.get('env') or None,
                    'Resources': resmanif,
                    'Count': svcdepl['count'],
                    'Expose': manifest2_expose,
#                    'Params': None,
                }
                [
                    placement
                    for placement in manifest3
                    if placement['Name'] == placement_name
                ][0]['Services'].append(manifest3_service)

        names = list(groups.keys())
        names.sort()
        groups = [GroupSpec(**groups[name]) for name in names]
        manifest_txt = json.dumps(manifest3, sort_keys=True, separators=[',',':'])
        for repl_in, repl_out in [
            ('<','\\u003c'),
            ('>','\\u003e'),
            ('&','\\u0026'),
        ]:
            manifest_txt = manifest_txt.replace(repl_in, repl_out)
        manifest_version = hashlib.sha256(manifest_txt.encode()).digest()
        return groups, manifest3, manifest_version

_cereal_re = re.compile('^([0-9]*)([^0-9]*)$')
_cereal_muls = dict(
    m = fractions.Fraction(1)/1000,
    k = 1000,    Ki = 1024,    M = 1000**2, Mi = 1024**2,
    G = 1000**3, Gi = 1024**3, T = 1000**4, Ti = 1024**4,
    P = 1000**5, Pi = 1024**5, E = 1000**6, Ei = 1024**6,
)
def parse_cereal(kibbles_and_bits):
    if type(kibbles_and_bits) is str:
        num, suffix = _cereal_re.match(kibbles_and_bits).groups()
        return int(fractions.Fraction(num) * _cereal_muls[suffix])
    else:
        return int(kibbles_and_bits)

class AsyncShellTransport(asyncio.transports.Transport):
    STDOUT = 100
    STDERR = 101
    RESULT = 102
    FAILURE = 103
    STDIN = 104
    TERMINAL_RESIZE = 105
    def __init__(self, websocket):
        self._ws = websocket
        self._stderr = asyncio.streams.StreamReader()
        self._stderr.set_transport(self)
        self._stdout = asyncio.streams.StreamReader()
        self._stdout.set_transport(self)
        self._reading_task = None
        self._stdin = asyncio.streams.StreamWriter(self, None, None, self._stdout._loop)
        self._write_processor = util.AsyncQeueueProcessor(process = self._do_write)
        self.resume_reading()
        self._closing_task = None

    def write(self, data, code = STDIN):
        last = self._write_processor.last(default=[None])
        if last[0] == self.STDIN:
        #if self._write_processor.queue.qsize() and code == self.STDIN and self._write_queue._queue[-1][0] == self.STDIN:
            last.extend(data)
            #self._write_queue._queue[-1].extend(data)
        else:
            self._write_processor.enqueue_nowait(bytearray([code]) + data)
            #self._write_queue.put_nowait(bytearray([code]) + data)
        #if self._writing_task is None or (self._writing_task.done() and self._writing_task.result()):
        #    self._writing_task = asyncio.create_task(self._do_writing())

    async def _do_write(self, queue):
    #async def _do_writing(self):
        ##print('opening write')
        #while self._write_queue.qsize():
            #data = await self._write_queue.get()
            data = await queue.get()
            #print('sending', data)
            await self._ws.send_bytes(data)
            self._write_queue.task_done()
        ##print('write closed')
        #return True

    def terminal_resize(self, width, height):
        # kubernetes format
        self.write(struct.pack('>HH', width, height), self.TERMINAL_RESIZE)

    #def can_write_eof(self):
    #def write_eof(self):

    def close(self):
        self._closing_task = asyncio.create_task(self._ws.close())

    def is_closing(self):
        return self._closing_task is not None

    def resume_reading(self):
        self._stdout._paused = False
        self._stderr._paused = False
        if self._reading_task is None or self._reading_task.done():
            assert self._reading_task is None or self._reading_task.result()
            self._reading_task = asyncio.create_task(self._do_reading())
    async def _do_reading(self):
        #print('opening read')
        while not self._stdout._paused and not self._stderr._paused:
            wsmsg = await self._ws.receive()
            if wsmsg.type in (aiohttp.WSMsgType.CLOSING, aiohttp.WSMsgType.CLOSED):
                if self._closing_task is None:
                    self.close()
                self._stdout.feed_eof()
                self._stderr.feed_eof()
                break
            #elif wsmsg.type != aiohttp.WSMsgType.BINARY:
            #    import pdb; pdb.set_trace()
            assert wsmsg.type == aiohttp.WSMsgType.BINARY
            data = wsmsg.data
            #print('received', data)
            code, msg = data[0], data[1:]
            if code == self.STDOUT:
                self._stdout.feed_data(msg)
            elif code == self.STDERR:
                self._stderr.feed_data(msg)
            elif code == self.RESULT:
                result = json.loads(msg)
                #print(result)
                self.result = result
                result_message = result.get('Message')
                if result_message:
                    exc = Exception('lease shell failed', result_message, result['ExitCode'])
                    exc.result = result
                    self._stdout.set_exception(exc)
                    self._stderr.set_exception(exc)
                else:
                    self._stdout.feed_eof()
                    self._stderr.feed_eof()
            elif code == self.FAILURE:
                exc = Exception('unknown provider error', msg)
                self._stdout.set_exception(exc)
                self._stderr.set_exception(exc)
            else:
                raise NotImplementedError('read code', code)
        #print('closing read')
        return True

    def pause_reading(self):
        self._stdout._paused = True
        self._stderr._paused = True

    def get_extra_info(self, name, default=None):
        return self._ws.get_extra_info(name, default)

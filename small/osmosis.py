#import cosmoschains

import asyncio, bisect, contextlib, datetime

#import grpc
import tqdm

from .client import Client
from .util import acache, cache, acache_day, cache_day

class Osmosis(Client):
    def __init__(self, api, protobuf='osmosis16_protobuf', async_only=False, **kwparams):
        assert protobuf != 'cosmospy_protobuf'
        super().__init__(api, protobuf,
            async_only = async_only,
            **kwparams,
        )
        self.denoms_by_name = {}
        self.denoms_by_code = {}
        self.pools_by_code = {}
        self.pools_by_id = {}
        self.block_timestamp_heights = []
        if not async_only:
            for pool in tqdm.tqdm(
                    self.pools(),
                    total=self.osmosis_gamm.NumPools().num_pools,
                    desc='Pool tokens',
                    unit='pl',
                    leave=False,
            ):
                pool = self.Pool(self, pool)
                assert self.pools_by_id.setdefault(pool.id, pool) is pool
                for code in pool.codes:
                    self.pools_by_code.setdefault(code, set()).add(pool)
                    if code not in self.denoms_by_code:
                        name = self.parse_denom(code)
                        self.denoms_by_code[code] = name
                        self.denoms_by_name.setdefault(name, set()).add(code)
    async def __ainit__(self):
        if await super().__ainit__():
            for pool in await self._apools():
                pool = self.Pool(self, pool)
                assert self.pools_by_id.setdefault(pool.id, pool) is pool
                for code in pool.codes:
                    self.pools_by_code.setdefault(code, set()).add(pool)
                    if code not in self.denoms_by_code:
                        name = await self.aparse_denom(code)
                        self.denoms_by_code[code] = name
                        self.denoms_by_name.setdefault(name, set()).add(code)
            return True
    @cache_day
    def _pools(self):
        return list(self.osmosis_gamm.Pools())
    @acache_day
    async def _apools(self):
        return [pool async for pool in self.osmosis_gamm.aPools()]
    class Pool:
        def __init__(self, client, proto):
            self.id = proto.id
            self.proto = proto
            if hasattr(self.proto, 'pool_assets'):
                self.codes = [asset.token.denom for asset in self.proto.pool_assets]
            elif hasattr(self.proto, 'poolAssets'):
                self.codes = [asset.token.denom for asset in self.proto.poolAssets]
            elif hasattr(self.proto, 'pool_liquidity'):
                self.codes = [liquidity.denom for liquidity in self.proto.pool_liquidity]
            else:
                self.codes = [liquidity.denom for liquidity in self.proto.poolLiquidity]
            self.codes = set(self.codes)
        def others(self, denomcode):
            others = [code for code in self.codes if code != denomcode]
            assert len(others) == len(self.codes) - 1
            return others
        def __hash__(self):
            return self.id
        def __key__(self):
            return self.id
        def __repr__(self):
            return f'{self.id}:{"/".join(self.codes)}'
    def iter_routes(self, src, dst, shallow=True, max_depth=2):
        pools = self.pools_by_code
        visited = set()
        queue = []
        for initial_pool in pools[src]:
            initial_path = (initial_pool,)
            visited.add(hash(initial_path))
            queue.append((initial_path, src))
        shallow_length = None
        oldct = 0
        with tqdm.tqdm(desc='iter_routes',leave=False,total=len(queue)) as pbar:
          while queue:
            meander, last_code = queue.pop()
            oldct += 1
            pbar.total = oldct + len(queue)
            pbar.update()
            if dst in meander[-1].others(last_code):#codes:
                if shallow_length is None:
                    shallow_length = len(meander)
                if shallow_length >= len(meander):
                    yield meander
                continue
            if shallow and shallow_length and len(meander) >= shallow_length:
                continue
            if max_depth and len(meander) >= max_depth:
                continue
            for next_code in meander[-1].codes:
                if next_code == last_code:
                    continue
                for next_pool in pools[next_code]:
                    if next_pool in meander:
                        continue
                    next_meander = (*meander, next_pool)
                    next_hash = hash(next_meander)
                    if next_hash in visited:
                        continue
                    visited.add(next_hash)
                    bisect.insort(queue, (next_meander, next_code), key=lambda pathcode: -len(pathcode[0]))
    @acache
    async def estimate_in(self, amount, denom_in, denom_out, *pools, height):
        #import pdb; pdb.set_trace()
        token_in = str(amount) + str(denom_in)

        routes = []
        last_denom = denom_in
        for pool, next_pool in zip(pools[:-1], pools[1:]):
            shared_denoms = pool.codes & next_pool.codes
            shared_denoms.discard(last_denom)
            #assert len(shared_denoms) == 1
            if len(shared_denoms) > 1:
                import warnings
                warnings.warn('more than one hop option between pools, add token identities to route iteration')
            for next_denom in shared_denoms:
                routes.append(dict(pool_id = pool.id, token_out_denom = next_denom))
                last_denom = next_denom
                break
        routes.append(dict(pool_id = pools[-1].id, token_out_denom = denom_out))
        #import pdb; pdb.set_trace()

        try:
            return int((await self.osmosis_poolmanager.aEstimateSwapExactAmountIn(
                token_in = token_in,
                routes = routes,
                metadata = height and {self.X_COSMOS_BLOCK_HEIGHT:str(height)},
            )))
        except self.Errors as error:
            status, message = self.error_status_message(error)
            print(type(error), status, message)
            if status == self.Statuses.INTERNAL and (
                'cannot input more than pool reserves' in message # amount too large
                or 'token amount must be positive' in message # amount too small
                or 'failed to find route for pool id' in message # ?? maybe unidirectional pools exist? happened routing from 0CD3A (dai-wei) to 92BE07 (uist) via 1052
                or 'base must be greater than 0' in message # ?? guessing this is rare, maybe amount too small
            ):
                return 0
            #if status == self.Statuses.ABORTED and 'Protocol error' in message:
            #    import pdb; pdb.set_trace()
            #    return 0
            raise
    @acache
    async def estimate_out(self, sender, amount, denom_in, denom_out, *pools, height=None):
        token_out = str(amount) + str(denom_out)

        routes = [dict(pool_id = pools[0].id, token_in_denom = denom_in)]
        for pool, next_pool in zip(pools[:-1], pools[1:]):
            shared_denoms = pool.codes & next_pool.codes
            for next_denom in shared_denoms:
                routes.append(dict(pool_id = pool.id, token_in_denom = next_denom))

        try:
            return int((await self.osmosis_poolmanager.aEstimateSwapExactAmountOut(
                sender = sender,
                routes = routes,
                token_out = token_out,
                metadata = height and {self.X_COSMOS_BLOCK_HEIGHT:str(height)},
            )))
        except Exception as error:
            raise
        #except grpc.RpcError as error:
        #    if error.code() == grpc.StatusCode.INTERNAL:# and 'cannot input more than pool reserves' in error.details():
        #        return float('inf')
        #    raise
    async def route_in(self, amount_in, denom_in, denom_out, height=None, routes=None):
        denom_in = self.encode_denom(denom_in)
        denom_out = self.encode_denom(denom_out)
        if routes is None:
            assert self.pools_by_code
            routes = list(self.iter_routes(denom_in, denom_out))
        if len(routes) == 0:
            return [], 0
        #amounts_out = await asyncio.gather(*[self.estimate_in(amount_in, denom_in, denom_out, *route, height = height) for route in routes])
        amounts_out = await tqdm.asyncio.tqdm.gather(*[self.estimate_in(amount_in, denom_in, denom_out, *route, height = height) for route in routes], desc=f'{await self.aparse_denom(denom_in)}->{await self.aparse_denom(denom_out)} routes', leave=False)
        #amounts_out = [await self.estimate_in(amount_in, denom_in, denom_out, *route, height = height) for route in tqdm.tqdm(routes, desc=f'{await self.aparse_denom(denom_in)}->{await self.aparse_denom(denom_out)} routes', leave=False)]
        return max(zip(routes, amounts_out), key=lambda tuple: tuple[1])
    async def route_out(self, sender, amount_out, denom_in, denom_out, height=None, routes=None):
        denom_in = self.encode_denom(denom_in)
        denom_out = self.encode_denom(denom_out)
        if routes is None:
            assert self.pools_by_code
            routes = list(self.iter_routes(denom_in, denom_out))
        if len(routes) == 0:
            return [], float('inf')
        amounts_in = await asyncio.gather(*[self.estimate_out(sender, amount_out, denom_in, denom_out, *route, height = height) for route in routes])
        return min(zip(routes, amounts_in), key=lambda tuple: tuple[1])
    #@acache
    async def auto_estimate_in(self, amount_in, denom_in, denom_out, height):
        route, amount_out = await self.route_in(amount_in, denom_in, denom_out, height=height)
        return amount_out
    @acache
    async def auto_estimate_out(self, sender, amount_out, denom_in, denom_out, height):
        route, amount_in = await self.route_out(sender, amount_out, denom_in, denom_out, height=height)
        return amount_in
    @contextlib.contextmanager
    def at_height(self, height):
        try:
            old_height = self.metadata.get(self.X_COSMOS_BLOCK_HEIGHT)
            self.metadata[self.X_COSMOS_BLOCK_HEIGHT] = str(height)
            yield self
        finally:
            if old_height is not None:
                self.metadata[self.X_COSMOS_BLOCK_HEIGHT] = old_height
            else:
                del self.metadata[self.X_COSMOS_BLOCK_HEIGHT]
    async def block_datetime(self, height):
        @acache
        async def block_datetime(self, height):
            block = await self.cosmos_base_tendermint.aGetBlockByHeight(height=int(height))
            return block.block.header.time.ToDatetime()
        if height is None:
            block = await self.cosmos_base_tendermint.aGetLatestBlock()
            result = block.block.header.time.ToDatetime()
            height = block.block.header.height
        else:
            result = await block_datetime(self, height)
        new_timestamp_height = (result.timestamp(), int(height))
        new_idx = bisect.bisect_left(self.block_timestamp_heights, new_timestamp_height)
        if new_idx == len(self.block_timestamp_heights) or self.block_timestamp_heights[new_idx] != new_timestamp_height:
            self.block_timestamp_heights.insert(new_idx, new_timestamp_height)
        return result
    @cache
    def parse_denom(self, code):
        # used in constructor, so sync
        if code.startswith('ibc/'):
            denom = self.ibc_applications_transfer.DenomTrace(hash=code).base_denom
        else:
            denom = code
        return denom
    @acache
    async def aparse_denom(self, code):
        # used in constructor, so sync
        if code.startswith('ibc/'):
            denom = (await self.ibc_applications_transfer.aDenomTrace(hash=code)).base_denom
        else:
            denom = code
        return denom
    def encode_denom(self, name):
        codes = self.denoms_by_name.get(name, [name])
        assert len(codes) == 1
        return next(iter(codes))

    @staticmethod
    def _block2timestampheight(block):
        return (block.block.header.time.ToDatetime().timestamp(), int(block.block.header.height))
    async def rough_height_for_time(self, datetime, modulo=1000):
        time = datetime.timestamp()
        if len(self.block_timestamp_heights) < 2:
            block = await self.cosmos_base_tendermint.aGetLatestBlock()
            bisect.insort(self.block_timestamp_heights, self._block2timestampheight(block))
        if len(self.block_timestamp_heights) < 2:
            block = await self.cosmos_base_tendermint.aGetBlockByHeight(height = self.block_timestamp_heights[0][1] - 16)
            bisect.insort(self.block_timestamp_heights, self._block2timestampheight(block))
        idx = min(
            bisect.bisect(self.block_timestamp_heights, (time, None)),
            len(self.block_timestamp_heights) - 2
        )
        (time1, height1), (time2, height2) = self.block_timestamp_heights[idx:idx+2]
        height = int((time - time1) / (time2 - time1) * (height2 - height1) + height1 + 0.5)
        height = height // modulo * modulo
        return await self.block_datetime(height), height
            
    #def rough_estimate_in(self, time, amount, denom_in, denom_out):
    #    height = self.rough_height_for_time(time)
aOsmosis = Osmosis.acreate

async def adefault():
    from . import cosmoschains, util
    chains = cosmoschains.FileRegistry()
    chain = chains.chain('osmosis-1', Client=Osmosis)
    url = await chain.aurl(Client=Osmosis)
    client = await aOsmosis(url)
    assert client.node_info.application_version.name == 'osmosis'
    release = util.ver(client.node_info.application_version.version).major
    protobuf = f'osmosis{release}_protobuf'
    client = await aOsmosis(url, protobuf=protobuf)
    assert client.denoms_by_code
    return client

import logging
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)
osmosis = None
client = None

async def prices_for(name_in, osmosis=osmosis, tip_block=None):
    if tip_block is None:
        tip_block = await client.cosmos_base_tendermint.aGetLatestBlock()
    height = tip_block.block.header.height
    modulo = 100000
    height = (height//modulo*modulo)
    result = {}
    while height:
        price = await price_for_at(name_in, osmosis, height)
        if not price:
            break
        result[height] = price
        height -= modulo
    return result
        

wei_scale = 1000000000000000000
m_scale = 1000
u_scale = 1000000
sat_scale = 100000000
nano_scale = 1000000000
def guess_name_scale_from_name(name):
    if name.endswith('-wei'):
        return name[:-4], wei_scale
    if name.endswith('-satoshi'):
        return name[:-8], sat_scale
    #if name.startswith('gamm/pool/'):
    #    return name, wei_scale
    if '-' in name:
        pre, post = name.split('-',1)
        postscale = guess_name_scale_from_name(post)
        if postscale is not None:
            post, scale = postscale
            return f'{pre}-{post}', scale
    if name == 'basecro':
        return name[4:], u_scale * 100
    if name == 'uusd':
        return name[1:], u_scale * 100
    if name.startswith('nano'):
        return name[4:], nano_scale
    if name.startswith('milli'):
        return name[5:], m_scale
    if name[0] == 'n':
        return name[1:], nano_scale
    if name[0] in 'ue':
        return name[1:], u_scale
    if name[0] == 'a':
        return name[1:], wei_scale
    
async def price_for_at(name_in, osmosis, height, amount_in = None, denom_name_scale_outs = [
    (name, guess_name_scale_from_name(name)[1]) for name in [
        'uusd',
        'uusdc',
        'dai-wei',
        'uusdt',
        'busd-wei',
        'avalanche-uusdc',
        'polygon-uusdc',
    ]
]):
    import fractions
    async def w2(code_in, code_out, name_out, scale_out):
        price = fractions.Fraction(await osmosis.auto_estimate_in(int(amount_in), code_in, code_out, height=height)) / scale_out
        return code_in, code_out, name_out, price
    if amount_in is None:
        amount_in = guess_name_scale_from_name(name_in)
        amount_in = amount_in[1] if amount_in else wei_scale
        amount_in *= 100
    codes_in = osmosis.denoms_by_name.get(name_in, [name_in])
    prices = await asyncio.gather(*[w2(code_in, osmosis.encode_denom(name), name, scale) for code_in in codes_in for name, scale in denom_name_scale_outs])
    price = max(prices, key=lambda tup: tup[-1])[-1] / amount_in
    logger.info(f'{name_in}@{height}: ${float(price)}')
    return price

#async def prices_at(osmosis, height=None):
#    import fractions
#    if height is None:
#        height = osmosis.cosmos_base_tendermint.GetLatestBlock().block.header.height
#    #osmosis = await osmosis.acreate(api=osmosis._api_url, x_cosmos_block_height=height)
#    nano_scale = 1000000000
#    #denom_out = osmosis.encode_denom('dai-wei'); denom_out_scale = wei_scale
#    denom_name_scale_outs = {
#        (osmosis.encode_denom('uusd'), 'uusd', u_scale * 100),
#        (osmosis.encode_denom('uusdc'), 'uusdc', u_scale),
#        (osmosis.encode_denom('dai-wei'), 'dai-wei', wei_scale),
#        (osmosis.encode_denom('uusdt'), 'uusdt', u_scale),
#        (osmosis.encode_denom('busd-wei'), 'busd-wei', wei_scale),
#        (osmosis.encode_denom('avalanche-uusdc'), 'avalanche-uusdc', u_scale),
#        (osmosis.encode_denom('polygon-uusdc'), 'polygon-uusdc', u_scale),
#    }
#    #denom_out = osmosis.encode_denom('uusdc'); denom_out_scale = u_scale
#    #amount_out = value_usd * denom_out_scale
#    amount_in = 10 ** 9
#    result = {}
#    async def w2(code_in, code_out, name_out, scale_out):
#        return code_out, name_out, fractions.Fraction(await osmosis.auto_estimate_in(int(amount_in), code_in, code_out, height=height)) / scale_out
#    async def w(code_in, name_in):
#        #if name_in in ('ungm', 'utick'):
#        #    import pdb; pdb.set_trace()
#        prices = [       
#            (code_in, name_in, code_out, name_out, (await w2(code_in, code_out, name_out, scale_out))[-1])
#            for code_out, name_out, scale_out in denom_name_scale_outs
#        ]
#        #import pdb; pdb.set_trace()
#        return max(prices, key = lambda tuple: tuple[-1])
#        #return code, name, await osmosis.auto_estimate_in(int(amount_in), code_in, code_out, height=height)#, routes=routes)
#    #import pdb; pdb.set_trace()
#    for coro in ([#asyncio.as_completed([
#            w(code, name)
#            for code, name
#            in osmosis.denoms_by_code.items()
#            if code.startswith('ibc/')
#    ]):
#        #import pdb; pdb.set_trace()
#        code_in, name_in, code_out, name_out, amount_out = await coro
#        if not amount_out:
#            continue
#        #amount_out /= denom_out_scale
#        #price = fractions.Fraction(amount_in) / (fractions.Fraction(amount_out) / denom_out_scale)
#        #price = fractions.Fraction(amount_out) / denom_out_scale / amount_in
#        price = amount_out / amount_in
#        result[f'{name_in}-{code_in}'] = price
#        if name_in.endswith('-wei'):
#            name_in = f'{name_in[:-4]}({name_in})'
#            price *= wei_scale
#        elif name_in == 'basecro':#.startswith('base'):
#            name_in = f'{name_in[4:]}({name_in})'
#            price *= u_scale * 100
#        elif name_in.startswith('nano'):
#            name_in = f'{name_in[4:]}({name_in})'
#            price *= nano_scale
#        elif name_in[0] in 'n':
#            name_in = f'{name_in[1:]}({name_in})'
#            price *= nano_scale
#        elif name_in[0] in 'ue':
#            name_in = f'{name_in[1:]}({name_in})'
#            price *= u_scale
#        print(f'{name_in} ${float(price)} ({name_out})')
#    return result
    

#print(apiname, apiuri)
#print(list(osmosis.denoms_by_name.keys()))
#route, amount_out = osmosis.route_in(int(amount_in), denom_in, denom_out)
#print(amount_in / 1000000, 'flix -> $', amount_out / 1000000000000000000)
tip_block = None
height = None
time = None
routes = None
async def amain():
    #import pdb; pdb.set_trace()
    global osmosis, client
    osmosis = await adefault()
    client = osmosis
    global tip_block, height, time, routes
    from decimal import Decimal
    amount_in = Decimal('1661.219') * 1000000
    denom_in = osmosis.encode_denom('uflix')
    denom_out = osmosis.encode_denom('dai-wei')
    tip_block = await client.cosmos_base_tendermint.aGetLatestBlock()
    height = tip_block.block.header.height
    #height = (height//1000000*1000000)
    modulo = 100000
    height = (height//modulo*modulo)
    block = await client.cosmos_base_tendermint.aGetBlockByHeight(height)
    time = block.block.header.time.ToDatetime()
    #time = tip_block.block.header.time.ToDatetime()
    routes = list(osmosis.iter_routes(denom_in, denom_out))
    for iter in range(8):
        #prices = await prices_at(osmosis, height)
        prices = {
            name: await price_for_at(name, osmosis, height)
            for name in osmosis.denoms_by_name
        }
        #route, amount_out = await osmosis.route_in(int(amount_in), denom_in, denom_out, height=height, routes=routes)
        blockdate = time.isoformat()
        for denom, price in prices.items():
            #print(height, blockdate, amount_in / 1000000, 'flix -> $', amount_out / 1000000000000000000)
            if denom.startswith('u'):
                denom = denom[1:]
                price *= 1000000
            print(height, blockdate, denom, '=>', float(price), '$')
        #time -= datetime.timedelta(hours=1)
        #time, height = await client.rough_height_for_time(time,modulo=modulo)
        height -= modulo
        block = await client.cosmos_base_tendermint.aGetBlockByHeight(height)
        time = block.block.header.time.ToDatetime()
if __name__ == '__main__':
    asyncio.run(amain())
    

#for height in range(tip_height, tip_height - 1*2, -1):
    #amount_out = osmosis.estimate_in(int(amount_in), denom_in, denom_out, *route, height=height)
    #amount_out = osmosis.auto_estimate_in(int(amount_in), denom_in, denom_out, height=height)

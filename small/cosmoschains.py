import requests.exceptions as requests_exceptions
import os, random, time, warnings
from functools import lru_cache; cache = lru_cache(None)
import json
import tqdm, tqdm.asyncio
import logging
logger = logging.getLogger(__name__)

from . import util
from .util import requests
from . import client
_client = client

json_patches = {
    'https://polkachu.com/api/v1/chains': [
        (
            '.github == "https://github.com/nymtech/nym"',
            [{
                'op':'replace', 'path':'/github',
                'value':'https://github.com/nymtech/nyxd/'
            }]
        )
    ]
}
def patched_json(func):
    def patched_json(*params, **kwparams):
        json = func(*params, **kwparams)
        if params[0] in json_patches:
            json = util.patch_json(json, json_patches[params[0]])
        return json
    return patched_json
get_day_text = util.get_day_text
get_day_json = patched_json(util.get_day_json)

class Polkachu:
    def __init__(self, api_url = 'https://polkachu.com/api/v1', grpcs_url = 'https://polkachu.com/public_grpc'):
        self.api_url = api_url
        self.grpcs_url = grpcs_url
    #@util.cache_day
    def chainjsons(self, chain_id):
        chain = self._chains.get(chain_id)
        if chain is None:
            return {}
        grpc = self._grpcs.get(chain['network'])
        live_peers = chain['live_peers']
        if live_peers['active']:
            live_peers.update(get_day_json(live_peers['endpoint']))
        chainjson = dict(
            chain_name = chain['network'],
            chain_id = chain['chain_id'],
            pretty_name = chain['name'],
            network_type = 'mainnet',
            status = 'live',
            codebase = dict(
                git_repo = chain['github'],
            ),
            daemon_name = chain['binary'],
            node_home = f'$HOME/{chain["folder"]}',
            genesis_url = chain['addrbook']['url'].replace('addrbook', 'genesis') if chain['addrbook']['active'] else None,
            logo_uris = {
                chain['logo'].rsplit('.',1)[-1]: chain['logo']
            },
            staking = dict(
                staking_tokens = [ dict(
                    denom = chain['denom'],
                    # chain['symbol']
                ) ],
            ),
            peers = dict(
                seeds = (
                    [ _idaddr(chain['seed_node']['url'], 'Polkachu') ]
                    if chain['seed_node']['active']
                    else []
                ) + (
                    [
                        _idaddr(peer)
                        for peer in live_peers['live_peers']
                    ]
                    if live_peers['active']
                    else []
                ),
                persistent_peers = []
            ),
            apis = dict(
                grpc = (
                    [ dict(
                    address = grpc,
                    provider = 'Polkachu',
                    ) ]
                    if grpc is not None
                    else []
                ),
                rpc = (
                    [ dict(
                        address = chain['rpc']['url'],
                        provider = 'Polkachu',
                    ) ]
                    if chain['rpc']['active']
                    else []
                ),
                rest = (
                    [ dict(
                        address = chain['api']['url'],
                        provider = 'Polkachu',
                    ) ]
                    if chain['api']['active']
                    else []
                ),
            ),
        )
        return dict(assets = None, chain = chainjson)
    @util.cache_day
    def chain(self, chain_id, Client=client.Client, **kwparams):
        reqs = self.chainjsons(chain_id)
        return Chain(reqs['chain'], reqs['assets'], Client=Client, **kwparams)
    @util.cached_property
    def _grpcs(self):
        return {
            line.split('-grpc.polkachu.com',1)[0]: line
            for line in get_day_text(self.grpcs_url).split('\n')
            if '-grpc.polkachu.com' in line
        }
    @property
    #@util.cache_day
    def _chains(self):
        return {
            chain['chain_id'] : chain
            for chain
            in get_day_json(f'{self.api_url}/chains')
        }
    def chain_ids(self, network_type='mainnet'):
        if network_type == 'mainnet':
            chains = list(self._chains.keys())
            random.shuffle(chains)
            return chains
        else:
            return []
    def __key__(self):
        return str(type(self)), self.api_url

class WebRegistry:
    def __init__(self, url = 'https://raw.githubusercontent.com/cosmos/chain-registry/master'):
        self.url = url
        self._by_id = {}
    @staticmethod
    def _get_json(url):
        return get_day_json(url)
    @util.cache
    def chainjsons(self, chain_id):
        chain = self._by_id.get(chain_id, chain_id)
        reqs = dict(
            assets = f'{self.url}/{chain}/assetlist.json',
            chain = f'{self.url}/{chain}/chain.json',
        )
        keys = list(reqs)
        random.shuffle(keys)
        for key in keys:
            try:
                reqs[key] = self._get_json(reqs[key])
            except:
                reqs[key] = None
        if not any(reqs.values()) and not self._by_id:
            assert not self._by_id
            self.chain_ids()
            assert self._by_id
            return self.chainjsons(chain_id)
        return reqs
    @util.cache_day
    def chain(self, chain_id, Client=client.Client, **kwparams):
        reqs = self.chainjsons(chain_id)
        return Chain(reqs['chain'], reqs['assets'], Client=client.Client, **kwparams)
    def chain_ids(self, network_type='mainnet'):
        chains = self.chains(network_type)
        for chain in tqdm.tqdm(chains):
            chain_id = self.chainjsons(chain)['chain']['chain_id']
            self._by_id[chain_id] = chain
        return list(self._by_id.keys())
    def __key__(self):
        return str(type(self)), self.url

class FileRegistry(WebRegistry):
    PROTO = 'file://'
    def __init__(
        self,
        path = os.path.join(
            os.path.dirname(__file__),
            '..',
            'chain-registry'
        ),
    ):
        path = os.path.abspath(path)
        super().__init__(self.PROTO + path)
    @classmethod
    def _get_json(cls, url):
        assert url.startswith(cls.PROTO)
        with open(url[len(cls.PROTO):]) as fh:
            return json.load(fh)
    @property
    def path(self):
        assert self.url.startswith(self.PROTO)
        return self.url[len(self.PROTO):]
    def chains(self, network_type='mainnet'):
        if network_type != 'mainnet':
            subdir = f'{network_type}s/'
        else:
            subdir = ''
        chains = [
            subdir + entry
            for entry in os.listdir(os.path.join(self.path, subdir))
            if
                os.path.isdir(os.path.join(self.path, subdir, entry)) and
                not entry.startswith('_') and
                not entry.startswith('.') and
                entry != 'testnets'
        ]
        random.shuffle(chains)
        return chains

class PingPub(WebRegistry):
    def __init__(self, api_url = 'https://registry.ping.pub'):
        super().__init__(api_url)
    @util.cache
    def chains(self, network_type='mainnet'):
        if network_type != 'mainnet':
            subdir = f'{network_type}s/'
        else:
            subdir = ''
        chains = get_day_json(f'{self.url}/{subdir}')
        chains = [
            subdir + entry['name']
            for entry in chains
            if
                entry['type'] == 'directory' and
                not entry['name'].startswith('_') and
                not entry['name'].startswith('.') and
                entry['name'] != 'testnets'
        ]
        random.shuffle(chains)
        return chains

class Chain:
    def __init__(self, chain, assets=None, Client=client.Client, **kwparams):
        self.chainjson = chain
        self.assetjson = assets

        self.status = self.chainjson['status']
        self.network = self.chainjson['network_type']
        self.name = self.chainjson['pretty_name']
        self.chain_id = self.chainjson['chain_id']
        codebase = self.chainjson['codebase']
        if 'git_repo' in codebase:
            if 'recommended_version' in codebase:
                self.gitrepo = f'{codebase["git_repo"]}@{codebase["recommended_version"]}'
            else:
                self.gitrepo = codebase["git_repo"]
        else:
            self.gitrepo = None
        if 'daemon_name' in self.chainjson:
            self.daemon_name = self.chainjson['daemon_name']
        elif 'node_home' in self.chainjson:
            self.daemon_name = self.chainjson['node_home'].split('.',1)[1]
        elif 'git_repo' in codebase:
            self.daemon_name = codebase['git_repo'].rsplit('/',1)[-1]
        else:
            self.daemon_name = None
        self.node_home = self.chainjson.get('node_home') or f'${{HOME}}/.{self.daemon_name}'
        if 'genesis' in codebase:
            self.genesis_url = codebase['genesis']['genesis_url']
        else:
            self.genesis_url = None
        self.peers = {
            peer.get('provider', peer['address']): (peerlistname[:-1], peer['id'], peer['address'])
            for peerlistname, peerlist in self.chainjson.get('peers',{}).items()
            for peer in peerlist
        }

        #assert self.status != 'live' or 'apis' in self.chainjson
        self.apis = {}
        for apitype, apis in self.chainjson.get('apis',{}).items():
            for api in apis:
                addr = api['address']
                if not '://' in addr:
                    host = addr.split('/',1)[0]
                    if not ':' in host:
                        proto = ['http','https']
                    else:
                        host, port = host.split(':')
                        port = int(port)
                        if port == 443:
                            proto = 'https'
                        else:
                            proto = ['http', 'https']
                            #import ssl, socket
                            ##import certifi
                            ##ctx = ssl.create_default_context(cafile=certifi.where())
                            #ctx = ssl.create_default_context()
                            #ctx.check_hostname = False
                            #ctx.verify_mode = ssl.CERT_NONE
                            #try:
                            #    with socket.create_connection((host, port), timeout=1) as sock:
                            #        with ctx.wrap_socket(sock, server_hostname=host) as ssock:
                            #            proto = 'https'
                            #except:# ssl.SSLError:
                            #    proto = 'http'
                    if type(proto) is list:
                        protos = proto
                        addr_apiurls = [(f'{proto}://{host}',f'{apitype}+{proto}://{host}') for proto in protos]
                    else:
                        addr_apiurls = [(f'{proto}://{host}',f'{apitype}+{proto}://{host}')]
                else:
                    addr_apiurls = [(addr,f'{apitype}+{addr}')]
                for addr, apiurl in addr_apiurls:
                    proto, host = addr.split('://', 1)
                    if 'provider' in api:
                        apiname = f'{api["provider"]};{apitype}+{proto}'
                    else:
                        apiname = apiurl
                    self.apis[apiname] = apiurl
        self.explorers = {}
        for explorer in self.chainjson.get('explorers', ()):
            kind = explorer.get('kind')
            explorers = self.explorers.setdefault(kind, {})
            explorers[explorer['url']] = {
                name[:-len('_page')]: templ.replace('$','')
                for name, templ in explorer.items()
                if name.endswith('_page')
            }

        #from cosmpy2 import LedgerClient
        if self.assetjson is None:
            #from cosmpy.aerial.config import NetworkConfigError
            from cosmpy.protos.cosmos.bank.v1beta1.query_pb2 import QueryDenomsMetadataRequest
            from google.protobuf.json_format import MessageToJson
            import json
            import grpc
            import urllib3.util.timeout
            default_timeout = urllib3.util.timeout._Default
            urllib3.util.timeout._Default = 1
            for api in self.apis.values():
                try:
                    client_ = Client(api=api, **kwparams)
                    assets_json = client_.cosmos_bank.DenomsMetadata()
                    #client = LedgerClient(self.config((None,None),api))
                    #assets_json = client.get_assets_json()
                #except NetworkConfigError:
                #    continue
                except AssertionError:
                    continue
                except grpc.RpcError:
                    continue
                except requests_exceptions.ConnectionError:
                    continue
                if self.assetjson is None:
                    self.assetjson = {
                        '$schema': self.chainjson['$schema'].replace('chain', 'assetlist'),
                        'chain_name': self.chainjson['chain_name'],
                        'assets': assets_json,
                    }
                break
                #assert self.assetjson is None or self.assetjson == assets_json
                #self.assetjson = assets_json
            urllib3.util.timeout._Default = default_timeout
        self.denoms = {}
        if self.assetjson is not None:
            #self.denoms = LedgerClient.parse_denoms(self.assetjson['assets'], self.chain_id)
            self.denoms = parse_denoms(self.assetjson['assets'], self.chain_id)
        if 'fees' in self.chainjson:
            self.fee_denoms = [
                token['denom']
                for token in self.chainjson['fees']['fee_tokens']
            ]
        else:
            self.fee_denoms = list(self.denoms.keys()) or (None,)
        if 'staking' in self.chainjson:
            self.staking_denoms = [
                token['denom']
                for token in self.chainjson['staking']['staking_tokens']
            ]
        else:
            self.staking_denoms = list(self.fee_denoms)
    def variants(self, staking=True, fee=False):
        if fee and not staking:
            for fdenom in self.fee_denoms:
                if fdenom in self.staking_denoms:
                    yield (fdenom, fdenom)
                else:
                    yield (fdenom, self.staking_denoms[0])
        else:
            if staking:
                sdenoms = self.staking_denoms
            else:
                sdenoms = (self.staking_denoms[0],)
            for sdenom in sdenoms:
                if fee:
                    fdenoms = self.fee_denoms
                elif sdenom in self.fee_denoms:
                    fdenoms = (sdenom,)
                else:
                    fdenoms = (self.fee_denoms[0],)
                for fdenom in fdenoms:
                    yield (fdenom, sdenom)
    def urls(self, type=None):
        urls = [url for url in self.apis.values() if type is None or url.startswith(type)]
        return urls
    def url(self, Client=client.Client, **kwparams):
        return self.client(None, None, Client=Client, **kwparams)._api_url
    async def aurl(self, Client=client.Client, **kwparams):
        return (await self.aclient(None, None, Client=Client, **kwparams))._api_url
    def is_eth(self):
        key_algos = self.chainjson.setdefault('key_algos', ['secp256k1'])
        assert key_algos in (
            ['secp256k1'],
            ['ethsecp256k1'],
        )
        if key_algos == ['secp256k1']:
            return False
        elif key_algos == ['ethsecp256k1']:
            return True
    def baseline_protobuf(self):
        return 'evmos_protobuf' if self.is_eth() else 'cosmospy_protobuf'
    def client(self, url=None, height=None, Client=client.Client, **kwparams):
        if url is None:
            verspeedurls = []
            urls_ = self.urls('grpc')
            exc = None
            with tqdm.tqdm(urls_, leave=False, unit='url') as urls:
                for url in urls:
                    #urls.desc = url
                    try:
                         speed, ver = Client.rank_api(url, **kwparams)
                    except Exception as e:
                        logger.info(f'{url} {type(e)} {e}')
                        if type(exc) not in util.BUILTIN_EXCEPTIONS:
                            exc = e
                        else:
                            raise e
                        continue
                    try:
                        ver = util.ver(ver)#nodeinfo.application_version.version)
                    except:
                        ver = util.ver('0.0.0')
                    verspeedurls.append((ver, speed, url))
            if not verspeedurls:
                if urls_:
                    warnings.warn('All results were exceptional. Wiping _cache folder will retry all.', stacklevel=2)
                    raise exc
                return None
            url = max(verspeedurls, key = lambda tup: tup[:-1])[-1]
        return client.Client(api=url, **kwparams, x_cosmos_block_height=height)
    async def aclient(self, url=None, height=None, Client=_client.Client, banned_urls=(), urls=None, **kwparams):
        if url is None:
            verspeedurls = []
            urls_ = urls or self.urls('grpc')
            exc = None
            async def arank_url(url):
                try:
                    return url, await Client.arank_api(api=url, **kwparams)
                except Exception as e:
                    return url, (-100, e)
            #import pdb; pdb.set_trace()
            for fut in tqdm.asyncio.tqdm.as_completed(
                [ arank_url(url) for url in urls_ if url not in banned_urls ],
                leave=False,
                unit='url',
            ):
                    url, (speed, ver) = await fut
                    if isinstance(ver, Exception):
                        if type(ver) not in util.BUILTIN_EXCEPTIONS:
                            exc = ver
                        else:
                            raise ver
                        logger.info(f'{url} {type(ver)} {ver}')
                        continue
                    if ver is None:
                        continue
                    try:
                        ver = util.ver(ver)#nodeinfo.application_version.version)
                    except:
                        ver = util.ver('0.0.0')
                    verspeedurls.append((ver, speed, url))
            if not verspeedurls:
                if exc is not None:
                    warnings.warn('All results were exceptional. Wipe _cache folder.', stacklevel=2)
                    raise exc
                return None
            url = max(verspeedurls, key = lambda tup: tup[:-1])[-1]
        return await Client.acreate(api=url, **kwparams, x_cosmos_block_height=height)
    def __key__(self):
        urls = self.urls()
        urls.sort()
        return urls

class MultiResource:
    def __init__(self, *apis):
        self.apis = apis
    @classmethod
    def all(cls):
        #return cls(Polkachu(), WebRegistry(), PingPub())
        if util.have_curl_cffi:
            return cls(Polkachu(), FileRegistry(), PingPub())
        else:
            return cls(Polkachu(), FileRegistry())
    def chain_ids(self, network_type = 'mainnet'):
        chain_ids = set()
        for api in self.apis:
            try:
                new_chain_ids = api.chain_ids(network_type)
            except AttributeError:
                continue
            #for chain_id in set(new_chain_ids) - chain_ids:
            #    yield chain_id
            #    chain_ids.add(chain_id)
            chain_ids.update(new_chain_ids)
        return chain_ids
    def chainjsons(self, chain):
        result = dict(chains={},assets=None)
        for api in self.apis:
            try:
                chainjsons_func = api.chainjsons
            except AttributeError:
                continue
            result = self._merge(result, chainjsons_func(chain))
        return result
    def chain(self, chain, protobuf='cosmospy_protobuf'):
        reqs = self.chainjsons(chain)
        return Chain(reqs['chain'], reqs['assets'], protobuf=protobuf)
    @classmethod
    def _merge(cls, combined, added, adjctx=None):
        if combined is added or combined == added or added is None:
            return combined
        elif combined is None:
            return added
        assert type(combined) is type(added)
        if type(combined) is dict:
            for key in set(combined.keys()) | set(added.keys()):
                if key in combined:
                    if key in added:
                        combined[key] = cls._merge(combined[key], added[key], adjctx=key)
                else:
                    combined[key] = added[key]
        elif type(combined) is list:
            #minlen = min(len(combined), len(added))
            #if minlen > 1 and combined[:minlen-1] == added[:minlen-1]:
            #    if len(added) > len(combined):
            #        combined[minlen:] = added[minlen:]
            #    return combined
            concatenated = combined + added
            if not len(concatenated):
                return concatenated
            if type(concatenated[0]) is dict:
                keys = [key for key in ('id','address','denom','name','url') if key in concatenated[0]]
                if keys == ['id','address']:
                    keys = ['id']
                assert len(keys) == 1
                key = keys[0]
                merged_dict = {}
                for item in concatenated:
                    itemkey = item[key]
                    cls._merge(merged_dict.setdefault(itemkey,item), item, adjctx=adjctx)
                combined.clear()
                combined.extend(merged_dict.values())
            else:
                combined.extend(set(added).difference(combined))
        else:
            if type(combined) is str:
                if combined.startswith(added):
                    return added
                elif added.startswith(combined):
                    return combined
                if combined.startswith('http') and added.startswith('http'):
                    if util.get_day_url(combined).rstrip('/') == added.rstrip('/'):
                        return added
                    elif util.get_day_url(added).rstrip('/') == combined.rstrip('/'):
                        return combined
                combined_l = combined.lower()
                added_l = added.lower()
                if combined_l.startswith(added_l) or added_l.startswith(combined_l):
                    if combined == combined.title():
                        return combined
                    if added == added.title():
                        return added
                    if combined_l.startswith('http'):
                        return combined if combined < added else added
                if adjctx in ('chain_name', 'pretty_name'):
                    return combined if len(combined) > len(added) else added
                if adjctx in ('provider',):
                    return f'{combined} / {added}'
                if adjctx in ('recommended_version','cosmos_sdk_version','linux/amd64','linux/arm64','darwin/amd64','darwin/arm64','windows/amd64'):
                    return max(combined, added)
                if adjctx in ('address',):
                    import socket
                    if ':' not in combined:
                        combined, added = added, combined
                    combined_host, combined_port = combined.rsplit(':',1)
                    if ':' not in added:
                        added_host = added
                        added_port = combined_port
                    else:
                        added_host, added_port = added.rsplit(':',1)
                    try:
                        with socket.create_connect(combined_host, combined_port):
                            combined_connect = True
                    except:
                        combined_connect = False
                    try:
                        with socket.create_connect(added_host, added_port):
                            added_connect = True
                    except:
                        added_connect = False
                    if combined_connect != added_connect:
                        return combined if combined_connect else added
                    if any([chr.isalpha for chr in combined]):
                        return combined
                    elif any([chr.isalpha for chr in added]):
                        return added
            raise RuntimeError(combined, added, "how to combine")
        return combined

def _idaddr(peer, provider = None):
    id, addr = peer.split('@',1)
    if provider is not None:
        return dict(
            id=id,
            address=addr,
            provider=provider,
        )
    else:
        return dict(
            id=id,
            address=addr,
        )

def parse_denoms(assets_json, prefix):#=''):
    denoms = {}
    for asset in assets_json:
        denom_digits = {}
        for unit in asset.get('denomUnits', asset.get('denom_units')):
            for denom in [unit['denom']] + unit.get('aliases',[]):
                digits = unit.get('exponent',0)
                if denom in denom_digits:
                    import warnings
                    warnings.warn(f'{prefix}: multiple units for {denom}, assuming the largest is {denom[1:]}', stacklevel=3)
                    denom_digits[denom] = min(denom_digits[denom], digits)
                    digits = max(denom_digits[denom], digits)
                    denom = denom[1:]
                denom_digits[denom] = digits
        for display_denom in (
            asset['display'],
            asset.get('symbol','').lower(),
            prefix.split('-',1)[0].split('net',1)[0]+asset['display'],
            prefix.split('-',1)[0].split('net',1)[0]+asset['symbol'].lower(),
        ):
            if display_denom in denom_digits:
                display_digits = denom_digits[display_denom]
                display = asset.get('symbol',display_denom.upper())
                break
        for name, digits in denom_digits.items():
            denoms[name] = (display_digits - digits, display)
    return denoms

async def amain():
    pp = MultiResource.all()
    for network_type in ('mainnet', 'testnet'):
        for chain_id in pp.chain_ids(network_type):
            chain = pp.chain(chain_id)
            print(chain.name, chain.apis)
            await chain.aclient()

if __name__ == '__main__':
    import asyncio
    asyncio.run(amain())

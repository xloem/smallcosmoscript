import asyncio, contextlib, datetime, fractions, functools, hashlib, importlib, operator, os, pkgutil, random, re, semver, sys, threading, time, types
import concurrent.futures

import diskcache, bc_jsonpath_ng.ext, jsonpatch, tqdm
try:
    from curl_cffi import requests
    have_curl_cffi = True
except:
    import requests
    have_curl_cffi = False
import requests.exceptions as requests_exceptions

import numpy as np

BUILTIN_EXCEPTIONS = set([
    builtin
    for builtin in __builtins__.values()
    if type(builtin) is type and issubclass(builtin, Exception)
])

UNSET = object()
UNSET_PAIR = [UNSET,UNSET]

def product(list):
    return functools.reduce(operator.mul, list, 1)

def deterministic_hash(object):
    return deterministic_hash.map[type(object)](object)
deterministic_hash.bytes = lambda bytes: hash(int.from_bytes(hashlib.sha1(bytes).digest(), 'little'))
deterministic_hash.iterable = lambda iterable: hash(tuple([deterministic_hash(item) for item in iterable]))
deterministic_hash.map = {
    bytes: deterministic_hash.bytes,
    str: lambda str: deterministic_hash.bytes(str.encode()),
    int: hash, float: hash,
    list: deterministic_hash.iterable,
    tuple: deterministic_hash.iterable,
    dict: lambda dict: deterministic_hash.iterable(dict.items()),
    object: lambda object: deterministic_hash.iterable([type(object).__name__, object.__dict__])
}

def camelCase(str):
    if type(str) is dict:
        return { camelCase(key): val for key, val in str.items() }
    if len(str):
        strs = str.split('_')
        return strs[0] + ''.join([s.title() for s in strs[1:]])
    else:
        return str
def CamelCase(str):
    if type(str) is dict:
        return { CamelCase(key): val for key, val in str.items() }
    return ''.join([s.title() for s in str.split('_')])
def snake_case(str):
    if type(str) is dict:
        return { snake_case(key): val for key, val in str.items() }
    assert '_' not in str
    return snake_case.re.sub('_', str).lower()
snake_case.re = re.compile(r'(?<!^)(?=[A-Z])')

def ver(str):
    str = str.lstrip('v').split('-',1)[0]
    return semver.VersionInfo.parse(str)

def async_in_thread(func, *params, **kwparams):
    loop = asyncio.get_running_loop()
    def wrapper():
        asyncio.set_event_loop(loop)
        return func(*params, **kwparams)
    fut = loop.run_in_executor(None, wrapper)
    return fut

def async_ignore_uncaught(fut):
    fut.add_done_callback(lambda fut: fut.cancelled() or fut.exception())
    return fut

def regression(x, y, degree=2):
    return np.polynomial.Polynomial.fit(
        x, y,
        min(degree, len(x)),
        full=True,
    )

#class DictQueue:
#    '''It's like Queue but it replaces unpopped values with matching keys, and doesn't attend to order.'''
#    def __init__(self):
#        self.dict = {}
#    async def put(self, key_value):
#        return self.put_nowait(key_value)
#    def put_nowait(self, key_value):
#        key, value = key_value
#        self.dict[key] = value
#    def qsize(self):
#        return len(self.dict)
#    async def get(self):
#        return self.dict.pop(next(iter(self.dict)))
    
class AsyncQueueProcessor:
    def __init__(self, *params, process = None, **kwparams):#Queue = asyncio.Queue, process = None, **kwparams):
        self._queue = asyncio.Queue(*params, **kwparams)#Queue(*params, **kwparams)
        self._process_task = None
        if process is not None:
            self.process = process
        #self._process_future = asyncio.Future()
    async def enqueue_async(self, item):#*params, **kwparams):
        fut = asyncio.Future()
        await self._queue.put([item, fut])#*params, **kwparams)
        self._process()
        return await fut
    def enqueue_sync(self, item):#*params, **kwparams):
        fut = asyncio.Future()
        self._queue.put_nowait([item, fut])#*params, *kwparams)
        self._process()
        return fut
    def last(default=None):
        if self._queue.qsize():
            return self._queue._queue[-1]
        else:
            return default
    def is_processing(self):
        if self._process_task is None:
            return False
        if not self._process_task.done():
            return True
        else:
            self._process_task.result()
            return False
    def _process(self):
        if not self.is_processing():
            if not hasattr(self, '_process_event_loop'):
                self._process_event_loop = asyncio.get_event_loop()
            #self._process_task = asyncio.create_task(self._process_loop())
            self._process_task = asyncio.run_coroutine_threadsafe(self._process_loop(), self._process_event_loop)
        # we could ask process() to return the items updated
        # then we could wait on that
            # i think the functions are different, dict and queue
            # shared concept of check-if-running, run-until-empty ... unsure, maybe too small for amount of boilerplate to use different datastructures with
                # one idea is an augmented queue that lets one update values that are in it, but is otherwise the same
                # i suppose it would need a way to perform the update, which is conditional and uses a private member. so adding to the queue would need to be factored out of performing the process launching. you'd make _process public, and the user would add to the queue.
        #return self._process_future
    async def _process_loop(self):
        while self._queue.qsize():
            item, fut = await self._queue.get()
            try:
                fut.set_result(await self.process(item))
            except Exception as exception:
                fut.set_exception(exception)
        return True
    async def process(self, queue):
        raise NotImplementedError()
    #async def batch_ctx(self):
    #    pass
    async def on_finish(self):
        pass

class AsyncDictProcessor(AsyncQueueProcessor):
    def __init__(self, *params, process = None, merge = None, **kwparams):
        self._dict = dict(*params, **kwparams)
        self._process_task = None
        self.lock_threading = threading.RLock()
        self.lock_asyncio = asyncio.Lock()
        if process is not None:
            self.process = process
        if merge is not None:
            self.merge = merge
        self._process_future = asyncio.Future()
    def __getitem__(self, key):
        with self.lock_threading:
            value, fut, orig_value = self._dict[key]
            return value, fut, orig_value
    async def enqueue_async(self, key, value, orig_value = None):
        async with self.lock_asyncio:
            return await self.enqueue_sync(key, value, orig_value)[0]
    def enqueue_sync(self, key, value, orig_value = None):
        '''this returns both a future and the value being written,
           because the value being written may be changed if there
           is a merge function and pending data.'''
        with self.lock_threading:
            old_entry = self._dict.get(key, UNSET)
            if old_entry is not UNSET:
                #print(threading.current_thread().ident, key, "exists")
                old_value, fut, old_orig_value = old_entry
                #if old_orig_value != orig_value:
                #    import pdb; pdb.set_trace()
                #assert old_orig_value == orig_value
                if old_value != orig_value:
                    print(value['addr'][:5], "state mismatch dict =", hash(repr(old_value)), "updated from", hash(repr(old_orig_value)))
                    print(value['addr'][:5], "expected =", hash(repr(orig_value)), "from __row now updating to", hash(repr(value)))
                    import pdb; pdb.set_trace()
                assert old_value == orig_value
                value = self.merge(old_value, value)
                old_entry[0] = value
                assert self.is_processing()
                return fut, value
            else:
                #print(threading.current_thread().ident, key, "new")
                fut = asyncio.Future()
                self._dict[key] = [value, fut, orig_value]
        self._process()
        return fut, value
    async def _process_loop(self):
        while len(self._dict):
            try:
                # process(dict) figures out what to do with the futures in the pairs
                await self.process(self._dict)
            except Exception as e:
                dict = self._dict
                self._dict = {}
                for value, fut, orig_value in dict.values():
                    if not fut.done():
                        fut.set_exception(e)
        return True
    def merge(self, old_value, new_value):
        return new_value

class AccumulateStreams:
    def __init__(self, *sources, verbose=False, store=True):
        self.store = store
        self.verbose = verbose
        if store:
            self.buffer = bytearray()
        self.astreams = set()
        self.aiters = set()
        for src in sources:
            if type(src) is types.AsyncGeneratorType:
                self.aiters.add(src)
            elif type(src) is asyncio.streams.StreamReader:
                self.astreams.add(src)
            else:
                raise TypeError(type(src))
        self.astreams_task = asyncio.create_task(self._pump_astreams())
        self.aiters_task = asyncio.create_task(self._pump_aiters())
    async def join(self):
        return await asyncio.gather(self.astreams_task, self.aiters_task)
    def read(self):
        assert self.store
        data = self.buffer
        self.buffer = bytearray()
        return data
    def peek(self):
        assert self.store
        return self.buffer
    def close(self):
        if self.task.done():
            self.task.result()
        else:
            self.task.cancel()
    async def _pump_astreams(self):
        while self.astreams:
            found = False
            for reader in list(self.astreams):
                if b'\n' in reader._buffer or reader.at_eof():
                    linebytes = await reader.readline()
                    if b'\n' not in self._add(linebytes):
                        self.astreams.remove(reader)
                    else:
                        found = True
            if self.astreams and not found:
                await next(asyncio.as_completed([
                    reader._waiter if reader._waiter
                    else reader._wait_for_data(self.__class__.__name__)
                    for reader in self.astreams
                    if not reader.at_eof()
                ]))
    async def _pump_aiters(self):
        while self.aiters:
            for aiter in list(self.aiters):
                try:
                    data = await anext(aiter)
                    if type(data) is str:
                        data = data.encode()
                    if data[-1:] != b'\n':
                        data = data + b'\n'
                    self._add(data)
                except StopAsyncIteration:
                    self.aiters.remove(aiter)
    def _add(self, data):
        if data:
            if self.verbose:
                sys.stderr.buffer.write(data)
            if self.store:
                self.buffer += data
        return data

class ConfirmationBlocksHist:
    def __init__(self):
        self.fee_blocks = {}
        self.max_delay = 0
    def add(self, client, denom, broadcast_height, tx_confirmation):
        events = tx_confirmation.tx_response.events
        fee = sum([
            int(fee.amount)
            for event in events
            if event.type == 'tx'
            for feebytes in [{
                attr.key: attr.value
                for attr in event.attributes
            }.get(b'fee')]
            if feebytes is not None
            for fee in [client.Coin(feebytes.decode())]
            if fee.denom == denom
        ])
        blocks = tx_confirmation.tx_response.height - broadcast_height
        return self.add_manual(fee, blocks)
    def add_manual(self, fee, blocks):
        blocks_dict = self.fee_blocks.setdefault(fee, {})
        blocks_dict[blocks] = blocks_dict.get(blocks, 0) + 1
        self.max_delay = max(self.max_delay, blocks * 4)
    def select_fee_delay_for_reward(self, reward_per_amount, validator_amounts, retained_fraction = 0, current_delay = 0):
        total_amount = sum([amount for amount in validator_amounts])
        performance_fee_delay = []
        #amount = total_amount
        amortized_delay_performances = []
        for fee, blocks_dict in self.fee_blocks.items():
            total_count = sum(blocks_dict.values())
            amortized_delay_performance = [
                sum([
                    #event_count * ((1 + (int(reward_per_amount * amount * (blocks+delay)) - fee) / amount) ** (1/(blocks+delay)) - 1)
                    event_count * ((1 + (sum([int(reward_per_amount * amount * (blocks+delay)) for amount in validator_amounts]) - fee) / total_amount) ** (1/(blocks+delay)) - 1)
                    for blocks, event_count in blocks_dict.items()
                ]) / total_count
                for delay in range(current_delay, self.max_delay)
            ]
            if len(amortized_delay_performance):
                best_performing_item = max(range(len(amortized_delay_performance)), key=amortized_delay_performance.__getitem__)
                performance_fee_delay.append((amortized_delay_performance[best_performing_item], fee, best_performing_item))
                amortized_delay_performances.append(amortized_delay_performance)
        if len(performance_fee_delay):
            performance, fee, delay = max(performance_fee_delay)
            if delay > self.max_delay:
                import pdb; pdb.set_trace()
            self.max_delay = max(self.max_delay, delay * 4)
            if fee + 1 not in self.fee_blocks:
                fee += 1 # accumulate data, note this could be checked in advance with a dummy delay=0 pass
            return fee, delay
        else:
            return 0, 0
    #@classmethod
    #def return_from_delay(cls, reward_per_amount, amount, blocks):
    #    return int
    def select_fee_delay_for_reward_new(self, reward_per_amount, validator_amounts, retained_fraction = 0, current_delay = 0):
        total_amount = sum([amount for amount in validator_amounts])
        performance_fee_delay = []
        numerators = []
        exponents = []
        amortized_delay_performances = []
        for fee, blocks_dict in self.fee_blocks.items():
            # if we assume blocks_dict is all that exists, the only delay that will show a return 
            # is the maximal entry, and that's the extent of the recorded data
            total_count = sum(blocks_dict.values())
            #numerator = [
            #        (sum([
            #            int(
            #                int(reward_per_amount * amount * (blocks+delay))
            #                * (1 - retained_fraction)
            #            )
            #            for amount in validator_amounts
            #        ]) - fee)
            #        for blocks, event_count in blocks_dict.items()
            #    ]
            #denominator = total_amount
            #constant = 1
            #exponent = [
            #    (event_count / (blocks+delay))
            #        for blocks, event_count in blocks_dict.items()
            #]
            #numerators.append(numerator)
            #exponents.append(exponent)
            amortized_delay_performance = [
            #    sum([
            #        # i think summing the %s is different from summing the values and the latter should be done
            #        # the * amount can be placed inside the sum
            #        # additionally the amounts could be summed for the histogram buckets and a single ratio produced; this would mean using a fractional return
                product([
            #        (numerator[idx] / denominator + constant) ** exponent[idx]
            #        for idx in range(len(numerator))
                    (max(0,sum([
                        int(
                            int(reward_per_amount * amount * (blocks+delay))
                            * (1 - retained_fraction)
                        )
                        for amount in validator_amounts
                    ]) - fee) / total_amount + 1) ** ((blocks_dict.get(blocks,0)+1) / (blocks+delay))
                    for blocks in range(1,self.max_delay)
                ]) ** (1 / (total_count+self.max_delay-1))
                    #]) - fee) / total_amount + 1) ** (event_count / (blocks+delay))
                    #for blocks, event_count in blocks_dict.items()
                #]) ** (1 / total_count)
            #        event_count * (1 + int(reward_per_amount * amount * (blocks+delay)) / amount - fee) ** (1/blocks)
            #        for blocks, event_count in blocks_dict.items()
            #    ]) / total_count
                for delay in range(current_delay, self.max_delay)
            ]
            if len(amortized_delay_performance):
                best_performing_item = max(range(len(amortized_delay_performance)), key=amortized_delay_performance.__getitem__)
                performance_fee_delay.append((amortized_delay_performance[best_performing_item], fee, best_performing_item))
                amortized_delay_performances.append(amortized_delay_performance)
        if len(performance_fee_delay):
            performance, fee, delay = max(performance_fee_delay)
            if delay * 4 > self.max_delay:
                import pdb; pdb.set_trace()
            self.max_delay = max(self.max_delay, delay * 4)
            if fee + 1 not in self.fee_blocks:
                fee += 1 # accumulate data, note this could be checked in advance with a dummy delay=0 pass
            return fee, delay
        else:
            return 0, 0

def patch_json(json, patches):
    for idx, (path, patch) in enumerate(patches):
        if type(path) is str:
            if type(json) is list:
                path = f'$[?(@{path})]'
            path = bc_jsonpath_ng.ext.parse(path)
            patch = jsonpatch.JsonPatch(patch)
            patches[idx] = (path, patch)
        for match in path.find(json):
            #print('previous:', match.value)
            result = patch.apply(match.value, in_place=True)
            #print('after:', match.value)
    return json

@contextlib.contextmanager
def no_relative_pkgs():
    '''Within context, the import sys.path does not include curdir.'''
    curdir = os.path.abspath(os.path.curdir)
    try:
        if os.path.abspath(sys.path[0]) == curdir:
            rel = sys.path.pop(0)
        else:
            rel = None
        yield
    finally:
        if rel is not None:
            sys.path[:0] = [rel]

# uncomment to debug descriptor loading
#import google.protobuf.descriptor_pool
#DP = type(google.protobuf.descriptor_pool.Default())
#wrapped = DP.AddSerializedFile
#@functools.wraps(wrapped)
#def __AddSerializedFile(pool, content):
#    import pdb; pdb.set_trace()
#    return wrapped(pool, content)
#DP.AddSerializedFile = __AddSerializedFile

import_module = importlib.import_module
def iter_submodules(pkg):
    '''Iterate a (name, module) pair of every subpackage, imported or not.'''
    if isinstance(pkg, str):
        pkg = importlib.import_module(pkg)
    seen = set()
    queue = [(pkg.__name__, pkg.__path__[0], True)]
    while queue:
        module_name, module_path, is_pkg = queue.pop()
        if module_path in seen:
            continue
        seen.add(module_path)
        if is_pkg:
            queue.extend([
                (
                    module_name + '.' + submod_name,
                    os.path.join(loader.path, submod_name),
                    is_subpkg
                )
                for loader, submod_name, is_subpkg
                in pkgutil.iter_modules([module_path])
            ])
        else:
            try:
                module = import_module(module_name)
                yield module_name, module_path, module
                for name, obj in module.__dict__.items():
                    assert not getattr(obj, '__module__', '').startswith('cosmos.')
            except ImportError as e:
                #print(module_path, e)
                pass
    #    return -100, e

#def get_min_commission_validators(client, validator_commissions = None):
#    if validator_commissions is None:
#        validator_commissions = {
#            validator: fractions.Fraction(validator.commission.commission_rates.rate)
#            for validator in client.cosmos_staking.Validators(status='BOND_STATUS_BONDED')
#        }
#    min_commission = min(validator_commissions.values())
#    return (
#        min_commission,
#        {
#            validator.operator_address: fractions.Fraction(validator.delegator_shares) / fractions.Fraction(validator.tokens)
#            for validator, commission
#            in validator_commissions.items()
#            if commission == min_commission
#        }
#    )

@contextlib.asynccontextmanager
async def await_poll(client, unit='chk', dividor=2, **kwparams):
        # shows a progress bar and waits a fraction of the block time whenever its yielded function is called
        # the excess content of this function could be elided away with some reorganization
        # the intent of the excess content is to increase the rate of updates near completion, to stop polling more quickly, while respecting ratelimits
        mint_params = await acache_day(client.cosmos_mint.aParams)()
        block_time = YEAR / mint_params.blocks_per_year
        min_delay = block_time / dividor
        delay = min_delay
        ratelimit_period = 0
        ratelimited_delay = delay
        ratelimited_count = 0
        eta = None
        with tqdm.tqdm(leave=False,unit=unit,**kwparams) as pbar:
            async def aupdate(desc=None,total=None,n=1,n_abs=None):
                nonlocal delay, mark_time, ratelimit_count, remaining_count, ratelimit_period, ratelimit_time, ratelimited_delay, ratelimited_count, eta
                ratelimit_count += 1
                if desc is not None:
                    pbar.desc = desc
                if total is not None:
                    pbar.total = total
                if n_abs is not None:
                    assert n == 1 or n is None
                    pbar.update(n_abs - pbar.n)
                else:
                    pbar.update(n)
                now = time.time()
                #print('aupdate called at', now)
                if ratelimit_period and pbar.total and pbar.n:
                    old_eta = eta
                    eta = (now - pbar.start_t) / pbar.n * pbar.total + pbar.start_t
                    remaining = eta - mark_time
                    if remaining_count is not None or mark_time + delay > eta - ratelimit_period:
                        if remaining_count is None:
                            if remaining > ratelimit_period:
                                remaining_count = int(remaining * ratelimited_count / ratelimit_period)
                            else:
                                remaining_count = ratelimited_count
                        assert remaining_count > 0
                    #ramp_period = eta - ratelimit_period
                    #if remaining <= ratelimit_period and ratelimit_period > delay * 2:
                        # reversetimepoints[remaining_count] = mark_time
                        # reversetimepoints[1] = eta - min_delay
                        # reversetimepoints[0] = eta
                        # y(x) = ax^2 + bx + c
                        remaining_count -= 1
                        #X = remaining_count - 1
                        #A_X = -(eta - mark_time) / remaining_count - min_delay
                        #B_X = -min_delay * remaining_count - (eta - mark_time) / remaining_count
                        #NAXXBX = (eta - mark_time) / remaining_count * (X - 1) + min_delay * (remaining_count - X)
                        #C = eta
                        #next_mark_time = C - NAXXBX
                        if remaining_count < 2:
                            next_mark_time = eta - min_delay
                        else:
                            next_mark_time = eta - remaining * (1 - 2 / remaining_count) - min_delay
                            #assert next_mark_time > mark_time # for example, if min_delay is much bigger than the calculated spacing
                        assert next_mark_time <= eta
                        next_delay = max(0, next_mark_time - mark_time)
                        #print(f'remaining={remaining},mark_time={mark_time},eta={eta},min_delay={min_delay}')
                        #print(f'remaining_count={remaining_count},ratelimited_count={ratelimited_count},ratelimit_period={ratelimit_period}')
                        #print(f'old_delay={delay},next_delay={next_delay},next_mark_time={next_mark_time}')
                        assert next_delay < delay or remaining_count + 1 >= ratelimited_count or ratelimit_time == mark_time or eta > old_eta
                        delay = next_delay
                    #elif mark_time + delay > ramp_period:
                    #    delay = ramp_period - mark_time
                next_mark_time = mark_time + delay
                if now < next_mark_time:
                    await asyncio.sleep(next_mark_time - now)
                    mark_time = next_mark_time
                else:
                    if ratelimit_count > 1:
                        ratelimit_period = now - ratelimit_time
                        ratelimited_delay = ratelimit_period / ratelimit_count
                        ratelimited_count = ratelimit_count
                        #print('ratelimited_count set to', ratelimited_count)
                        delay = ratelimited_delay
                        ratelimit_count = 0
                        ratelimit_time = now
                    else:
                        delay = now - mark_time
                        ratelimited_delay = delay
                    mark_time = now
            mark_time = time.time()
            ratelimit_time = mark_time
            ratelimit_count = 0
            remaining_count = None
            yield aupdate

async def await_for_sync(client):
    if not await client.cosmos_base_tendermint.aGetSyncing():
        return

    async def sync_height_time():
        block = await client.cosmos_base_tendermint.aGetLatestBlock()
        return block.block.header.height, block.block.header.time.ToNanoseconds() / 1000000000

    start_sync_height, start_sync_time = await sync_height_time()
    start_clock_time = datetime.datetime.utcnow().timestamp()
    async with await_poll(client, desc='Syncing', unit='blk') as update:
        while await client.cosmos_base_tendermint.aGetSyncing():
            sync_height, sync_time = await sync_height_time()
            clock_time = datetime.datetime.utcnow().timestamp()

            sync_speed = (sync_time - start_sync_time) / (clock_time - start_clock_time)
            rel_blocks = sync_height - start_sync_height
            if sync_speed > 1:
                total_seconds = (start_clock_time - start_sync_time) / (sync_speed - 1)
                total_blocks = int((sync_height - start_sync_height) / (clock_time - start_clock_time) * total_seconds + 1)
                await update(desc='Syncing', total=total_blocks, n_abs=rel_blocks)
            else:
                await update(desc='Syncing is too slow to complete.',n_abs=rel_blocks, total=rel_blocks*2)
        #sync_speed = (sync_time - start_sync_time) / (clock_time - start_clock_time)
        #sync_speed * total_seconds + start_sync_time = total_seconds + start_clock_time
        #(sync_speed - 1) * total_seconds = start_clock_time - start_sync_time
        #total_seconds = (start_clock_time - start_sync_time) / (sync_speed - 1)

async def aget_validators(client, status_or_delegator='BOND_STATUS_BONDED', metadata=None):
    #if validator_commissions is None:
    BondStatus = client._types_by_url['/cosmos.staking.v1beta1.BondStatus']
    if status_or_delegator is None or status_or_delegator in BondStatus.keys() or status_or_delegator in BondStatus.values():
        validator_req = client.cosmos_staking.aValidators(status=status_or_delegator, metadata=metadata)
    else:
        validator_req = client.cosmos_staking.aDelegatorValidators(delegator_addr=status_or_delegator, metadata=metadata)
    return {
        validator.operator_address:
        (
            fractions.Fraction(validator.commission.commission_rates.rate),
            fractions.Fraction(validator.delegator_shares),
            fractions.Fraction(validator.tokens),
        )
        async for validator in validator_req
    }

async def aget_min_validators(client, validator_commission_shares_tokens = None):
    if validator_commission_shares_tokens is None:
        validator_commission_shares_tokens = await aget_validators(client)
    min_commission, min_tokenshares = min([(commission, shares / tokens) for commission, shares, tokens in validator_commission_shares_tokens.values()])
    return (
        min_commission,
        min_tokenshares,
        [
            validator
            for validator, (commission, shares, tokens)
            in validator_commission_shares_tokens.items()
            if commission == min_commission
            and shares / tokens == min_tokenshares
        ]
    )

def get_bpr(client):#, blocks_per_year = None):
    bond_denom = client.cosmos_staking.Params().bond_denom
    #bond_supply = client.cosmos_bank.SupplyOf(bond_denom).amount
    try:
        annual_provisions = fractions.Fraction(client.cosmos_mint.AnnualProvisions().annual_provisions.decode()) / client._dec_scale
    except:
        import warnings
        warnings.warn(f'{client._api_url} failed to get annual_provisions', stacklevel=2)
        return 0
    #if blocks_per_year is None:
    mint_params = client.cosmos_mint.Params()
    mint_denom = mint_params.mint_denom
    #assert bond_denom == mint_params.mint_denom
    blocks_per_year = mint_params.blocks_per_year
    staking_pool = client.cosmos_staking.Pool()
    bonded_tokens = fractions.Fraction(staking_pool.bonded_tokens)
    block_provision = annual_provisions / blocks_per_year
    if mint_denom != bond_denom:
        warnings.warn(f'chain has bond token of {bond_denom} but mint token of {mint_denom}',stacklevel=2)
    else:
        assert block_provision < bonded_tokens # maybe using display denom?
    return block_provision / bonded_tokens

async def aget_bpr(client):#, blocks_per_year = None):
    bond_denom = (await client.cosmos_staking.aParams()).bond_denom
    #bond_supply = client.cosmos_bank.SupplyOf(bond_denom).amount
    try:
        annual_provisions = fractions.Fraction((await client.cosmos_mint.aAnnualProvisions()).annual_provisions.decode()) / client._dec_scale
    except:
        import warnings
        warnings.warn(f'{client._api_url} failed to get annual_provisions', stacklevel=2)
        return 0
    #if blocks_per_year is None:
    mint_params = await client.cosmos_mint.aParams()
    mint_denom = mint_params.mint_denom
    #assert bond_denom == mint_params.mint_denom
    blocks_per_year = mint_params.blocks_per_year
    staking_pool = await client.cosmos_staking.aPool()
    bonded_tokens = fractions.Fraction(staking_pool.bonded_tokens)
    block_provision = annual_provisions / blocks_per_year
    if mint_denom != bond_denom:
        warnings.warn(f'chain has bond token of {bond_denom} but mint token of {mint_denom}',stacklevel=2)
    else:
        assert block_provision < bonded_tokens # maybe using display denom?
    return block_provision / bonded_tokens

async def aget_reward_shares(client, delegator_address, validator_address=None, metadata=None):
    if validator_address is not None:
        delegation_rewards = await client.cosmos_distribution.aDelegationRewards(
                delegator_address=delegator_address,
                validator_address=validator_address,
                metadata=metadata,
            )
        return {
            reward.denom: fractions.Fraction(reward.amount)
            for reward in delegation_rewards
        }
    else:
        validator_delegation_rewards = await client.cosmos_distribution.aDelegationTotalRewards(
                delegator_address=delegator_address,
                metadata=metadata,
            )
        return {
            delegation_rewards.validator_address: {
                reward.denom: fractions.Fraction(reward.amount)
                for reward in delegation_rewards.reward
            }
            for delegation_rewards in validator_delegation_rewards.rewards
        }

async def aget_amounts_rewards_unbondings(client, delegator_address, denom=None, validators=None, metadata=None):
    positions = {}
    redelegations = [redelegation async for redelegation in client.cosmos_staking.aRedelegations(delegator_address, metadata=metadata)]
    delegations = {delegation.delegation.validator_address:delegation async for delegation in client.cosmos_staking.aDelegatorDelegations(delegator_address, metadata=metadata)}
    unbondings = {
        unbonding.validator_address: sum([fractions.Fraction(entry.balance) for entry in unbonding.entries])
        async for unbonding in client.cosmos_staking.aDelegatorUnbondingDelegations(delegator_address, metadata=metadata)
    }
    reward_shares = await aget_reward_shares(client, delegator_address, metadata=metadata)
    if validators is None:
        validators = await aget_validators(client, metadata=metadata)
    all_validators = set(delegations.keys()) | set(unbondings.keys())
    missing_validators = all_validators - validators.keys()
    assert not missing_validators
    tokenshares = {validator:validators[validator][1]/validators[validator][2] for validator in set(delegations.keys()) | set(unbondings.keys())}

    # sanity check that shares and tokens are converted correctly, might need an update if bond_denom != mint_denom
    for validator in tokenshares:
        if validator in delegations:
            shares = fractions.Fraction(delegations[validator].delegation.shares)
            #bdenom = delegations[validator].balance.denom
            tokens = fractions.Fraction(delegations[validator].balance.amount)
            assert int(shares / tokenshares[validator]) == tokens

    if denom is None:
        return {
            validator: (
                fractions.Fraction(delegations[validator].delegation.shares) / tokenshares[validator]
                    if validator in delegations else 0,
                {
                    denom: fractions.Fraction(amount) / tokenshares[validator]
                    for denom, amount in reward_shares[validator].items()
                }
                    if validator in reward_shares else {},
                unbondings.get(validator, 0),
            ) for validator in validators
                if validator in delegations or validator in reward_shares or validator in unbondings
        }
    else:
        return {
            delegations[validator].delegation.validator_address: (
                fractions.Fraction(delegations[validator].delegation.shares) / tokenshares[validator] if validator in delegations else 0,
                fractions.Fraction(reward_shares[validator].get(denom,0)) / tokenshares[validator] if validator in reward_shares else 0,
                unbondings.get(validator, 0),
            ) for validator in validators
                if validator in delegations or validator in reward_shares or validator in unbondings
        }

def exception(Type = Exception, Exclude = None, value = None, handler = None):
    if handler is None:
        handler = lambda exception: value
    else:
        assert value is None
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*params, **kwparams):
            try:
                return func(*params, **kwparams)
            except Type as exception:
                if isinstance(exception, Exclude):
                    raise
                return handler(exception)
        return wrapper
    return decorator

def aexception(Type = Exception, Exclude = None, value = None, handler = None):
    if handler is None:
        handler = lambda exception: value
    else:
        assert value is None
    def decorator(func):
        @functools.wraps(func)
        async def wrapper(*params, **kwparams):
            try:
                return await func(*params, **kwparams)
            except Type as exception:
                if isinstance(exception, Exclude):
                    raise
                return handler(exception)
        return wrapper
    return decorator

def diskcache_cache__key__(base, typed, ignore):
    '''Generates a diskcache-compatible cache_key function
       that calls out to __key__ member functions if present
       on objects.'''
    @functools.wraps(diskcache.core.args_to_key)
    def cache_key(*params, **kwparams):
        params = [
            getattr(param, '__clskey__' if type(param) is type else '__key__', lambda: param)()
            for idx, param in enumerate(params)
        ]
        kwparams = {
            key: getattr(val, '__clskey__' if type(val) is type else '__key__', lambda: val)()
            for key, val in kwparams.items()
        }
        return diskcache.core.args_to_key(base, params, kwparams, typed, ignore)
    return cache_key

@functools.wraps(diskcache.Cache.memoize)
def diskcache_memoize(
    cache, name=None, typed=False, expire=None, tag=None, ignore=(), async_=False
):
    '''Async-compatible diskcache memoize using diskcache_cache__key__.'''
    if callable(name):
        raise TypeError('name cannot be callable')
    def decorator(func):
        funcname = diskcache.core.full_name(func) if name is None else name
        if hasattr(func, '__key__'):
            base = (funcname, func.__key__())
        else:
            base = (funcname,)
        def get_key_result(*params, **kwparams):
            key = wrapper.__cache_key__(*params, **kwparams)
            result = cache.get(key, default=diskcache.core.ENOVAL, retry=True)
            return key, result
        def get(*params, **kwparams):
            key, result = get_key_result(*params, **kwparams)
            if result is diskcache.core.ENOVAL:
                raise KeyError(params, kwparams)
            return result
        def has(*params, **kwparams):
            key, result = get_key_result(*params, **kwparams)
            return result is not diskcache.core.ENOVAL
        if async_:
            @functools.wraps(func)
            async def wrapper(*params, **kwparams):
                key, result = get_key_result(*params, **kwparams)
                if result is diskcache.core.ENOVAL or (
                    type(result) in (tuple,list) and any([
                        type(item) in BUILTIN_EXCEPTIONS
                        #or isinstance(item, Exception) # this could be enabled with a flag or environment variable
                        for item in result
                    ])
                ) or type(result) in BUILTIN_EXCEPTIONS:
                    result = await func(*params, **kwparams)
                    cache.set(key, result, expire, tag=tag, retry=True)
                return result
        else:
            @functools.wraps(func)
            def wrapper(*params, **kwparams):
                key, result = get_key_result(*params, **kwparams)
                if result is diskcache.core.ENOVAL or (
                    type(result) in (tuple,list) and any([
                        type(item) in BUILTIN_EXCEPTIONS
                        for item in result
                    ])
                ) or type(result) in BUILTIN_EXCEPTIONS:
                    result = func(*params, **kwparams)
                    cache.set(key, result, expire, tag=tag, retry=True)
                return result
        wrapper.__cache_key__ = diskcache_cache__key__(base, typed, ignore)
        wrapper.get = get
        wrapper.has = has
        return wrapper
    return decorator
diskcache.Cache.memoize = diskcache_memoize
CACHE = diskcache.Cache('_cache')

'''decorators to memoize or cache function results'''
memoize = CACHE.memoize
expiry_range_memoize = lambda min, max, *params, **kwparams: memoize(*params, expire=(random.random()*(max-min)+min), **kwparams)
acache = memoize(async_=True)
cache = memoize()
cached_property = lambda func: property(cache(func))
expiry_cache = lambda expire: memoize(expire=expire)
expiry_acache = lambda expire: memoize(expire=expire, async_=True)
expiry_range_cache = lambda min, max: expiry_range_memoize(min=min, max=max)
expiry_range_acache = lambda min, max: expiry_range_memoize(min=min, max=max, async_=True)

'''times in seconds, for expire or other values'''
SECOND=1
MINUTE=60
HOUR=60*MINUTE
DAY=24*HOUR
WEEK=7*DAY
YEAR=36525*DAY//100
MONTH=YEAR//12

cache_minute = expiry_cache(MINUTE)
acache_minute = expiry_acache(MINUTE)
cache_hour = expiry_cache(HOUR)
acache_hour = expiry_acache(HOUR)
cache_day = expiry_cache(DAY)
acache_day = expiry_acache(DAY)
cache_week = expiry_cache(WEEK)
acache_week = expiry_acache(WEEK)

def get_text(*params, **kwparams):
    '''Hands off to requests.get and returns .text,
    for memoization and convenience.'''
    return requests.get(*params, **kwparams).text

def get_json(*params, **kwparams):
    '''Hands off to requests.get and returns .json,
    for memoization and convenience.'''
    response = requests.get(*params, **kwparams)
    response.raise_for_status() # won't be json if it's a failure
    return response.json()

def post_json(*params, **kwparams):
    '''Hands off to requests.post and returns .json,
    for memoization and convenience.'''
    response = requests.post(*params, **kwparams)
    response.raise_for_status() # won't be json if it's a failure
    return response.json()

def get_url(*params, **kwparams):
    '''Hands off to requests and returns the final url only.'''
    response = requests.get(*params, headers={'Range':'bytes=0-0'}, **kwparams)
    return response.url

get_day_text = cache_day(get_text)
get_day_json = cache_day(get_json)
post_day_json = cache_day(post_json)
get_day_url = cache_day(get_url)

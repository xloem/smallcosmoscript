import asyncio, contextlib, hashlib, os

import tqdm

from .tx import Transaction
from . import util

#import bech32, bip_utils, ecdsa, ecdsa.curves, ecdsa.util, Crypto.Hash
#
#class ECDSAPrivateKey:
#    def __init__(self, seed_phrase, curve=ecdsa.SECP256k1, digest=hashlib.sha256):
#        if seed_phrase:
#            if os.path.exists(seed_phrase):
#                with open(seed_phrase) as fh:
#                    seed_phrase = fh.read().strip()
#                seed_bytes = bip_utils.Bip39SeedGenerator(seed_phrase).Generate()
#            bip44_def_ctx = bip_utils.Bip44.FromSeed(
#                seed_bytes, bip_utils.Bip44Coins.COSMOS
#            ).DeriveDefaultPath()
#            data = bip44_def_ctx.PrivateKey().Raw().ToBytes()
#            self.key = ecdsa.SigningKey.from_string(data, curve=curve, hashfunc=digest)
#        else:
#            self.key = ecdsa.SigningKey.generate(curve=curve, hashfunc=digest)
#    @property
#    def public(self):
#        return self.key.get_verifying_key()
#
#    def sign(self, msg_or_digest, digest = False, deterministic = True, canonical = True):
#        if digest:
#            sign_func = [
#                self.key.sign_digest,
#                self.key.sign_digest_deterministic,
#            ][deterministic]
#        else:
#            sign_func = [
#                self.key.sign,
#                self.key.sign_deterministic,
#            ][deterministic]
#        return sign_func(msg_or_digest, sigencode=[
#                ecdsa.util.sigencode_string,
#                ecdsa.util.sigencode_string_canonize,
#            ][canonical])
#
#PrivateKey = ECDSAPrivateKey

class Wallet:
    def __init__(self, chain, seed_phrase = 'cosmos.wallet', client = None, broadcast_tx_bytes = []):
        self.chain = chain
        #self.key = PrivateKey(seed_phrase)
        if seed_phrase is not None and len(seed_phrase.strip().split(' ')) < 12:
            with open(seed_phrase) as fh:
                seed_phrase = fh.read().strip()
        self.seed_phrase = seed_phrase
        self.client = client
        if self.client is not None:
            assert self.client.chain_id == chain.chain_id
        self._broadcast_txs = broadcast_tx_bytes
    async def __ainit__(self, **kwparams):
        protobuf = self.chain.baseline_protobuf()
        if self.client is None:
            self.client = await self.chain.aclient(protobuf=protobuf, **kwparams)
        await util.await_for_sync(self.client)
        await asyncio.gather(*[
            Transaction(self).abroadcast(tx_bytes=broadcast_tx, wait=True)
            for broadcast_tx in self._broadcast_txs
        ])
        del self._broadcast_txs
        self.account = await self.client.cosmos_auth.aAccount(self._bootstrap_address)
        if hasattr(self.account, 'base_account'):
            self.account = self.account.base_account
    async def update(self):
        self.account = await self.client.cosmos_auth.aAccount(self.address)
        return self.account
    @property
    def _bootstrap_address(self):
        return self.mospy().address
    @property
    def address(self):
        return self.account.address
    #@property
    #def address(self):
    #    assert self.chain.chainjson['key_algos'] = ['secp256k1']
    #    prefix = self.chain.chainjson['bech32_prefix']
    #    addr_bytes = Crypto.Hash.RIPEMD160.RIPEMD160Hash(
    #            hashlib.sha256(
    #                self.key.public.to_string('compressed')
    #            ).digest()
    #        ).digest()
    #    addr_base5 = bech32.convertbits(addr_bytes, 8, 5, True)
    #    assert addr_base5 is not None
    #    return bech32.bech32_encode(prefix, addr_base5)
    #    display = to_bech32(self._prefix, address)

    @property
    def default_fee_denom(self):
        fee_denoms = self.chain.chainjson['fees']['fee_tokens']
        fee_denoms.sort(key = lambda denom: (denom['fixed_min_gas_price'], denom.get('low_gas_price')))
        return fee_denoms[0]['denom']

    @contextlib.asynccontextmanager
    async def atx(self, gas_price = 0, fee_denom = None, concurrent = False, desc = None):
        if fee_denom is None or gas_price is None:
            fee_denoms = self.chain.chainjson['fees']['fee_tokens']
            if fee_denom is None:
                fee_denoms.sort(key = lambda denom: (denom['fixed_min_gas_price'], denom.get('low_gas_price')))
                fee_denom = fee_denoms[0]['denom']
            if gas_price is None:
                gas_price = [denom.get('fixed_min_gas_price') or denom.get('low_gas_price', 0) for denom in fee_denoms if denom['denom'] == fee_denom][0]
        tx = Transaction(self)
        yield tx
        if len(tx):
            self.last = await tx.asignbroadcast(gas_price, fee_denom, concurrent=concurrent, desc=desc)
        else:
            self.last = None

    def mospy(self):
        import mospy
        prefix = self.chain.chainjson['bech32_prefix']
        slip44 = self.chain.chainjson['slip44']
        # this could check the contents of the protobuf rather than chainjson
        eth = self.chain.is_eth()
        if hasattr(self, 'account'):
            return mospy.Account(
                seed_phrase = self.seed_phrase,
                #private_key = self.key.key.to_string(),
                account_number = self.account.account_number,
                next_sequence = self.account.sequence,
                hrp = prefix,
                slip44 = slip44,
                # address_index = 0,
                protobuf = self.client.protobuf,
                eth = eth,
            )
        else:
            return mospy.Account(
                seed_phrase = self.seed_phrase,
                #private_key = self.key.key.to_string(),
                #account_number = self.account.account_number,
                #next_sequence = self.account.sequence,
                hrp = prefix,
                slip44 = slip44,
                # address_index = 0,
                protobuf = self.client.protobuf,
                eth = eth,
            )

if __name__ == '__main__':
    import asyncio
    from . import cosmoschains
    async def amain():
        chain = cosmoschains.FileRegistry().chain('cosmoshub-4')
        wallet = Wallet(chain)
        await wallet.__ainit__()
        async with wallet.atx() as tx:
            import pdb; pdb.set_trace()
            for msgname, msgcall in tx._msgs.items():
                print(msgname)
    asyncio.run(amain())

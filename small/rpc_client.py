#from .util import post_json
#from itertools import count as atomic_count

import asyncio, functools, json, os
import openapi3, yaml
from . import util

DEFAULT_SPEC_FN = os.path.join(
    os.path.dirname(__file__),
    # the node info can be queried for the tendermint version and cloned via git
    # this file is then rpc/openapi/openapi.yaml in resulting clone
    'cometbft_v0_34_27_openapi.yaml'
    #'cometbft_v0_35_9_openapi.yaml'
)
with open(DEFAULT_SPEC_FN) as fh:
    DEFAULT_SPEC = yaml.safe_load(fh.read())
DEFAULT_API = openapi3.OpenAPI(DEFAULT_SPEC)

class RPCClient:
    def __init__(self, url, spec=None):
        self._api = DEFAULT_API if spec is None else openapi3.OpenAPI(spec)
        if '+' in url:
            proto, url = url.split('+',1)
            assert proto == 'rpc'
        self._base_url = url
        for id, op in self._api._operation_map.items():
            setattr(self, id, self._rpc_wrapper(id, op, async_=False))
            setattr(self, 'a'+id, self._rpc_wrapper(id, op, async_=True))
    def _rpc_wrapper(self, id, op, async_=False):
        request = _FlexibleRequest(op)
        paged = any([param.name == 'per_page' for param in op.parameters])
        callable = openapi3.openapi.OperationCallable(request, self._base_url, self._api._security, self._api._ssl_verify, self._api._session)
        if not async_:
            if not paged:
                @functools.wraps(callable)
                def operation(*params, **kwparams):
                    parameters = self._parse_params(op, params, kwparams)
                    response = callable(parameters=parameters)
                    return self._parse_response(response)
            else:
                @functools.wraps(callable)
                def operation(*params, page=1, per_page=100, **kwparams):#024, **kwparams):
                    kwparams['page'] = page
                    kwparams['per_page'] = per_page
                    total = None
                    key = None
                    while True:
                        parameters = self._parse_params(op, params, kwparams)
                        print(kwparams)
                        response = callable(parameters=parameters)
                        parsed = self._parse_response(response)
                        assert len(parsed) == 2
                        total = parsed.pop('total_count')
                        if key is None:
                            key = list(parsed)[0]
                        val = parsed[key]
                        yield from val
                        if len(val) + kwparams['page'] * kwparams['per_page'] >= int(total):
                            break
                        else:
                            assert len(val) == kwparams['per_page']
                            kwparams['page'] += 1
        else:
            if not paged:
                @functools.wraps(callable)
                async def operation(*params, **kwparams):
                    params = self._parse_params(op, params, kwparams)
                    response = await asyncio.to_thread(callable,parameters=params)
                    return self._parse_response(response)
            else:
                @functools.wraps(callable)
                async def operation(*params, page=1, per_page=100, **kwparams):
                    kwparams['page'] = page
                    kwparams['per_page'] = per_page
                    total = None
                    key = None
                    while True:
                        parameters = self._parse_params(op, params, kwparams)
                        response = await asyncio.to_thread(callable,parameters=parameters)
                        parsed = self._parse_response(response)
                        assert len(parsed) == 2
                        total = parsed.pop('total_count')
                        if key is None:
                            key = list(parsed)[0]
                        val = parsed[key]
                        for item in val:
                            yield item
                        if len(val) + kwparams['page'] * kwparams['per_page'] >= int(total):
                            break
                        else:
                            assert len(val) == kwparams['per_page']
                            kwparams['page'] += 1
            operation.__name__ = op.operationId
            operation.__name__ = 'a' + op.operationId
        operation.__doc__ = '\n'.join([str(x) for x in [
            op.summary,
            op.description,
            *[p.raw_element for p in op.parameters],
            *[r.raw_element for c, r in op.responses.items()],
        ]])
        operation._operation = op
        operation._callable = callable
        return operation
    def _parse_params(self, op, params, kwparams):
        offset = 0
        for idx, param in enumerate(params):
            while True:
                name = op.parameters[idx+offset].name
                if name not in kwparams:
                    break
                offset += 1
            kwparams[name] = param
        return {key:json.dumps(val) for key, val in kwparams.items()}
    def _parse_response(self, response):
        if hasattr(response, 'error'):
            raise ValueError(response.error)
        elif 'error' in response:
            raise ValueError(response['error'])
        #return response.result#['result']
        return response['result']

class _FlexibleRequest:
    def __init__(self, operation):
        self.operation = operation
    def __call__(self, base_url, security={}, data=None, parameters={}, verify=True, session=None, raw_response=False, backoff_seconds=1):
        self = self.operation
        self._request = lambda: None
        self._request.method = self.path[-1]
        self._request.data = None
        self._request.headers = []
        self._request.params = {}
        self._request.url = base_url + self.path[-2]
        assert not security or not self.security
        if self.requestBody:
            if self.requestBody.required and data is None:
                raise ValueError('Request Body is required but none was provided.')
            self._request_handle_body(data)
        self._request_handle_parameters(parameters)
        if session is None:
            session = self._session
        #result = session.send(self._request.prepare(), verify=verify)
        while True:
            result = session.request(**self._request.__dict__)
            if result.status_code != 500: # contains json error text
                try:
                    result.raise_for_status()
                except Exception as exc:
                    if result.status_code == 503: # looks like ratelimiting
                        print(exc)
                        time.sleep(backoff_seconds)
                        backoff_seconds *= 2
                        continue
                    raise
            status_code = str(result.status_code)
            # status_code should be included in response i imagine?
            return result.json()

def default():
    from . import cosmoschains
    chains = cosmoschains.FileRegistry()
    chain = chains.chain('cosmoshub-4')
    for url in chain.urls('rpc'):
        client = RPCClient(url)
        try:
            status = client.status()
            return client
        except Exception as e:
            exc = e
    else:
        raise exc

if __name__ == '__main__':
    client = default()
    status = client.status()
    print(client._base_url)
    print(status)
    height = status['sync_info']['latest_block_height']
    for idx, tx in enumerate(client.tx_search(f'tx.height={height}')):
        print(idx, tx['hash'], tx['height'], tx['index'])

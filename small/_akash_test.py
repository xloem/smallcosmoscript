from akash_protobuf.akash.deployment.v1beta3.groupspec_pb2 import GroupSpec as GroupSpec_1_3
from akash_protobuf.akash.deployment.v1beta2.groupspec_pb2 import GroupSpec as GroupSpec_1_2
from akash_protobuf.akash.deployment.v1beta1.group_pb2 import GroupSpec as GroupSpec_1_1

test_sdl = '''version: '2.0'
services:
  minesweeper:
    image: creepto/minesweeper
    expose:
      - port: 3000
        as: 80
        to:
          - global: true
profiles:
  compute:
    minesweeper:
      resources:
        cpu:
          units: 0.1
        memory:
          size: 512Mi
        storage:
          - size: 512Mi
  placement:
    akash:
      attributes:
        organization: akash.network
      signedBy:
        anyOf:
          - akash1365yvmc4s7awdyj3n2sav7xfx76adc6dnmlx63
          - akash18qa2a2ltfyvkyj0ggj3hkvuj6twzyumuaru9s4
      pricing:
        minesweeper:
          denom: uakt
          amount: 10000
deployment:
  minesweeper:
    akash:
      profile: minesweeper
      count: 1
'''

expected_manifest = [
  {
    "Name": "akash",
    "Services": [
      {
        "Name": "minesweeper",
        "Image": "creepto/minesweeper",
        "Command": None,
        "Args": None,
        "Env": None,
        "Resources": {
          "cpu": {
            "units": {
              "val": "100"
            }
          },
          "memory": {
            "size": {
              "val": "536870912"
            }
          },
          "storage": [
            {
              "name": "default",
              "size": {
                "val": "536870912"
              }
            }
          ],
          "endpoints": None
        },
        "Count": 1,
        "Expose": [
          {
            "Port": 3000,
            "ExternalPort": 80,
            "Proto": "TCP",
            "Service": "",
            "Global": True,
            "Hosts": None,
            "HTTPOptions": {
              "MaxBodySize": 1048576,
              "ReadTimeout": 60000,
              "SendTimeout": 60000,
              "NextTries": 3,
              "NextTimeout": 0,
              "NextCases": [
                "error",
                "timeout"
              ]
            },
            "IP": "",
            "EndpointSequenceNumber": 0
          }
        ]
      }
    ]
  }
]

expected_groups = [
  GroupSpec_1_3(**{
    "name": "akash",
    "requirements": {
      "attributes": [
        {
          "key": "organization",
          "value": "akash.network"
        }
      ],
      "signed_by": {
        "all_of": [],
        "any_of": [
          "akash1365yvmc4s7awdyj3n2sav7xfx76adc6dnmlx63",
          "akash18qa2a2ltfyvkyj0ggj3hkvuj6twzyumuaru9s4"
        ]
      }
    },
    "resources": [
      {
        "resources": {
          "cpu": {
            "units": {
              "val": bytes([
                49,
                48,
                48
              ])
            }
          },
          "memory": {
            "quantity": {
              "val": bytes([
                53,
                51,
                54,
                56,
                55,
                48,
                57,
                49,
                50
                ])
            }
          },
          "storage": [
            {
              "name": "default",
              "quantity": {
                "val": bytes([
                  53,
                  51,
                  54,
                  56,
                  55,
                  48,
                  57,
                  49,
                  50
                  ])
              }
            }
          ],
          "endpoints": [
            {
              "kind": 0,
              "sequence_number": 0
            }
          ]
        },
        "price": {
          "denom": "uakt",
          "amount": "10000"
        },
        "count": 1
      }
    ]
  })
]

expected_version = bytes([
  117, 11, 114, 73, 243, 231, 14, 234,
  211, 32, 100, 158, 202, 240, 89, 251,
  6, 222, 2, 248, 30, 169, 146, 97,
  176, 103, 44, 196, 64, 220, 97, 102
]);

def test():
    from _akash import SDL

    sdl = SDL.from_yaml(test_sdl)
    groups, manifest, version = sdl.groups_manifest_version(GroupSpec_1_3)
    assert manifest == expected_manifest
    assert groups == expected_groups
    assert version == expected_version

if __name__ == '__main__':
    test()

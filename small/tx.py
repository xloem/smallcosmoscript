import asyncio, functools, os, re

import tqdm

from . import util

class BroadcastException(Exception):
    pass

class BaseTransaction:
    def __init__(self, wallet):
        self.wallet = wallet
        self.Coin = self.wallet.client.Coin
        self._msgs = {}
        namedepths = {}
        rnames = [
            (';;'.join([name.lower(), *msgsname.split('_')[:0:-1]]), msg)
            for msgsname, msgs in self.wallet.client.__dict__.items()
            if msgsname.startswith('msgs_')
            for name, msg in msgs.msgs.items()
        ]
        rnames.sort()
        rnames = [('',None),*rnames,('',None)]
        rnames = [
            (max([len(os.path.commonprefix([rnames[a][0],rnames[a+1][0]])) for a in range(idx-1,idx+1)]), *rnames[idx])
            for idx in range(1,len(rnames)-1)
        ]
        for commonlen, rname, msg in rnames:
            try:
                shortrname = rname[:rname.index(';;',commonlen)]
            except ValueError as error:
                import pdb; pdb.set_trace()
                pass
            name = '_'.join(shortrname.split(';;')[::-1])
            longname = '_'.join(rname.split(';;')[::-1])
            dispatch = self._dispatch(msg)
            assert not hasattr(self, name)
            assert not hasattr(self, longname)
            setattr(self, name, dispatch)
            setattr(self, longname, dispatch)
            self._msgs[name] = dispatch
            self._msgs[longname] = dispatch
    def _dispatch(self, msgfactory):
        @functools.wraps(msgfactory)
        def dispatch(*params, **kwparams):
            msg = msgfactory(*params, **kwparams)
            return self.add_msg(msg)
        return dispatch
    async def _agas_fee(self, gas_price, flat, multiplier=2):
        simulation = await self.wallet.client.cosmos_tx.aSimulate(tx_bytes=self.tx_bytes())
        gas_limit = simulation.gas_info.gas_used * multiplier
        fee = gas_price if flat else gas_price * gas_limit
        self.gas_limit = gas_limit
        self.fee = fee
    def add_msg(self, msg):
        raise NotImplementedError('abstract base')
    def __len__(self):
        raise NotImplementedError('abstract base')
    async def asignbroadcast(self, gas_price, fee_denom, wait=True, flat=False, multiplier=2, concurrent=False, desc=None):
        while True:
            try:
                await self.asign(gas_price, fee_denom, flat, multiplier)
                return await self.abroadcast(wait=wait, desc=desc)
            except self.wallet.client.Errors as err:
                status, message = self.wallet.client.error_status_message(err)
                if concurrent and status in (self.wallet.client.Statuses.UNKNOWN,) and 'incorrect account sequence' in message:
                    exc = err
                else:
                    raise
            except BroadcastException as err:
                if concurrent and exc.args[0].code == 32:
                    exc = err
                else:
                    raise
            #print('bumped into a concurrent tx! waiting for it to settle...')
            async with util.await_poll(self.wallet.client, desc='waiting for concurrent tx') as await_poll:
                start_acct = self.wallet.account
                while self.wallet.account == start_acct:
                    await self.wallet.update()
                    await await_poll()
    async def asign(self, gas_price, fee_denom, flat=False, multiplier=2):
        raise NotImplementedError('abstract base')
    async def abroadcast(self, wait=True, tx_bytes=None, desc=None):
        if tx_bytes is None:
            tx_bytes = self.tx_bytes()
        # broadcast modes: unspecified = 0, block = 1, sync = 2, async = 3
        # block: wait for block confirmation, throws timeout error if does not happen fast enough
        # sync: wait for CheckTx response
        # async: do not wait
        result = await self.wallet.client.cosmos_tx.aBroadcastTx(tx_bytes, mode=2)
        # result.code
        # 0 (unset): success
        # 11(sdk): out of gas / gas_used > gas_wanted
        # 19(sdk): already in mempool?
        self.txhash = result.txhash
        if result.code:
            raise BroadcastException(result)
        if hasattr(self.wallet, 'account'):
            self.wallet.account.sequence += 1
        if wait:
            return await self.asyncwait(broadcast_bytes=tx_bytes, desc=desc)
        else:
            return result
    async def asyncwait(self, broadcast_bytes=None, desc=None, txhash=None):
        async with util.await_poll(self.wallet.client, desc=desc) as await_poll:
          while True:
            try:
                confirmation = await self.wallet.client.cosmos_tx.aGetTx(txhash or self.txhash)
                break
            except self.wallet.client.Errors as error:
                status, message = self.wallet.client.error_status_message(error)
                if status in (self.wallet.client.Statuses.NOT_FOUND, self.wallet.client.Statuses.UNAVAILABLE):
                    if broadcast_bytes is not None:
                        result = await self.wallet.client.cosmos_tx.aBroadcastTx(broadcast_bytes, mode=2)
                        #print(result)
                        if result.code == 0: # rebroadcast dropped tx i guess
                            continue
                        if result.code != 19:
                            raise BroadcastException(result)
                    await await_poll(desc or result.txhash)
                    continue
                raise
        return confirmation

class MospyTransaction(BaseTransaction):
    def __init__(self, wallet):
        super().__init__(wallet)
        import mospy
        self.mospy = mospy
        self.tx = self.mospy.Transaction(
            account = wallet.mospy(),
            gas = 0,
            #memo = '',
            chain_id = wallet.client.chain_id,
            protobuf = wallet.client.protobuf,
        )
        self.tx.set_fee(0, wallet.chain.chainjson['fees']['fee_tokens'][0]['denom'])
    #def delegate(self, validator, amount, denom):
    #    client = self.wallet.client
    #    self.add_msg(
    #        client.msgs_cosmos_staking.Delegate(self.wallet.address, validator, client.Coin(amount,denom))
    #    )
    def add_msg(self, msg):
        self.tx.add_raw_msg(msg, '/'+msg.DESCRIPTOR.full_name)
    def __len__(self):
        return len(self.tx._tx_body.messages)
    async def asign(self, gas_price, fee_denom, flat=False, multiplier=2):
        simulation_gas = 1
        self.tx.set_gas(simulation_gas)
        self.tx.set_fee(gas_price if flat else gas_price * simulation_gas, fee_denom)
        await self._agas_fee(gas_price, flat, multiplier) # calls _tx()
        self.tx.set_gas(self.gas_limit)
        self.tx.set_fee(self.fee, fee_denom)
    def tx_bytes(self):
        return self.tx.get_tx_bytes()

class CosmpyTransaction(BaseTransaction):
    def __init__(self, wallet):
        super().__init__(wallet)
        import cosmpy.aerial.client, cosmpy.aerial.tx
        self.cosmpy = cosmpy
        self.tx = self.cosmpy.aerial.client.Transaction()
    def add_msg(self, msg):
        self.tx.add_message(msg)
    def __len__(self):
        return len(self.tx._msgs)
    async def asign(self, gas_price, fee_denom, flat=False, multiplier=2):
        simulation_gas = 1
        simulation_fee = gas_price if flat else gas_price * simulation_gas
        self.tx.seal(
            self.cosmpy.aerial.tx.SigningCfg.direct(self.wallet.key.public, self.wallet.account.sequence),
            fee=f'{simulation_fee}{fee_denom}',
            gas_limit=simulation_gas,
            #memo=memo,
        )
        self.tx.sign(self.wallet.key, self.wallet.client.chain_id, self.wallet.account.number)
        self.tx.complete()
        await self._agas_fee(self.tx.tx, gas_price, flat, multiplier)
        self.tx.seal(
            self.cosmpy.aerial.tx.SigningCfg.direct(self.wallet.key.public, self.wallet.account.sequence),
            fee=f'{self.fee}{fee_denom}',
            gas_limit=self.gas_limit,
            #memo=memo,
        )
        self.tx.sign(self.wallet.key, self.wallet.client.chain_id, self.wallet.account.number)
        self.tx.complete()
    def tx_bytes(self):
        return self.tx.tx.SerializeToString()

Transaction = MospyTransaction

async def amain():
    from . import cosmoschains
    from . import wallet
    chain = cosmoschains.FileRegistry().chain('planq')
    wallet = wallet.Wallet(chain)
    await wallet.__ainit__()
    import pdb; pdb.set_trace()
    tx = Transaction(wallet)
    await tx.send(None)

if __name__ == '__main__':
    import asyncio
    asyncio.run(amain())

#from decimal import Decimal
#from fractions import Fraction as Decimal
from fractions import Fraction
import json
import math
from datetime import datetime, timedelta
import time
import requests.exceptions

from cosmpy2 import LedgerClient
from cosmpy.aerial.config import NetworkConfig
from cosmpy.aerial.client import get_paginated, ValidatorStatus
from cosmpy.mint.rest_client import MintRestClient
from cosmpy.tendermint.rest_client import CosmosBaseTendermintRestClient, GetBlockByHeightRequest
from cosmpy.staking.rest_client import QueryDelegatorDelegationsRequest, QueryValidatorsRequest
from cosmpy.distribution.rest_client import QueryDelegationRewardsRequest
from cosmpy.crypto.hashfuncs import sha256
from cosmpy.crypto.address import Address
from cosmpy.aerial.wallet import LocalWallet
from cosmpy.aerial.tx import SigningCfg
from cosmpy.aerial.client import Transaction, create_delegate_msg, prepare_and_broadcast_basic_transaction
from cosmpy.aerial.exceptions import InsufficientFeesError, QueryTimeoutError

# address prefix, chain_id, fee denom, staking denom
# be helpful to get them from a chainjson
import os
CHAIN_ID=os.environ.get('CHAIN_ID', 'omniflixhub-1')
SEED_PHRASE=os.environ.get('SEED_PHRASE')
if SEED_PHRASE is None:
    with open('cosmos.wallet') as fh:
        SEED_PHRASE=fh.read().strip()
BECH32_PREFIX=os.environ.get('BECH32_PREFIX', 'omniflix')
FEE_DENOM=os.environ.get('FEE_DENOM', 'uflix')
STAKING_DENOM=os.environ.get('STAKING_DENOM', FEE_DENOM)
API_URL=os.environ.get('API_URL', 'rest+http://127.0.0.1:1317')

wallet = LocalWallet.from_mnemonic(SEED_PHRASE, BECH32_PREFIX)
cfg = NetworkConfig(CHAIN_ID, 0, FEE_DENOM, STAKING_DENOM,
    url=API_URL,
    #url='rest+http://127.0.0.1:1317',
    #url='rest+https://rest.omniflix.network',
)
client = LedgerClient(cfg, api_timeout_secs=60)
retain_pct = 0#Decimal('10') / 100
retain_balance = 0#client.query_bank_balance(wallet.address())

#def get_tx(raw):
#  txid = sha256(raw).hex()
#  return json.loads(rest_client.get(f'{client.txs.API_URL}/txs/{txid}'))
#
#claim_gas = 216182 # 1 delegation
#delegate_gas = 263122 # 1 delegation, larger than initial
#gas_prices = []
#for height in range(block2_height, block1_height+1):
#  block = {block1_height:block1, block2_height:block2}.get(height)
#  if block is None:
#    block = client.tendermint.GetBlockByHeight(GetBlockByHeightRequest(height = height)).block
#  for tx in block.data.txs:
#    tx = get_tx(tx)
#    fee = tx['tx']['auth_info']['fee']
#    fee_amounts = fee['amount']
#    if len(fee_amounts) > 1:
#        continue
#    elif len(fee_amounts) == 0:
#        fee_amount = 0
#    else:
#        fee_amount = fee_amounts[0]
#        assert fee_amount['denom'] == client.network_config.fee_denomination
#        fee_amount = int(fee_amount['amount'])
#    gas_limit = int(fee['gas_limit'])
#
#    gas_price = Fraction(fee_amount, gas_limit)
#    gas_prices.append(gas_price)
#    print(gas_price)
#    
#print(gas_prices)

#min_gas_price = 0
#assert min_gas_price == 0

account = client.query_account(wallet.address())

expected_redelegation_fee = 0#413

assert client.network_config.fee_minimum_gas_price == 0



rest_client = client.distribution._rest_api
if not hasattr(client, 'mint'):
    client.mint = MintRestClient(rest_client)
if not hasattr(client, 'tendermint'):
    client.tendermint = CosmosBaseTendermintRestClient(rest_client)

block2 = None
block2_positions = None
last_delegation = None

bpr = None

while True:
  annual_provisions = Fraction(client.mint.AnnualProvisions().annual_provisions.decode())
  mint_params = json.loads(rest_client.get(f'{MintRestClient.API_URL}/params'))['params']
  # b'{\n  "params": {\n    "mint_denom": "uflix",\n    "inflation_rate_change": "0.005000000000000000",\n    "inflation_max": "0.33 0000000000000000",\n    "inflation_min": "0.325000000000000000",\n    "goal_bonded": "0.670000000000000000",\n    "blocks_per_ye ar": "6311520"\n  }\n}'
  blocks_per_year = int(mint_params['blocks_per_year'])
  inflation = Fraction(client.mint.Inflation().inflation.decode())
  bonded_tokens = int(client.staking.Pool(None).pool.bonded_tokens)
  community_tax = Fraction(client.distribution.Params().params.community_tax)

  validator_commissions = {}
  for resp in get_paginated(
      QueryValidatorsRequest(status=ValidatorStatus.BONDED.value),
      client.staking.Validators
  ):
      validator_commissions.update({
          validator.operator_address: Fraction(validator.commission.commission_rates.rate)
          for validator in resp.validators
      })
  min_commission = min(validator_commissions.values())
  
  block1 = client.query_latest_block()
  block1_height = block1.height
  #  if block2 is None or block1_height > block2_height:
  #      break
  if block2 is None:
    block2_height = block1_height - 10000 if block1_height > 10000 else 1
    block2 = client.query_block(block2_height)
    block3_height = block2_height
    block3 = block2
  elif block1_height == block2_height:
    time.sleep(365.25 * 24 * 60 * 60 / blocks_per_year_real / 4)
    continue
  year_secs = 24 * 6 * 6 * 36525 # 60 * 60 * 365.25, move the 10s
  block_secs = block1.time.timestamp() - block3.time.timestamp()
  blocks_per_year_real = math.ceil((block1_height - block3_height) * year_secs / block_secs)
  
  nominal_apr = annual_provisions * (1 - community_tax) / bonded_tokens
  
  block_provision = annual_provisions / blocks_per_year
  #block_provision = annual_provisions / blocks_per_year
  real_provision = block_provision * blocks_per_year_real
  real_apr = nominal_apr * (real_provision / annual_provisions)
  
  bpr = Fraction(block_provision, bonded_tokens)
  #assert bpr > 1e-6
  assert real_apr == bpr * blocks_per_year_real
  print(float(real_apr*100*(1-min_commission)),'%')
  print(float(bpr*100*(1-min_commission)),'%')
  #impossible_apr = (1+bpr)**blocks_per_year_real - 1
  #print(impossible_apr*100,'%')

  block1_positions = client.get_amounts_rewards(str(wallet.address()))
  wait_for_reward_pct = float('inf')
  for validator, (amount, reward) in block1_positions.items():
    amount = Fraction(amount)
    reward = Fraction(reward)
    if block2_positions != None:
        _, block2_reward = block2_positions[validator]
        #if reward  == block2_reward: # maybe from using the reward endpoint instead of lining up with block
                                     # happens with heights offset by 1
        #    continue
#        new_bpr = (reward - block2_reward) / amount / (block1_height - block2_height)
#        #assert new_bpr > 0
#        #assert new_bpr > 1e-6
#        if new_bpr > 0: # roughly stemming from getting the rewards not accurate to the blocktimes
#            bpr = new_bpr
    commission = validator_commissions[validator]
    rewblks = round(reward / (bpr * amount * (1 - commission))) or 1
    #new_bpr = reward / (rewblks * amount * (1 - commission))
    #assert new_bpr
    #bpr = new_bpr

    current_balance = client.query_bank_balance(wallet.address())
    reward += current_balance - retain_balance
    if reward == 0:
        continue
    validator = Address(validator)
    #print(validator, amount, reward)

    while True:
        max_bpr = 0

        for blkct in range(1,blocks_per_year):
            est_reward = int(blkct * bpr * amount * (1 - commission)) - expected_redelegation_fee
            est_bpr = float(1 + est_reward/amount*(1-retain_pct)) ** (1 / blkct) - 1
            est_apr = float(1 + est_reward/amount*(1-retain_pct)) ** (blocks_per_year_real / blkct) - 1
            if est_bpr < max_bpr:
                break
            max_bpr = est_bpr
            max_apr = est_apr
            min_blkct = blkct
            min_reward = est_reward + expected_redelegation_fee
        print(validator, amount, float(reward), min_blkct, min_reward, max_apr)
        if bpr < 1e-6:
            print('estreward=',est_reward)
            print('i think the occasional low numbers are because the reward is from a partial block compared to 0 or such')
        #assert est_apr > 40
        if reward < min_reward:
            delegation = None
            new_wait_for_reward_pct = (min_reward - reward) / amount
            if reward > 0:
                new_wait_for_reward_pct -= bpr
            #if new_wait_for_reward_pct > bpr:
            #    new_wait_for_reward_pct -= bpr
            wait_for_reward_pct = min(wait_for_reward_pct, new_wait_for_reward_pct)
            if wait_for_reward_pct > 0:
                break

        delegation_tx = Transaction()
        delegation_tx.add_message(
            create_delegate_msg(
                wallet.address(),
                validator,
                int(reward),
                client.network_config.staking_denomination,
            )
        )
    
        # we need to build up a representative transaction so that we can accurately simulate it
        delegation_tx.seal(
            SigningCfg.direct(wallet.public_key(), account.sequence),
            fee="",
            gas_limit=0,
            #memo=memo,
        )
        delegation_tx.sign(wallet.signer(), client.network_config.chain_id, account.number)
        delegation_tx.complete()
    
        # simulate the gas and fee for the transaction
        try:
            gas_limit, fee = client.estimate_gas_and_fee_for_tx(delegation_tx)
        except RuntimeError as error:
            fields = {key: val for key, val in [line.strip().split(': ', 1) for line in error.args[0].split('\n') if ': ' in line]}
            if 'Response' in fields:
                response = fields['Response']
                if response.endswith(')') and not response.startswith('('):
                    response = '(' + response
                statuscode, body = eval(response)
                response = json.loads(body)
                if response['code'] == 2:
                    msg = response['message']
                    if 'account sequence' in msg:
                        sequence = msg.split(', ')[1].split(' ')[-1]
                        print(f'adjusting sequence from {account.sequence} to {sequence}')
                        account.sequence = int(sequence)
                        continue
                    elif 'insufficient funds' in msg:
                        break
            raise
        fee_in_denom = int(fee[:-len(client.network_config.fee_denomination)]) 
        if fee_in_denom != expected_redelegation_fee:
            expected_redelegation_fee = fee_in_denom
            continue
    
        try:
            delegation = prepare_and_broadcast_basic_transaction(
                client, delegation_tx, wallet, account=account, gas_limit=gas_limit#, memo=memo
            )
            last_delegation = delegation
            print(f'broadcasted with account sequence {account.sequence}')
            print(delegation.tx_hash)
            if last_delegation is not None:
                try:
                    #client.wait_for_query_tx(last_delegation.tx_hash, timeout=timedelta(days=365.25/blocks_per_year_real), poll_period=timedelta(seconds=0.25))
                    last_delegation.wait_to_complete(timeout=timedelta(days=3*365.25/blocks_per_year_real), poll_period=timedelta(days=0.05*365.25/blocks_per_year_real))
                    if not last_delegation.response.is_successful():
                        break
                except QueryTimeoutError:
                    #break
                    pass
                except requests.exceptions.ReadTimeout:
                    pass
            block1_positions[validator] = (amount + reward, 0)
            block2_positions = None
            retain_balance += retain_pct * reward
            account.sequence += 1
            print(f'raised account sequence to {account.sequence}')
            break
        except InsufficientFeesError as fee_error:
            fee_in_denom = int(fee_error.minimum_required_fee[:-len(client.network_config.fee_denomination)]) 
            client.network_config.fee_minimum_gas_price = fee_in_denom / gas_limit
            expected_redelegation_fee = fee_in_denom
            continue
        except RuntimeError as error:
            print(error)
            account.sequence += 1
            continue

  if wait_for_reward_pct != float('inf') and wait_for_reward_pct > 0:
    if block2_positions == None:
      block2 = block1
      block2_height = block1_height
      block2_positions = block1_positions
    wait_blocks = float(wait_for_reward_pct / bpr)
    block_seconds = 36525*24*6*6/blocks_per_year_real
    now = time.time()
    deadline = now + min(1,wait_blocks) * block_seconds
    print(block1_height, '+', wait_blocks, datetime.fromtimestamp(deadline).isoformat() )
    time.sleep(deadline - now)
